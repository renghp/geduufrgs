﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class scheduleActivityController : MonoBehaviour {

	private int status = 0; //0 = default, 1 = green, 2 = yellow, 3 = red
	private float timer;
	private bool countingTime;



	// Use this for initialization
	void Start () {
		countingTime = false;



	}
	
	// Update is called once per frame
	void Update () {

		if (countingTime) {
			timer += Time.deltaTime;

			if (timer >= 0.3f) {

				Debug.Log ("foi");



				if (gameObject.name.Contains ("Simu")) {

					if (gameObject.name.Contains ("monday") || gameObject.name.Contains ("tuesday") || gameObject.name.Contains ("wednesday"))
					{
						PlayerPrefs.SetInt ("subgroup", weekController.currentSubgroup);

					}
					else
					{
						PlayerPrefs.SetInt ("subgroup", weekController.currentSubgroup2);

					}

					string subjectDay = gameObject.tag + "Subject";

					getDirectedQuestionsDB.currentSubject = PlayerPrefs.GetInt (subjectDay);

					getDirectedQuestionsDB.weekDaySimu = gameObject.tag + "Simu";

					SceneManager.LoadScene ("directedSimulationScene");

				}
				else
					GameObject.FindGameObjectWithTag ("scriptManager").GetComponent<panelController> ().openPanel ();
				
				countingTime = false;
				timer = 0f;
			}
		}




	}

	public void turnGreenOnClick()
	{
		if (status == 0) {
			transform.GetComponent<Image> ().color = new Color (0f, 0.609f, 0.031f); //green

			string statusName = gameObject.tag + "Status";

			PlayerPrefs.SetInt (statusName, 1); 	//gray = 0, green = 1, math = 2, chem = 3


			status++;
		} else if (status == 1) {

			transform.GetComponent<Image> ().color = new Color (1f, 0.808f, 0f); //yellow

			string statusName = gameObject.tag + "Status";

			PlayerPrefs.SetInt (statusName, 2); 	//gray = 0, green = 1, math = 2, chem = 3

			status++;
		}
		else if (status == 2) {

			transform.GetComponent<Image> ().color = new Color (0.949f, 0.133f, 0.286f); //red

			string statusName = gameObject.tag + "Status";

			PlayerPrefs.SetInt (statusName, 3); 	//gray = 0, green = 1, math = 2, chem = 3

			status++;
		}
		else if (status == 3) {

			Color gray = new Color (0.329f, 0.369f, 0.404f);
			gray.a = 0.390625f;
			transform.GetComponent<Image> ().color = gray;

			string statusName = gameObject.tag + "Status";

			PlayerPrefs.SetInt (statusName, 0); 	//gray = 0, green = 1, math = 2, chem = 3

			status = 0;
		}

		foreach (GameObject prog in GameObject.FindGameObjectsWithTag ("progress"))		//forces all progress bars to update given a new value
			prog.transform.GetComponent<activityProgress> ().updateProgress ();
		
	}

	public void turnGreenOnClickRest()
	{
		if (status == 0) {
			transform.GetComponent<Image> ().color = new Color (0f, 0.609f, 0.031f); //green

			string statusName = gameObject.tag + "Rest";

			PlayerPrefs.SetInt (statusName, 1); 	//gray = 0, green = 1, math = 2, chem = 3


			status++;
		} else if (status == 1) {

			transform.GetComponent<Image> ().color = new Color (1f, 0.808f, 0f); //yellow

			string statusName = gameObject.tag + "Rest";

			PlayerPrefs.SetInt (statusName, 2); 	//gray = 0, green = 1, math = 2, chem = 3

			status++;
		}
		else if (status == 2) {

			transform.GetComponent<Image> ().color = new Color (0.949f, 0.133f, 0.286f); //red

			string statusName = gameObject.tag + "Rest";

			PlayerPrefs.SetInt (statusName, 3); 	//gray = 0, green = 1, math = 2, chem = 3

			status++;
		}
		else if (status == 3) {

			Color gray = new Color (0.329f, 0.369f, 0.404f);
			gray.a = 0.390625f;
			transform.GetComponent<Image> ().color = gray;

			string statusName = gameObject.tag + "Rest";

			PlayerPrefs.SetInt (statusName, 0); 	//gray = 0, green = 1, math = 2, chem = 3

			status = 0;
		}

		foreach (GameObject prog in GameObject.FindGameObjectsWithTag ("progress"))		//forces all progress bars to update given a new value
			prog.transform.GetComponent<activityProgress> ().updateProgress ();

	}

	public void turnGreenOnClickSimu()
	{
		if (status == 0) {
			transform.GetComponent<Image> ().color = new Color (0f, 0.609f, 0.031f); //green

			string statusName = gameObject.tag + "Simu";

			PlayerPrefs.SetInt (statusName, 1); 	//gray = 0, green = 1, math = 2, chem = 3


			status++;
		} else if (status == 1) {

			transform.GetComponent<Image> ().color = new Color (1f, 0.808f, 0f); //yellow

			string statusName = gameObject.tag + "Simu";

			PlayerPrefs.SetInt (statusName, 2); 	//gray = 0, green = 1, math = 2, chem = 3

			status++;
		}
		else if (status == 2) {

			transform.GetComponent<Image> ().color = new Color (0.949f, 0.133f, 0.286f); //red

			string statusName = gameObject.tag + "Simu";

			PlayerPrefs.SetInt (statusName, 3); 	//gray = 0, green = 1, math = 2, chem = 3

			status++;
		}
		else if (status == 3) {

			Color gray = new Color (0.329f, 0.369f, 0.404f);
			gray.a = 0.390625f;
			transform.GetComponent<Image> ().color = gray;

			string statusName = gameObject.tag + "Simu";

			PlayerPrefs.SetInt (statusName, 0); 	//gray = 0, green = 1, math = 2, chem = 3

			status = 0;
		}

		foreach (GameObject prog in GameObject.FindGameObjectsWithTag ("progress"))		//forces all progress bars to update given a new value
			prog.transform.GetComponent<activityProgress> ().updateProgress ();

	}

	public void beginTimer()
	{
		
		timer = 0f;
		countingTime = true;
		Debug.Log ("começou a contar");
			
	}

	public void endTimer()
	{
		if (timer >= 0.3f) {

			Debug.Log ("foi");
		
			if (gameObject.name.Contains ("Simu")) {

				if (gameObject.name.Contains ("monday") || gameObject.name.Contains ("tuesday") || gameObject.name.Contains ("wednesday"))
				{
					PlayerPrefs.SetInt ("subgroup", weekController.currentSubgroup);

				}
				else
				{
					PlayerPrefs.SetInt ("subgroup", weekController.currentSubgroup2);

				}

				string subjectDay = gameObject.tag + "Subject";

				getDirectedQuestionsDB.currentSubject = PlayerPrefs.GetInt (subjectDay);

				getDirectedQuestionsDB.weekDaySimu = gameObject.tag + "Simu";

				SceneManager.LoadScene ("directedSimulationScene");
			}
			else
				GameObject.FindGameObjectWithTag ("scriptManager").GetComponent<panelController> ().openPanel ();
		}

		countingTime = false;
		timer = 0f;

	}


}
