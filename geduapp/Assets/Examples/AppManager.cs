﻿using Amazon.S3.Model;
using UnityEngine;
using UnityEngine.UI;

namespace AWSSDK.Examples
{
    public class AppManager : MonoBehaviour
    {
        #region VARIABLES 


        [SerializeField] private string S3BucketName;
        //[Tooltip("FileName with Extesion. E.G file.txt")] [SerializeField] private string fileNameOnBucket;

		string fileNameOnBucket;
		string pathFile;
       // [Tooltip("Path and FileName with Extesion. E.G Documents/file.txt")] [SerializeField] private string pathFileUpload;

       // [Header("Buttons")]
       // [SerializeField] private Button buttonListBuckets;
       // [SerializeField] private Button buttonListFilesBucket;
        //[SerializeField] private Button buttonGetFileBucket;
        [SerializeField] private Button buttonUploadFileBucket;
        //[SerializeField] private Button buttonDeleteFileBucket;
        [SerializeField] private Text resultTextOperation;

        #endregion

        #region METHODS MONOBEHAVIOUR

        void Start()
        {



            //buttonListBuckets.onClick.AddListener(() => { ListBuckets(); });
            //buttonListFilesBucket.onClick.AddListener(() => { ListObjectsBucket(); });
            //buttonGetFileBucket.onClick.AddListener(() => { GetObjectBucket(); });
            buttonUploadFileBucket.onClick.AddListener(() => { UploadObjectForBucket(S3BucketName); });
           // buttonDeleteFileBucket.onClick.AddListener(() => { DeleteObjectOnBucket(); });

           // S3Manager.Instance.OnResultGetObject += GetObjectBucket;
        }

        #endregion

        #region METHODS CREATED

      /*  private void ListBuckets()
        {
            resultTextOperation.text = "Fetching all the Buckets";

            S3Manager.Instance.ListBuckets((result, error) =>
            {
                resultTextOperation.text += "\n";

                if (string.IsNullOrEmpty(error))
                {
                    resultTextOperation.text += "Got Response \nPrinting now \n";

                    result.Buckets.ForEach((bucket) =>
                    {
                        resultTextOperation.text += string.Format("bucket = {0}\n", bucket.BucketName);
                    });
                }
                else
                {
                    print("Get Error:: " + error);
                    resultTextOperation.text += "Got Exception \n";
                }
            });
        }*/

     /*   private void ListObjectsBucket()
        {
            resultTextOperation.text = "Fetching all the Objects from " + S3BucketName;

            S3Manager.Instance.ListObjectsBucket(S3BucketName, (result, error) =>
            {
                resultTextOperation.text += "\n";
                if (string.IsNullOrEmpty(error))
                {
                    resultTextOperation.text += "Got Response \nPrinting now \n";
                    result.S3Objects.ForEach((file) =>
                    {
                        resultTextOperation.text += string.Format("File: {0}\n", file.Key);
                    });
                }
                else
                {
                    print("Get Error:: " + error);
                    resultTextOperation.text += "Got Exception \n";
                }
            });
        }*/

       /* private void GetObjectBucket(GetObjectResponse resultFinal = null, string errorFinal = null)
        {
            resultTextOperation.text = string.Format("fetching {0} from bucket {1}", fileNameOnBucket, S3BucketName);

            if(errorFinal != null)
            {
                resultTextOperation.text += "\n";
                resultTextOperation.text += "Get Data Error";
                print("Get Error:: " + errorFinal);
                return;
            }

            S3Manager.Instance.GetObjectBucket(S3BucketName, fileNameOnBucket, (result, error) =>
            {
                if (string.IsNullOrEmpty(error))
                {
                    resultTextOperation.text += "\nGet Data Complete.";
                }
                else
                {
                    resultTextOperation.text += "\n";
                    resultTextOperation.text += "Get Data Error";
                    print("Get Error:: " + error);
                }
            });

        }*/

        private void UploadObjectForBucket(string S3BucketName)
        {

			Debug.Log (getImageGallery.userAvatarFilePath);


			if ((getImageGallery.userAvatarFilePath != null) && (getImageGallery.userAvatarFilePath != "")) {

				pathFile = getImageGallery.userAvatarFilePath;

				if (pathFile.Contains (".png") || pathFile.Contains (".PNG") || pathFile.Contains (".JPG") || pathFile.Contains (".jpg") || pathFile.Contains (".jpeg") || pathFile.Contains (".JPEG")) {

					fileNameOnBucket = loginManager.currentUserID + "userAvatar.png";

					long imageSize = new System.IO.FileInfo(pathFile).Length;

					if (imageSize <= 500000)
					{


						FeedBackOk ("Enviando imagem");
						//resultTextOperation.text = "\nCreating request object";
						//resultTextOperation.text = "\nMaking HTTP post call";





						S3Manager.Instance.UploadObjectForBucket (pathFile, S3BucketName, fileNameOnBucket, (result, error) => {
							if (string.IsNullOrEmpty (error)) {
								FeedBackOk ("Imagem enviada!");
							} else {
								FeedBackError ("Falha no envio");
								Debug.LogError ("Get Error:: " + error);
							}
						});
					}
					else
						FeedBackError ("Imagem grande demais! :O\nLimite de 500 kB!");
				}
				else 
					FeedBackError("Extensão inválida :(\nAceitamos \'jpg\' e \'png\'!");

			} else
				FeedBackError("Imagem não especificada ou inválida!");
        }

       /* private void DeleteObjectOnBucket()
        {
            resultTextOperation.text = string.Format("deleting {0} from bucket {1}\n", fileNameOnBucket, S3BucketName);

            S3Manager.Instance.DeleteObjectOnBucket(fileNameOnBucket, S3BucketName, (result, error) =>
            {
                if (string.IsNullOrEmpty(error))
                {
                    resultTextOperation.text += "\nFile Deleted Success";
                }
                else
                {
                    resultTextOperation.text += "\nFile Deleted Failed";
                    print("Get Error:: " + error);
                }
            });
        }*/

        #endregion



		void FeedBackOk(string mensagem)
		{
			resultTextOperation.CrossFadeAlpha(100f, 1f, false);
			resultTextOperation.color = new Color(0.13f, 0.5f, 0.83f);
			resultTextOperation.text = mensagem;
			resultTextOperation.CrossFadeAlpha(0f, 3f, false);
		}

		void FeedBackError(string mensagem)
		{
			resultTextOperation.CrossFadeAlpha(0f, 1f, false);
			resultTextOperation.CrossFadeAlpha(100f, 2f, false);
			resultTextOperation.color = new Color(0.878f, 0.043f, 0f);
			resultTextOperation.text = mensagem;
			resultTextOperation.CrossFadeAlpha(0f, 4f, false);
		}
    }


}