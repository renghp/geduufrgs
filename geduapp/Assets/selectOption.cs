﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class selectOption : MonoBehaviour {

	public Button [] options;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void selectAnswer(int chosenOptionId)
	{
		foreach (Button opt in options)
		{
			opt.interactable = true;
		}

		options [chosenOptionId].interactable = false;
	}
}
