﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


	public class loadUserAvatar : MonoBehaviour {

	static public string imgURL;

	public RawImage tempImage;

	public Sprite defaultImg;

	// Use this for initialization
	void Start () {
		
		updatePic ();			//if it doesn't update frequently enough, move to "onEnable"
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void updatePic()
	{
		if ((loginManager.currentUserID != null) && (loginManager.currentUserID != "")) {
			imgURL = "https://s3.amazonaws.com/gedunorthamerica/" + loginManager.currentUserID + "userAvatar.png";


			WWW imgPage = new WWW (imgURL); // + "?usuario=" + usuario + "&senha=" + senha);


			StartCoroutine (loadImage (imgPage));

		}
	}

	void OnEnable()
	{
		
	}

	IEnumerator loadImage(WWW imgPage)
	{

		float timeout = 30f;
		float waitingTime = 0f;

		gameObject.GetComponent<RawImage> ().texture = null;
		tempImage.texture = null;

		while (!imgPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {

			gameObject.GetComponent<RawImage> ().texture = defaultImg.texture;
			tempImage.texture = defaultImg.texture;

			Debug.Log ("Tentamos, mas não rolou de carregar a imagem do usuario");
			yield return null;
		} else if ((imgPage.texture.height >10) && (imgPage.texture.width >10)){
			gameObject.GetComponent<RawImage> ().texture = imgPage.texture;


			gameObject.GetComponent<RawImage> ().SetNativeSize ();

			Debug.Log ("width = " + imgPage.texture.width + "height: " + imgPage.texture.height);

			tempImage.texture = gameObject.GetComponent<RawImage> ().texture;
			tempImage.SetNativeSize ();

			if (imgPage.texture.width > imgPage.texture.height) {
				Debug.Log ("imagem horizontal");
				gameObject.GetComponent<RawImage> ().rectTransform.sizeDelta = new Vector2 ((250f / gameObject.GetComponent<RawImage> ().rectTransform.sizeDelta.y) * gameObject.GetComponent<RawImage> ().rectTransform.sizeDelta.x, 250f);

			} else {
				Debug.Log ("imagem vertical");
				gameObject.GetComponent<RawImage> ().rectTransform.sizeDelta = new Vector2 (250f, (250f / gameObject.GetComponent<RawImage> ().rectTransform.sizeDelta.x) * gameObject.GetComponent<RawImage> ().rectTransform.sizeDelta.y);
			}

			tempImage.rectTransform.sizeDelta = new Vector2 ((400f / gameObject.GetComponent<RawImage> ().rectTransform.sizeDelta.y) * gameObject.GetComponent<RawImage> ().rectTransform.sizeDelta.x, 400f);



		} else {
			gameObject.GetComponent<RawImage> ().texture = defaultImg.texture;
			tempImage.texture = defaultImg.texture;
		}
	}
}