﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;
//using MySql.Data.MySqlClient; 
//using MySql.Data; 
using System.IO;
//using System.Data;
using System.Collections; 
using System;

public class createAccController : MonoBehaviour {

	public static string name, lastname, user, email, userpwd;
	public static int area, major, works, studies, hs;

	public Text areaField, majorField, worksField, studiesField, hsField;

	public GameObject nameField, lastnameField, userField, emailField,pwdField, pwd2Field;

	//public GameObject invisibleBackToLoginButton;

	public Text feedbackText;
	public Text feedbackText2;



	// Use this for initialization
	void Start () {
		//invisibleBackToLoginButton.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void feedbackError(string msg)
	{
		feedbackText.text = msg;
	}
	void feedbackPositive(string msg)
	{
		feedbackText2.text = msg;
	}

	public void proceedWithAcc()
	{



		if (pwdField.GetComponent<InputField>().text.Length < 4) {
			feedbackError ("Senha precisa ter pelo menos 4 caracteres!");
		} else if (nameField.GetComponent<InputField>().text == "") {
			feedbackError ("Seu nome precisa ser preenchido!");
		} else if (lastnameField.GetComponent<InputField>().text == "") {
			feedbackError ("Seu sobrenome precisa ser preenchido!");
		} else if (userField.GetComponent<InputField>().text.Length < 4) {
			feedbackError ("Usuário precisa ter pelo menos 4 caracteres!");
		} else if (!emailField.GetComponent<InputField>().text.Contains('@') || !emailField.GetComponent<InputField>().text.Contains('.') || emailField.GetComponent<InputField>().text.Length < 4) {
			feedbackError ("Email inválido!");
		} else if (pwdField.GetComponent<InputField>().text != pwd2Field.GetComponent<InputField>().text)  {
			feedbackError ("Senhas não são iguais!");
		} else {
			name = nameField.GetComponent<InputField>().text;
			lastname = lastnameField.GetComponent<InputField>().text;
			user = userField.GetComponent<InputField>().text;
			email = emailField.GetComponent<InputField>().text;
			userpwd = pwdField.GetComponent<InputField>().text;

			switch (areaField.text) {
			case "Artes":
				area = 1;
				break;
			case "Biológicas, Naturais e Agrárias":
				area = 2;
				break;
			case "Comunicação e Informação":
				area = 3;
				break;
			case "Economia, Gestão e Negócios":
				area = 4;
				break;
			case "Engenharia e Arquitetura":
				area = 5;
				break;
			case "Exatas e Tecnológicas":
				area = 6;
				break;
			case "Humanas e Sociais":
				area = 7;
				break;
			case "Saúde":
				area = 8;
				break;
			default:
				area = 99;
				break;

			}

			switch (majorField.text) {
			case "Administração":
				major = 32;
				break;
			case "Administração Pública e Social":
				major = 33;
				break;
			case "Agronomia":
				major = 8;
				break;
			case "Arquitetura e Urbanismo":
				major = 39;
				break;
			case "Arquivologia":
				major = 25;
				break;
			case "Artes Visuais":
				major = 1;
				break;
			case "Biblioteconomia":
				major = 26;
				break;
			case "Biomedicina":
				major = 70;
				break;
			case "Biotecnologia":
				major = 10;
				break;
			case "Ciência da Computação":
				major = 53;
				break;
			case "Ciência e Tecnologia (Litoral Norte)":
				major = 56;
				break;
			case "Ciências Atuariais":
				major = 34;
				break;
			case "Ciências Biológicas":
				major = 11;
				break;
			case "Ciências Biológicas - Ceclimar":
				major = 12;
				break;
			case "Ciências Contábeis":
				major = 35;
				break;
			case "Ciências da Natureza (EAD)":
				major = 13;
				break;
			case "Ciências Econômicas":
				major = 36;
				break;
			case "Ciências Jurídicas e Sociais":
				major = 58;
				break;
			case "Ciências Sociais":
				major = 59;
				break;
			case "Ciências Sociais (EAD)":
				major = 60;
				break;
			case "Comunicação Social":
				major = 27;
				break;
			case "Dança":
				major = 2;
				break;
			case "Desenvolvimento Regional (Litoral Norte)":
				major = 14;
				break;
			case "Desenvolvimento Rural (EAD)":
				major = 9;
				break;
			case "Design de Produto":				
				major = 4;
				break;
			case "Design Visual":				
				major = 3;
				break;
			case "Educação do Campo (Litoral Norte)":	
				major = 15;
				break;
			case "Educação Física":
				major = 71;
				break;
			case "Enfermagem":				
				major = 72;
				break;
			case "Engenharia Ambiental":
				major = 16;
				break;
			case "Engenharia Cartográfica":
				major = 17;
				break;
			case "Engenharia Civil":
				major = 40;
				break;
			case "Engenharia de Alimentos":
				major = 18;
				break;
			case "Engenharia de Computação":
				major = 47;
				break;
			case "Engenharia de Controle e Automação":
				major = 48;
				break;
			case "Engenharia de Energia":
				major = 49;
				break;
			case "Engenharia de Gestão de Energia (Litoral Norte)":
				major = 50;
				break;
			case "Engenharia de Materiais":
				major = 51;
				break;
			case "Engenharia de Minas":
				major = 52;
				break;
			case "Engenharia de Produção":
				major = 37;
				break;
			case "Engenharia de Serviços (Litoral Norte)":
				major = 38;
				break;
			case "Engenharia Elétrica":
				major = 41;
				break;
			case "Engenharia Física":
				major = 42;
				break;
			case "Engenharia Hídrica":
				major = 43;
				break;
			case "Engenharia Mecânica":
				major = 44;
				break;
			case "Engenharia Metalúrgica":
				major = 45;
				break;
			case "Engenharia Química":
				major = 46;
				break;
			case "Estatística":
				major = 54;
				break;
			case "Farmácia":
				major = 73;
				break;
			case "Filosofia":
				major = 61;
				break;
			case "Física":
				major = 55;
				break;
			case "Fisioterapia":
				major = 74;
				break;
			case "Fonoaudiologia":
				major = 75;
				break;
			case "Geografia":
				major = 19;
				break;
			case "Geografia (EAD)":
				major = 20;
				break;
			case "Geografia (Litoral Norte)":
				major = 21;
				break;
			case "História":
				major = 62;
				break;
			case "História da Arte":
				major = 5;
				break;
			case "Jornalismo":
				major = 28;
				break;
			case "Letras":
				major = 63;
				break;
			case "Matemática":
				major = 57;
				break;
			case "Medicina":
				major = 76;
				break;
			case "Medicina Veterinária":
				major = 22;
				break;
			case "Museologia":
				major = 29;
				break;
			case "Música":
				major = 6;
				break;
			case "Nutrição":
				major = 77;
				break;
			case "Odontologia":
				major = 78;
				break;
			case "Pedagogia":
				major = 64;
				break;
			case "Pedagogia (EAD)":
				major = 65;
				break;
			case "Políticas Públicas":
				major = 66;
				break;
			case "Psicologia":
				major = 67;
				break;
			case "Publicidade e Propaganda":
				major = 30;
				break;
			case "Química":
				major = 23;
				break;
			case "Relações Internacionais":
				major = 68;
				break;
			case "Relações Públicas":
				major = 31;
				break;
			case "Saúde Coletiva":
				major = 79;
				break;
			case "Serviço Social":
				major = 69;
				break;
			case "Teatro":
				major = 7;
				break;
			case "Zootecnia":
				major = 24;
				break;
			default:
				major = 99;
				break;

			}

			SceneManager.LoadScene ("accProfilingScene");
		}
	}

/*	public void OpenSql()
	{

		try
		{
			string connectionString = string.Format("Server = {0};port={4};Database = {1}; User ID = {2}; Password = {3};",host,database,id,pwd,"3306");
			dbConnection = new MySqlConnection(connectionString);
			dbConnection.Open(); 
		}catch (Exception e)
		{
			feedbackError (e.Message.ToString());
			throw new Exception("error" + e.Message.ToString());  



		}

	}*/

	public void createAcc()
	{


		switch (worksField.text) {
		case "Sim":
			works = 1;
			break;
		case "Não":
			works = 2;
			break;
		default:
			works = 0;
			break;
		}
		switch (studiesField.text) {
		case "Não":
			studies = 1;
			break;
		case "Sim, somente cursinho":
			studies = 2;
			break;
		case "Sim, colégio e cursinho":
			studies = 3;
			break;
		case "Sim, somente colégio":
			studies = 4;
			break;
		default:
			studies = 0;
			break;
		}
		switch (hsField.text) {
		case "Ensino Público":
			hs = 1;
			break;
		case "Ensino Privado":
			hs = 2;
			break;
		default:
			hs = 0;
			break;
		}

		StartCoroutine(registerNewUser());



	}


	private IEnumerator registerNewUser()
	{

		string url = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/registerUser.php";

		WWWForm dbForm = new WWWForm();
		dbForm.AddField("user", user);
		dbForm.AddField("password", userpwd);
		dbForm.AddField("fname", name);
		dbForm.AddField("lname", lastname);
		dbForm.AddField("email", email);
		dbForm.AddField("area", area.ToString());
		dbForm.AddField("major", major.ToString());
		dbForm.AddField("works", works.ToString());
		dbForm.AddField("studies", studies.ToString());
		dbForm.AddField("highschool", hs.ToString());


		//public static string name, lastname, user, email, userpwd;
		//public static int area, major, works, studies, hs;


		WWW db = new WWW(url, dbForm);


		yield return db;

		if (db.error != null)
		{
			feedbackError("Não foi possível estabelecer conexão com o servidor.\nPor favor, cheque sua internet.");
			Debug.Log(db.error);
		}
		else
		{
			
			
			if (db.text.Contains("sucesso"))
			{
				feedbackError ("");
				feedbackPositive ("Usuário criado com sucesso :)\nVoltando à tela de login!");


				StartCoroutine (waitABit ());

			}
			else if (db.text.Contains("Duplicate"))
			{
				if (db.text.Contains("email"))
					feedbackError("Endereço de e-mail já registrado!");
				else if (db.text.Contains("user"))
					feedbackError("Nome de usuário já registrado!");
			}
			else
			{
				feedbackError("Erro desconhecido:" + db.text);
			}

		}

	}


	IEnumerator waitABit()
	{
		yield return new WaitForSeconds (2);

		SceneManager.LoadScene("loginScene");
	}

}
