﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System.Collections;

public class profileController : MonoBehaviour {

	public Text userNameText;
	public Text errorFeedback;
	public Dropdown userAreaDD;
	public Dropdown userMajorDD;
	public Text userMajorText;

	public Text physAvgText;
	public Text mathAvgText;
	public Text chemAvgText;

	public Sprite greenCircle;
	public Sprite yellowCircle;
	public Sprite redCircle;

	public static int physAvg, mathAvg, chemAvg;

	public static int currentUserWeek;

	//public Image colorStatus;

	// Use this for initialization
	void Start () {
		
	}

	void OnEnable()
	{
		if ((loginManager.currentUser != "") && (loginManager.currentUser != null))
			userNameText.text = loginManager.currentUser;
		else
			userNameText.text = "Usuário";

		string url = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/menu.php";

		string urlAvgs = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/getResultsAvg.php";

		WWWForm postForm = new WWWForm();

		WWWForm postFormAvgs = new WWWForm ();

		if (loginManager.currentUserID != null) {
			postForm.AddField("userID", loginManager.currentUserID);

			postFormAvgs.AddField("userID", loginManager.currentUserID);
		}


		Debug.Log( loginManager.currentUserID);

		WWW validationPage = new WWW(url, postForm); // + "?usuario=" + usuario + "&senha=" + senha);

		WWW validationPageAvgs = new WWW(urlAvgs, postFormAvgs);

		StartCoroutine(getUserInfo(validationPage));
		StartCoroutine(getUserAvgs(validationPageAvgs));

	}

	public void updateName()
	{
		OnEnable ();
	}



	IEnumerator getUserAvgs(WWW validationPage)
	{
		float timeout = 10f;
		float waitingTime = 0f;

		while (!validationPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {
			//Debug.Log("timeout, fuck");
			errorFeedback.text = "Problemas para conectar. Por favor cheque sua conexão à internet.";
			yield return null;
		}
		else {
			//Debug.Log("yeah that's the spirit");

			Debug.Log("error = " + validationPage.error);
			Debug.Log("text = " + validationPage.text);

			if (!string.IsNullOrEmpty(validationPage.error))
			{
				if (validationPage.error == "404: Not Found")
				{
					errorFeedback.text = "Servidor indisponível no momento.\n Por favor, tente novamente mais tarde.";
				}

				else
				{
					errorFeedback.text = "Ocorreu um erro inesperado.\n Por favor, cheque sua conexão.";
				}

				yield break;
			}



			int resultPage;

			if (validationPage.text.Contains("§"))
			{
				Debug.Log (validationPage.text);

				//currentUserID = Regex.Match(validationPage.text, @"\d+").Value;

				string physAvgStr = Regex.Match(validationPage.text, @"(.*)\§(.*)\§(.*)").Groups[1].Value;
				string mathAvgStr = Regex.Match(validationPage.text, @"(.*)\§(.*)\§(.*)").Groups[2].Value;
				string chemAvgStr = Regex.Match(validationPage.text, @"(.*)\§(.*)\§(.*)").Groups[3].Value;



				physAvg = int.Parse (physAvgStr);

				if (physAvg > 80) {
					physAvgText.transform.GetChild (0).GetComponent<Image> ().sprite = greenCircle;
					physAvgText.text = "Fís: Avançado (" + physAvgStr + "%)";
				} else if (physAvg >= 35) {
					physAvgText.transform.GetChild (0).GetComponent<Image> ().sprite = yellowCircle;
					physAvgText.text = "Fís: Intermediário (" +physAvgStr+"%)";
				}
				else
				{
					physAvgText.transform.GetChild (0).GetComponent<Image> ().sprite = redCircle;
					physAvgText.text = "Fís: Básico (" +physAvgStr+"%)";
				}

				mathAvg = int.Parse (mathAvgStr);

				if (mathAvg>80)
				{
					mathAvgText.transform.GetChild (0).GetComponent<Image> ().sprite = greenCircle;
					mathAvgText.text = "Mat: Avançado (" +mathAvgStr+"%)";
				}
				else if (mathAvg>=35)
				{
					mathAvgText.transform.GetChild (0).GetComponent<Image> ().sprite = yellowCircle;
					mathAvgText.text = "Mat: Intermediário (" +mathAvgStr+"%)";
				}
				else
				{
					mathAvgText.transform.GetChild (0).GetComponent<Image> ().sprite = redCircle;
					mathAvgText.text = "Mat: Básico (" +mathAvgStr+"%)";
				}

				chemAvg = int.Parse (chemAvgStr);

				if (chemAvg > 80) {
					chemAvgText.transform.GetChild (0).GetComponent<Image> ().sprite = greenCircle;
					chemAvgText.text = "Quí: Avançado (" + chemAvgStr + "%)";
				} else if (chemAvg >= 35) {
					chemAvgText.transform.GetChild (0).GetComponent<Image> ().sprite = yellowCircle;
					chemAvgText.text = "Quí: Intermediário (" + chemAvgStr + "%)";
				} else {
					chemAvgText.transform.GetChild (0).GetComponent<Image> ().sprite = redCircle;
					chemAvgText.text = "Quí: Básico (" + chemAvgStr + "%)";
				}




			}
			else if (!int.TryParse(validationPage.text, out resultPage))
			{
				errorFeedback.text = "Ocorreu um erro inesperado. Por favor, cheque sua conexão.";
			}
			else
			{
				switch (resultPage)
				{
				case 3:
					//else if(www.text=="0"){ <- Original, mas não funcionou
				errorFeedback.text = "Usuário inválido";
					break;

				case 2:
				errorFeedback.text = "Servidor indisponível!";
					break;

				default:
					errorFeedback.text = "Ocorreu um erro inesperado.";
					//FeedBackError (www.text);
					break;
				}

				yield break;
			}




		}
	}


	IEnumerator getUserInfo(WWW validationPage)
	{
		float timeout = 10f;
		float waitingTime = 0f;
		//loginButton.interactable = false;

		// LaunchFeedbackMessage("Carregando...", FeedbackMessageType.Info);
		//Debug.Log("waiting for validation");

		while (!validationPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {
			//Debug.Log("timeout, fuck");
			errorFeedback.text = "Problemas para conectar. Por favor cheque sua conexão à internet.";
			yield return null;
		}
		else {
			//Debug.Log("yeah that's the spirit");

			Debug.Log("error = " + validationPage.error);
			Debug.Log("text = " + validationPage.text);

			if (!string.IsNullOrEmpty(validationPage.error))
			{
				if (validationPage.error == "404: Not Found")
				{
					errorFeedback.text = "Servidor indisponível no momento.\n Por favor, tente novamente mais tarde.";
				}

				else
				{
					errorFeedback.text = "Ocorreu um erro inesperado.\n Por favor, cheque sua conexão.";
				}

				yield break;
			}



			int resultPage;

			if (validationPage.text!="")
			{
				Debug.Log (validationPage.text);

				//currentUserID = Regex.Match(validationPage.text, @"\d+").Value;

				string userName = Regex.Match(validationPage.text, @"(.*)\$(.*)\$(.*)\$(.*)").Groups[1].Value;
				string userArea = Regex.Match(validationPage.text, @"(.*)\$(.*)\$(.*)\$(.*)").Groups[2].Value;
				string userMajor = Regex.Match(validationPage.text, @"(.*)\$(.*)\$(.*)\$(.*)").Groups[3].Value;
				string currentWeek = Regex.Match(validationPage.text, @"(.*)\$(.*)\$(.*)\$(.*)").Groups[4].Value;

				currentUserWeek = int.Parse (currentWeek);

				Debug.Log ("name = " +userName);
				Debug.Log ("userArea = " +userArea);
				Debug.Log ("userMajor = " +userMajor);
				Debug.Log ("currentWeek = " +currentWeek);

				if(userName !="")
					userNameText.text = userName;


				for (int i = 0; i < 8; i++) {
					Debug.Log ("looking for " + userArea + " and found " + userAreaDD.options [i].text);
					if (userArea == userAreaDD.options [i].text) {
						userAreaDD.value = i;
					}
				}

				userAreaDD.GetComponent<majorDropdownController> ().manualUpdate();

				int cont = 0;

				foreach (var option in userMajorDD.options) {

					Debug.Log ("looking for " + userMajor + " and found " + option.text);

					if (userMajor == userMajorDD.options [cont].text) {
						userMajorDD.value = cont;
					}

					cont++;
				}


				/*for (int i = 0; i < userMajorDD; i++) {
					Debug.Log ("looking for " + userArea + " and found " + userMajorDD.options [i].text);
					if (userMajor == userMajorDD.options [i].text) {
						userMajorDD.value = i;
					}
				}*/

				//userAreaText.text = userArea;
				//userMajorText.text = userMajor;



			}
			else if (!int.TryParse(validationPage.text, out resultPage))
			{
				errorFeedback.text = "Ocorreu um erro inesperado. Por favor, cheque sua conexão.";
			}
			else
			{
				switch (resultPage)
				{
				case 3:
					//else if(www.text=="0"){ <- Original, mas não funcionou
					errorFeedback.text = "Servidor indisponível!";
					break;

				case 2:
					errorFeedback.text = "Usuário inválido";
					break;

				default:
					errorFeedback.text = "Ocorreu um erro inesperado.";
					//FeedBackError (www.text);
					break;
				}

				yield break;
			}




		}
	}




	
	// Update is called once per frame
	void Update () {
		
	}


}
