﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class openResponseTextField : MonoBehaviour {


	public GameObject hideableContent;

	// Use this for initialization
	void Start () {

		hideableContent.SetActive(false);

		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void openTextField()
	{
		hideableContent.SetActive(true);

	}



	public void closeTextField()
	{
		hideableContent.SetActive(false);

	}
}
