﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class questionController : MonoBehaviour {

	public GameObject finalAnswersPanel;
	public GameObject statusPanel;
	public GameObject questionPanel;
	public GameObject answerPanel;
	public GameObject finalStatusPanel;

	// Use this for initialization
	void Start () {
		finalAnswersPanel.SetActive (false);
		finalStatusPanel.SetActive (false);
		statusPanel.SetActive (true);
		answerPanel.SetActive (true);
		questionPanel.SetActive (true);

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void endQuestionnaire()
	{
		finalStatusPanel.SetActive (true);
		finalAnswersPanel.SetActive (true);
		statusPanel.SetActive (false);
		answerPanel.SetActive (false);
		questionPanel.SetActive (false);
	}

	public void checkQuestion()
	{	
		finalStatusPanel.SetActive (false);
		finalAnswersPanel.SetActive (false);
		statusPanel.SetActive (true);
		answerPanel.SetActive (true);
		questionPanel.SetActive (true);
	}

}
