﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class updateNameLastName : MonoBehaviour {

	public Text newNameText, newLastNameText, errorFeedback;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void updateNameLastNameDB()
	{


		string url = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/updateNameLastName.php";

		WWWForm postForm = new WWWForm();
		postForm.AddField("userID", loginManager.currentUserID);
		postForm.AddField("name",  newNameText.text);
		postForm.AddField("lastName", newLastNameText.text);


		WWW validationPage = new WWW(url, postForm); // + "?usuario=" + usuario + "&senha=" + senha);


		StartCoroutine(updateDB(validationPage));


	}


	IEnumerator updateDB(WWW validationPage)
	{
		float timeout = 10f;
		float waitingTime = 0f;
		//loginButton.interactable = false;

		// LaunchFeedbackMessage("Carregando...", FeedbackMessageType.Info);
		//Debug.Log("waiting for validation");

		while (!validationPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {
			//Debug.Log("timeout, fuck");
			Debug.Log( "Problemas para conectar. Por favor cheque sua conexão à internet.");
			yield return null;
		}
		else {
			//Debug.Log("yeah that's the spirit");

			Debug.Log("error = " + validationPage.error);
			Debug.Log("text = " + validationPage.text);

			if (!string.IsNullOrEmpty(validationPage.error))
			{
				if (validationPage.error == "404: Not Found")
				{
					Debug.Log("Servidor indisponível no momento.\n Por favor, tente novamente mais tarde.");
				}

				else
				{
					Debug.Log("Ocorreu um erro inesperado.\n Por favor, cheque sua conexão.");
				}

				yield break;
			}



			int resultPage;

			if (validationPage.text.Contains("sucesso"))
			{
				FeedBackOk ("Nome/Sobrenome atualizados!");

			}
			else if (!int.TryParse(validationPage.text, out resultPage))
			{
				Debug.Log("Ocorreu um erro inesperado. Por favor, cheque sua conexão.");
			}
			else
			{
				switch (resultPage)
				{
				case 3:
					//else if(www.text=="0"){ <- Original, mas não funcionou
					Debug.Log("Nenhuma info nova");
					break;

				case 2:
					Debug.Log("Usuário inválido");
					break;

				default:
					Debug.Log("Ocorreu um erro inesperado.");
					//FeedBackError (www.text);
					break;
				}

				yield break;
			}




		}



	}

	void FeedBackOk(string mensagem)
	{
		errorFeedback.CrossFadeAlpha(100f, 1f, false);
		errorFeedback.color = new Color(0.13f, 0.5f, 0.83f);
		errorFeedback.text = mensagem;
		errorFeedback.CrossFadeAlpha(0f, 3f, false);
	}

	void FeedBackError(string mensagem)
	{
		errorFeedback.CrossFadeAlpha(0f, 1f, false);
		errorFeedback.CrossFadeAlpha(100f, 2f, false);
		errorFeedback.color = new Color(0.878f, 0.043f, 0f);
		errorFeedback.text = mensagem;
		errorFeedback.CrossFadeAlpha(0f, 4f, false);
	}
}
