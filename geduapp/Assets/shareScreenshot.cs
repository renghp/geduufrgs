﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;


public class shareScreenshot : MonoBehaviour {

	private bool m_screenShotLock = false;

	//function called from a button
	public void ButtonShare ()
	{


		m_screenShotLock = true;
		StartCoroutine(TakeScreenShotCo());
	}

	private IEnumerator TakeScreenShotCo()
	{
		yield return new WaitForEndOfFrame();

		/* var directory = new DirectoryInfo(Application.dataPath);
         var path = Path.Combine(directory.Parent.FullName, string.Format("Screenshot_{0}.png", System.DateTime.Now.ToString("yyyyMMdd_Hmmss")));
         Debug.Log("Taking screenshot to " + path);
         ScreenCapture.CaptureScreenshot(path);*/

		Texture2D ss = new Texture2D( Screen.width, Screen.height, TextureFormat.RGB24, false );
		ss.ReadPixels( new Rect( 0, 0, Screen.width, Screen.height ), 0, 0 );
		ss.Apply();

		string filePath = Path.Combine( Application.temporaryCachePath, string.Format("Captura{0}.png", System.DateTime.Now.ToString("yyyyMMdd_Hmmss")) );
		File.WriteAllBytes( filePath, ss.EncodeToPNG() );

		// To avoid memory leaks
		Destroy( ss );


		new NativeShare().AddFile( filePath ).SetSubject( "Meus Resultados g.edu" ).SetText( "Rumo à UFRGS com a g.edu!" ).Share();



		m_screenShotLock = false;

	}





}