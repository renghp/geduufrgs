﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class screenRotationController : MonoBehaviour {

	Scene m_Scene;

	// Use this for initialization
	void Start () {
	



	}
	
	// Update is called once per frame
	void Update () {
		m_Scene = SceneManager.GetActiveScene();

		if (m_Scene.name == "simulationScene") {
			Screen.orientation = ScreenOrientation.AutoRotation;
		}
		else
			Screen.orientation = ScreenOrientation.Portrait;
			
	}
}
