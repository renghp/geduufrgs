﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class subjectSelector : MonoBehaviour {

	public Text subjectTitle;


	List<int> [] subScores = new List<int>[9];
	public Text physAvgText, mathAvgText, chemAvgText;
	public Text physLastAvgText, mathLastAvgText, chemLastAvgText;
	public Text physRecentAvgText, mathRecentAvgText, chemRecentAvgText;

	public Text errorFeedback;

/*	List<float> chePercentages = new List<float> (new float[] {70, 80, 25, 90});		//get these
	List<float> phyPercentages = new List<float> (new float[] {10, 50, 77, 100});		//from the
	List<float> matPercentages = new List<float> (new float[] {80, 10, 50, 50});		//database
	List<float> bioPercentages = new List<float> (new float[] {80, 10, 50, 50});*/

	float subAvg=0;


	public Transform clonableMarker;


	// Use this for initialization
	void Start () {

		subScores[0] = new List<int>();		//phys
		subScores[1] = new List<int>();		//math
		subScores[2] = new List<int>();		//chem

		physAvgText.text = profileController.physAvg.ToString () + "%";
		mathAvgText.text = profileController.mathAvg.ToString () + "%";
		chemAvgText.text = profileController.chemAvg.ToString () + "%";

		/*subScores[0] = new List<float> (new float[] {70, 80, 25, 90});
		subScores[1] = new List<float> (new float[] {10, 50, 77, 100});
		subScores[2] = new List<float> (new float[] {80, 10, 50, 50});
		subScores[3] = new List<float> (new float[] {88, 17, 42, 86});
		subScores[4] = new List<float> (new float[] {7, 45, 30, 90});
		subScores[5] = new List<float> (new float[] {50, 78, 77, 54});
		subScores[6] = new List<float> (new float[] {80, 46, 88, 10});
		subScores[7] = new List<float> (new float[] {56, 41, 99, 86});
		subScores[8] = new List<float> (new float[] {88, 55, 77, 100});*/

		string urlResultsHistory = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/getResultsHistory.php";

		WWWForm postFormResultsHistory = new WWWForm();

		postFormResultsHistory.AddField("userID", loginManager.currentUserID);



		WWW validationResultsHistory = new WWW(urlResultsHistory, postFormResultsHistory); // + "?usuario=" + usuario + "&senha=" + senha);


		StartCoroutine(getResultsHistory(validationResultsHistory));

	}

	IEnumerator getResultsHistory(WWW validationPage)
	{
		float timeout = 10f;
		float waitingTime = 0f;

		while (!validationPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {
			//Debug.Log("timeout, fuck");
			errorFeedback.text = "Problemas para conectar. Por favor cheque sua conexão à internet.";
			yield return null;
		}
		else {
			//Debug.Log("yeah that's the spirit");

			Debug.Log("error = " + validationPage.error);
			Debug.Log("text = " + validationPage.text);

			if (!string.IsNullOrEmpty(validationPage.error))
			{
				if (validationPage.error == "404: Not Found")
				{
					errorFeedback.text = "Servidor indisponível no momento.\n Por favor, tente novamente mais tarde.";
				}

				else
				{
					errorFeedback.text = "Ocorreu um erro inesperado.\n Por favor, cheque sua conexão.";
				}

				yield break;
			}



			int resultPage;

			if (validationPage.text.Contains("§"))
			{
				Debug.Log (validationPage.text);

				//currentUserID = Regex.Match(validationPage.text, @"\d+").Value;

				physRecentAvgText.text = Regex.Match(validationPage.text, @"recent:(\d+)\§(\d+)\§(\d+)\§last:(\d+)\§(\d+)\§(\d+)\§allPhys:(.*)\§allMath:(.*)\§allChem:(.*)").Groups[1].Value + "%";
				mathRecentAvgText.text = Regex.Match(validationPage.text, @"recent:(\d+)\§(\d+)\§(\d+)\§last:(\d+)\§(\d+)\§(\d+)\§allPhys:(.*)\§allMath:(.*)\§allChem:(.*)").Groups[2].Value + "%";
				chemRecentAvgText.text = Regex.Match(validationPage.text, @"recent:(\d+)\§(\d+)\§(\d+)\§last:(\d+)\§(\d+)\§(\d+)\§allPhys:(.*)\§allMath:(.*)\§allChem:(.*)").Groups[3].Value + "%";

				physLastAvgText.text = Regex.Match(validationPage.text, @"recent:(\d+)\§(\d+)\§(\d+)\§last:(\d+)\§(\d+)\§(\d+)\§allPhys:(.*)\§allMath:(.*)\§allChem:(.*)").Groups[4].Value + "%";
				mathLastAvgText.text = Regex.Match(validationPage.text, @"recent:(\d+)\§(\d+)\§(\d+)\§last:(\d+)\§(\d+)\§(\d+)\§allPhys:(.*)\§allMath:(.*)\§allChem:(.*)").Groups[5].Value + "%";
				chemLastAvgText.text = Regex.Match(validationPage.text, @"recent:(\d+)\§(\d+)\§(\d+)\§last:(\d+)\§(\d+)\§(\d+)\§allPhys:(.*)\§allMath:(.*)\§allChem:(.*)").Groups[6].Value + "%";

				string allPhys = Regex.Match(validationPage.text, @"recent:(\d+)\§(\d+)\§(\d+)\§last:(\d+)\§(\d+)\§(\d+)\§allPhys:(.*)\§allMath:(.*)\§allChem:(.*)").Groups[7].Value;
				string allMath = Regex.Match(validationPage.text, @"recent:(\d+)\§(\d+)\§(\d+)\§last:(\d+)\§(\d+)\§(\d+)\§allPhys:(.*)\§allMath:(.*)\§allChem:(.*)").Groups[8].Value;
				string allChem = Regex.Match(validationPage.text, @"recent:(\d+)\§(\d+)\§(\d+)\§last:(\d+)\§(\d+)\§(\d+)\§allPhys:(.*)\§allMath:(.*)\§allChem:(.*)").Groups[9].Value;



				Debug.Log (allPhys);

				while (allPhys != "") {
					subScores[0].Add(int.Parse(Regex.Match(allPhys, @"(\d+)\§?(.*)").Groups[1].Value));
					allPhys = Regex.Match (allPhys, @"(\d+)\§(.*)").Groups [2].Value;

				}

				while (allMath != "") {
					subScores[1].Add(int.Parse(Regex.Match(allMath, @"(\d+)\§?(.*)").Groups[1].Value));
					allMath = Regex.Match (allMath, @"(\d+)\§(.*)").Groups [2].Value;

				}

				while (allChem != "") {
					subScores[2].Add(int.Parse(Regex.Match(allChem, @"(\d+)\§?(.*)").Groups[1].Value));
					allChem = Regex.Match (allChem, @"(\d+)\§(.*)").Groups [2].Value;

				}



			}
			else if (!int.TryParse(validationPage.text, out resultPage))
			{
				errorFeedback.text = "Ocorreu um erro inesperado. Por favor, cheque sua conexão.";
			}
			else
			{
				switch (resultPage)
				{
				case 3:
					//else if(www.text=="0"){ <- Original, mas não funcionou
					errorFeedback.text = "Usuário inválido";
					break;

				case 2:
					errorFeedback.text = "Servidor indisponível!";
					break;

				default:
					errorFeedback.text = "Ocorreu um erro inesperado.";
					//FeedBackError (www.text);
					break;
				}

				yield break;
			}




		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	/*void printTotalAverages()
	{
		int cont = 0;
		foreach (List<float> ss in subScores) {

			subAvg = 0;

			foreach (float pct in ss)
				subAvg += pct;

			subAvg /= ss.Count;

			totalAverageText[cont].text = subAvg.ToString("0.00") + "%";
			cont++;
		}


	}*/

	public void selectSubject(int sub)
	{

		switch (sub) {
		case 0:
			subjectTitle.text = "Física";
			break;
		case 1:
			subjectTitle.text = "Matemática";
			break;
		case 2:
			subjectTitle.text = "Química";
			break;
		case 3:
			subjectTitle.text = "Biologia";
			break;
		case 4:
			subjectTitle.text = "Geografia";
			break;
		case 5:
			subjectTitle.text = "Literatura";
			break;
		case 6:
			subjectTitle.text = "Português";
			break;
		case 7:
			subjectTitle.text = "Língua Estrangeira";
			break;
		case 8:
			subjectTitle.text = "História";
			break;

		}


		fillGraph (subScores [sub]);


	}

	public void fillGraph(List<int> subPercentages)
	{
		GameObject [] deletableObjects = GameObject.FindGameObjectsWithTag ("toBeDeleted");

		foreach (GameObject delObj in deletableObjects)
		{
			Destroy(delObj);
		}

		int cont = 0;


		
		foreach (int per in subPercentages) {

			Debug.Log ("vai instanciar");

			Transform instantiatedGO = Instantiate(clonableMarker, new Vector3(0, 0, 0), Quaternion.identity);

			instantiatedGO.transform.SetParent (transform);
			instantiatedGO.tag = "toBeDeleted";


			instantiatedGO.transform.localPosition = new Vector3((-300f + cont*600f/subPercentages.Count), (((float)per-62.12f)/0.3f)-248f, 0);
			instantiatedGO.transform.localScale = new Vector3(1, 1, 1);

			cont++;

		}
		
	}
}
