﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class zoomInAnswerPanel : MonoBehaviour {

	public Text errorFeedback;
	public GameObject panel;
	public Text panelText;
	public RawImage answerImg;

	public static bool directed;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (answerImg.IsActive ()) {


			answerImg.SetNativeSize ();

			if (Screen.orientation == ScreenOrientation.Portrait)
				answerImg.rectTransform.sizeDelta = new Vector2 (700f, (700f / answerImg.rectTransform.sizeDelta.x) * answerImg.rectTransform.sizeDelta.y);



			panelText.rectTransform.offsetMax = new Vector2 (panelText.rectTransform.offsetMax.x, -answerImg.rectTransform.sizeDelta.y - 20f);
		}
		else
			panelText.rectTransform.offsetMax = new Vector2 (panelText.rectTransform.offsetMax.x, -20f);


	}

	public void openAnswerPanel(int option)
	{

		answerImg.texture = null;	//clears the image


		panel.SetActive(true);

		if (!transform.GetChild (0).GetComponent<Text> ().text.Contains ("Clique na lupa"))
			panelText.text = transform.GetChild (0).GetComponent<Text> ().text;
		else
			panelText.text = "Imagem acima";

		WWW imgPage = null;

		if ((getQuestionFromDB.imgURL != null) && (getQuestionFromDB.imgURL != "")) {
			imgPage = new WWW (getQuestionFromDB.imgURL);		//sets up the question image in case there is no image
		}

		if (!directed) {
			switch (option) {

			case 1:
				if ((getQuestionFromDB.AimgURL != null) && (getQuestionFromDB.AimgURL != "")) {

					imgPage = new WWW (getQuestionFromDB.AimgURL);

				}
				break;
			case 2:
				if ((getQuestionFromDB.BimgURL != null) && (getQuestionFromDB.BimgURL != "")) {

					imgPage = new WWW (getQuestionFromDB.BimgURL);

				}
				break;
			case 3:
				if ((getQuestionFromDB.CimgURL != null) && (getQuestionFromDB.CimgURL != "")) {

					imgPage = new WWW (getQuestionFromDB.CimgURL);

				}
				break;
			case 4:
				if ((getQuestionFromDB.DimgURL != null) && (getQuestionFromDB.DimgURL != "")) {

					imgPage = new WWW (getQuestionFromDB.DimgURL);

				}
				break;
			case 5:
				if ((getQuestionFromDB.EimgURL != null) && (getQuestionFromDB.EimgURL != "")) {

					imgPage = new WWW (getQuestionFromDB.EimgURL);

				}
				break;

			}
		} else {
			switch (option) {

			case 1:
				if ((getDirectedQuestionsDB.AimgURL != null) && (getDirectedQuestionsDB.AimgURL != "")) {

					imgPage = new WWW (getDirectedQuestionsDB.AimgURL);

				}
				break;
			case 2:
				if ((getDirectedQuestionsDB.BimgURL != null) && (getDirectedQuestionsDB.BimgURL != "")) {

					imgPage = new WWW (getDirectedQuestionsDB.BimgURL);

				}
				break;
			case 3:
				if ((getDirectedQuestionsDB.CimgURL != null) && (getDirectedQuestionsDB.CimgURL != "")) {

					imgPage = new WWW (getDirectedQuestionsDB.CimgURL);

				}
				break;
			case 4:
				if ((getDirectedQuestionsDB.DimgURL != null) && (getDirectedQuestionsDB.DimgURL != "")) {

					imgPage = new WWW (getDirectedQuestionsDB.DimgURL);

				}
				break;
			case 5:
				if ((getDirectedQuestionsDB.EimgURL != null) && (getDirectedQuestionsDB.EimgURL != "")) {

					imgPage = new WWW (getDirectedQuestionsDB.EimgURL);

				}
				break;

			}
		}


		if (imgPage != null) {
			answerImg.gameObject.SetActive (true);
			StartCoroutine (loadImage (imgPage));
		} else {
			answerImg.gameObject.SetActive (false);
		}

		
	}

	IEnumerator loadImage(WWW imgPage)
	{



		float timeout = 60f;
		float waitingTime = 0f;


		while (!imgPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {

			errorFeedback.text = "Imagem não encontrada, grande demais ou servidor offline :(";
			yield return null;
		} else {

			errorFeedback.text = "";

			answerImg.gameObject.SetActive (true);

			answerImg.texture = imgPage.texture;


		}
	}


}
