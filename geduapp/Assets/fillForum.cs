﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;


public class fillForum : MonoBehaviour {

	public GameObject solveButton, unsolveButton;

	public GameObject likeTopicButton, unlikeTopicButton;

	public Text errorFeedback;

	public Text topicTitle;

	public Transform clonableTopic;

	public GameObject singleTopicPanel;

	public Text singleTopicTitleText;

	public GameObject ogPost;

	public Transform clonableComment;


	public GameObject contentController;

	public GameObject ogUserAvatar;

	public GameObject topicsSizeablePanel;

	public static int currentTopic;

	public static int currentSubject;

	public Text titleTextField;
	public GameObject topicTextField;

	public GameObject postTextField;

	public GameObject editTextField;
	public GameObject sendButton;
	public GameObject invCloseButton;
	public GameObject editPostPanel;

	public GameObject writePostPanel, writePostTextField;
	public GameObject writeTopicPanel, writeTopicTextField, writeTopicTitleTextField;

	// Use this for initialization
	void Start () {

		editPostPanel.SetActive (false);
		solveButton.SetActive (true);
		unsolveButton.SetActive (false);

		likeTopicButton.SetActive (true);
		unlikeTopicButton.SetActive (false);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void callPHPForum(int subject)
	{
		GameObject [] deletableObjects = GameObject.FindGameObjectsWithTag ("toBeDeleted");

		foreach (GameObject delObj in deletableObjects)
		{
			Destroy(delObj);
		}

		currentSubject = subject;

		switch (subject) {
		case 1:
			topicTitle.text = "Física";
			break;
		case 2:
			topicTitle.text = "Matemática";
			break;
		case 3:
			topicTitle.text = "Química";
			break;
		default:
			topicTitle.text = "Diversas";
			break;
		}

		string url = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/getAllPostsBySubject.php";

		WWWForm postForm = new WWWForm();
		postForm.AddField("subjectID", subject);


		WWW validationPage = new WWW(url, postForm); // + "?usuario=" + usuario + "&senha=" + senha);


		StartCoroutine(getAllTopics(validationPage));

	}

	IEnumerator getAllTopics(WWW validationPage)
	{
		float timeout = 10f;
		float waitingTime = 0f;
		//loginButton.interactable = false;

		// LaunchFeedbackMessage("Carregando...", FeedbackMessageType.Info);
		//Debug.Log("waiting for validation");

		while (!validationPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {
			//Debug.Log("timeout, fuck");
			Debug.Log( "Problemas para conectar. Por favor cheque sua conexão à internet.");
			yield return null;
		}
		else {
			//Debug.Log("yeah that's the spirit");

			Debug.Log("error = " + validationPage.error);
			Debug.Log("text = " + validationPage.text);

			if (!string.IsNullOrEmpty(validationPage.error))
			{
				if (validationPage.error == "404: Not Found")
				{
					Debug.Log("Servidor indisponível no momento.\n Por favor, tente novamente mais tarde.");
				}

				else
				{
					Debug.Log("Ocorreu um erro inesperado.\n Por favor, cheque sua conexão.");
				}

				yield break;
			}



			int resultPage;

			if (validationPage.text.Length > 5)
			{
				//FeedBackOk (validationPage.text);
				parseAndFill(validationPage.text);
			}
			else if (!int.TryParse(validationPage.text, out resultPage))
			{

				GameObject [] deletableObjects = GameObject.FindGameObjectsWithTag ("toBeDeleted");

				foreach (GameObject delObj in deletableObjects)
				{
					Destroy(delObj);
				}

				FeedBackOk("Nenhuma questão foi postada ainda, mas não seja tímido, seja o primeiro! :)");
			}
			else
			{
				switch (resultPage)
				{
				case 3:
					//else if(www.text=="0"){ <- Original, mas não funcionou
					FeedBackError("Nenhuma info nova");
					break;

				case 2:
					FeedBackError("Usuário inválido");
					break;

				default:
					FeedBackError("Ocorreu um erro inesperado.");
					//FeedBackError (www.text);
					break;
				}

				yield break;
			}




		}



	}

	void openSingleTopic(int topicId)
	{



		singleTopicPanel.SetActive (true);


		string urlPost = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/getAPostAndCommentsByID.php";

		WWWForm postFormPost = new WWWForm();
		postFormPost.AddField("topicID", topicId);


		WWW validationPagePost = new WWW(urlPost, postFormPost); // + "?usuario=" + usuario + "&senha=" + senha);


		StartCoroutine(getATopicAndItsComments(validationPagePost));

		currentTopic = topicId;



	}

	public void  likePost(bool like, int postID, GameObject likeButton,  GameObject unlikeButton,  Text likesText)
	{

		string urlLikeTopic = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/likePost.php";

		WWWForm postFormLikeTopic = new WWWForm();
		postFormLikeTopic.AddField("postID", postID);

		int currentLikes;
		currentLikes = int.Parse(likesText.text);

		if (like) {
			postFormLikeTopic.AddField ("status", 1);
			likeButton.SetActive (false);
			unlikeButton.SetActive (true);

			currentLikes++;
			likesText.text = currentLikes.ToString ();

		} else {
			postFormLikeTopic.AddField ("status", 0);
			likeButton.SetActive (true);
			unlikeButton.SetActive (false);

			currentLikes--;
			likesText.text = currentLikes.ToString ();
		}


		WWW validationPageLikeTopic = new WWW(urlLikeTopic, postFormLikeTopic); // + "?usuario=" + usuario + "&senha=" + senha);


		StartCoroutine(likePostDB(validationPageLikeTopic));


	}

	IEnumerator likePostDB(WWW validationPage)
	{
		float timeout = 10f;
		float waitingTime = 0f;
		//loginButton.interactable = false;

		// LaunchFeedbackMessage("Carregando...", FeedbackMessageType.Info);
		//Debug.Log("waiting for validation");

		while (!validationPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {
			//Debug.Log("timeout, fuck");
			Debug.Log( "Problemas para conectar. Por favor cheque sua conexão à internet.");
			yield return null;
		}
		else {
			//Debug.Log("yeah that's the spirit");

			Debug.Log("error = " + validationPage.error);
			Debug.Log("text = " + validationPage.text);

			if (!string.IsNullOrEmpty(validationPage.error))
			{
				if (validationPage.error == "404: Not Found")
				{
					Debug.Log("Servidor indisponível no momento.\n Por favor, tente novamente mais tarde.");
				}

				else
				{
					Debug.Log("Ocorreu um erro inesperado.\n Por favor, cheque sua conexão.");
				}

				yield break;
			}



			int resultPage;

			if (!int.TryParse(validationPage.text, out resultPage))
			{
				FeedBackError("Ocorreu um erro inesperado.");
			}
			else
			{

				int currentLikes;

				switch (resultPage)
				{
				case 0:
					FeedBackOk ("Postagem descurtida!");

					/*likeTopicButton.SetActive (true);
					unlikeTopicButton.SetActive (false);

					currentLikes = int.Parse (unlikeTopicButton.transform.parent.GetChild (unlikeTopicButton.transform.GetSiblingIndex() + 1).GetChild (0).GetComponent<Text> ().text);

					currentLikes--;

					unlikeTopicButton.transform.parent.GetChild (unlikeTopicButton.transform.GetSiblingIndex() + 1).GetChild (0).GetComponent<Text> ().text = currentLikes.ToString ();*/

					break;
				case 1:
					FeedBackOk("Postagem curtida!");

					/*likeTopicButton.SetActive (false);
					unlikeTopicButton.SetActive (true);

					currentLikes = int.Parse (unlikeTopicButton.transform.parent.GetChild (unlikeTopicButton.transform.GetSiblingIndex() + 1).GetChild (0).GetComponent<Text> ().text);

					currentLikes++;

					unlikeTopicButton.transform.parent.GetChild (unlikeTopicButton.transform.GetSiblingIndex() + 1).GetChild (0).GetComponent<Text> ().text = currentLikes.ToString ();*/



					break;
				case 2:
					FeedBackError("Tópico inválido");
					break;
				case 3:
					FeedBackError("Requerimento inválido");
					break;
				case 4:
					FeedBackError("Tópico não encontrado");
					break;
				default:
					FeedBackError("Ocorreu um erro inesperado.");
					//FeedBackError (www.text);
					break;
				}

				callPHPForum (currentSubject);

				yield break;
			}


		}



	}

	public void  postComment()
	{

		string urlpostTopic = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/postPost.php";

		WWWForm postFormpostTopic = new WWWForm();


		Debug.Log ("full text: " + topicTextField.GetComponent<InputField>().text);

		postFormpostTopic.AddField("studentID", loginManager.currentUserID.ToString());
		postFormpostTopic.AddField("topicID", currentTopic.ToString());

		postFormpostTopic.AddField("topicText", postTextField.GetComponent<InputField>().text);



		WWW validationPagepostTopic = new WWW(urlpostTopic, postFormpostTopic);


		StartCoroutine(postCommentDB(validationPagepostTopic));


	}

	IEnumerator postCommentDB(WWW validationPage)
	{
		float timeout = 10f;
		float waitingTime = 0f;
		//loginButton.interactable = false;

		// LaunchFeedbackMessage("Carregando...", FeedbackMessageType.Info);
		//Debug.Log("waiting for validation");

		while (!validationPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {
			//Debug.Log("timeout, fuck");
			Debug.Log( "Problemas para conectar. Por favor cheque sua conexão à internet.");
			yield return null;
		}
		else {
			//Debug.Log("yeah that's the spirit");

			Debug.Log("error = " + validationPage.error);
			Debug.Log("text = " + validationPage.text);

			if (!string.IsNullOrEmpty(validationPage.error))
			{
				if (validationPage.error == "404: Not Found")
				{
					Debug.Log("Servidor indisponível no momento.\n Por favor, tente novamente mais tarde.");
				}

				else
				{
					Debug.Log("Ocorreu um erro inesperado.\n Por favor, cheque sua conexão.");
				}

				yield break;
			}



			int resultPage;

			if (!int.TryParse(validationPage.text, out resultPage))
			{
				FeedBackError("Ocorreu um erro inesperado.");
			}
			else
			{

				int currentLikes;

				switch (resultPage)
				{

				case 1:
					FeedBackOk ("Comentário postado!");



					singleTopicPanel.SetActive (false);

					writePostPanel.SetActive (false);
					writePostTextField.GetComponent<InputField> ().text = "";

					callPHPForum (currentSubject);

					openSingleTopic (currentTopic);
					//contentController.GetComponent<scrollviewSizeController> ().organizeAnswerTextPositions ();


					break;
				case 2:
					FeedBackError("Usuário ou tópico não definidos");
					break;
				case 4:
					FeedBackError("Texto não definido");
					break;
				default:
					FeedBackError("Ocorreu um erro inesperado.");
					//FeedBackError (www.text);
					break;
				}



				yield break;
			}


		}



	}



	public void  postTopic()
	{

		string urlpostTopic = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/postTopic.php";

		WWWForm postFormpostTopic = new WWWForm();


		Debug.Log ("full text: " + topicTextField.GetComponent<InputField>().text);

		postFormpostTopic.AddField("studentID", loginManager.currentUserID.ToString());
		postFormpostTopic.AddField("subjectID", currentSubject.ToString());


		postFormpostTopic.AddField("title", titleTextField.text);


		postFormpostTopic.AddField("topicText", topicTextField.GetComponent<InputField>().text);



		WWW validationPagepostTopic = new WWW(urlpostTopic, postFormpostTopic);


		StartCoroutine(postTopicDB(validationPagepostTopic));


	}

	IEnumerator postTopicDB(WWW validationPage)
	{
		float timeout = 10f;
		float waitingTime = 0f;
		//loginButton.interactable = false;

		// LaunchFeedbackMessage("Carregando...", FeedbackMessageType.Info);
		//Debug.Log("waiting for validation");

		while (!validationPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {
			//Debug.Log("timeout, fuck");
			Debug.Log( "Problemas para conectar. Por favor cheque sua conexão à internet.");
			yield return null;
		}
		else {
			//Debug.Log("yeah that's the spirit");

			Debug.Log("error = " + validationPage.error);
			Debug.Log("text = " + validationPage.text);

			if (!string.IsNullOrEmpty(validationPage.error))
			{
				if (validationPage.error == "404: Not Found")
				{
					Debug.Log("Servidor indisponível no momento.\n Por favor, tente novamente mais tarde.");
				}

				else
				{
					Debug.Log("Ocorreu um erro inesperado.\n Por favor, cheque sua conexão.");
				}

				yield break;
			}



			int resultPage;

			if (!int.TryParse(validationPage.text, out resultPage))
			{
				FeedBackError("Ocorreu um erro inesperado.");
			}
			else
			{

				int currentLikes;

				switch (resultPage)
				{

				case 1:
					FeedBackOk ("Tópico postado!");



					writeTopicPanel.SetActive (false);
					writeTopicTextField.GetComponent<InputField> ().text = "";
					writeTopicTitleTextField.GetComponent<InputField> ().text = "";


					callPHPForum (currentSubject);

					break;
				case 2:
					FeedBackError("Usuário ou matéria não definidos");
					break;
				case 3:
					FeedBackError("Título não definido");
					break;
				case 4:
					FeedBackError("Texto não definido");
					break;
				default:
					FeedBackError("Ocorreu um erro inesperado.");
					//FeedBackError (www.text);
					break;
				}



				yield break;
			}


		}



	}


	public void  likeTopic(bool like)
	{

		string urlLikeTopic = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/likeTopic.php";

		WWWForm postFormLikeTopic = new WWWForm();
		postFormLikeTopic.AddField("topicID", currentTopic);

		if (like)
			postFormLikeTopic.AddField("status", 1);
		else
			postFormLikeTopic.AddField("status", 0);


		WWW validationPageLikeTopic = new WWW(urlLikeTopic, postFormLikeTopic); // + "?usuario=" + usuario + "&senha=" + senha);


		StartCoroutine(likeTopicDB(validationPageLikeTopic));


	}

	IEnumerator likeTopicDB(WWW validationPage)
	{
		float timeout = 10f;
		float waitingTime = 0f;
		//loginButton.interactable = false;

		// LaunchFeedbackMessage("Carregando...", FeedbackMessageType.Info);
		//Debug.Log("waiting for validation");

		while (!validationPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {
			//Debug.Log("timeout, fuck");
			Debug.Log( "Problemas para conectar. Por favor cheque sua conexão à internet.");
			yield return null;
		}
		else {
			//Debug.Log("yeah that's the spirit");

			Debug.Log("error = " + validationPage.error);
			Debug.Log("text = " + validationPage.text);

			if (!string.IsNullOrEmpty(validationPage.error))
			{
				if (validationPage.error == "404: Not Found")
				{
					Debug.Log("Servidor indisponível no momento.\n Por favor, tente novamente mais tarde.");
				}

				else
				{
					Debug.Log("Ocorreu um erro inesperado.\n Por favor, cheque sua conexão.");
				}

				yield break;
			}



			int resultPage;

			if (!int.TryParse(validationPage.text, out resultPage))
			{
				FeedBackError("Ocorreu um erro inesperado.");
			}
			else
			{

				int currentLikes;

				switch (resultPage)
				{
				case 0:
					FeedBackOk ("Tópico descurtido!");

					likeTopicButton.SetActive (true);
					unlikeTopicButton.SetActive (false);

					currentLikes = int.Parse (unlikeTopicButton.transform.parent.GetChild (unlikeTopicButton.transform.GetSiblingIndex() + 1).GetChild (0).GetComponent<Text> ().text);

					currentLikes--;

					unlikeTopicButton.transform.parent.GetChild (unlikeTopicButton.transform.GetSiblingIndex() + 1).GetChild (0).GetComponent<Text> ().text = currentLikes.ToString ();

					break;
				case 1:
					FeedBackOk("Tópico curtido!");

					likeTopicButton.SetActive (false);
					unlikeTopicButton.SetActive (true);

					currentLikes = int.Parse (unlikeTopicButton.transform.parent.GetChild (unlikeTopicButton.transform.GetSiblingIndex() + 1).GetChild (0).GetComponent<Text> ().text);

					currentLikes++;

					unlikeTopicButton.transform.parent.GetChild (unlikeTopicButton.transform.GetSiblingIndex() + 1).GetChild (0).GetComponent<Text> ().text = currentLikes.ToString ();



					break;
				case 2:
					FeedBackError("Tópico inválido");
					break;
				case 3:
					FeedBackError("Requerimento inválido");
					break;
				case 4:
					FeedBackError("Tópico não encontrado");
					break;
				default:
					FeedBackError("Ocorreu um erro inesperado.");
					//FeedBackError (www.text);
					break;
				}

				callPHPForum (currentSubject);

				yield break;
			}


		}



	}



	public void  setTopicAsResolved(bool resolved)
	{

		string urlResolved = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/setTopicAsResolved.php";

		WWWForm postFormResolved = new WWWForm();
		postFormResolved.AddField("topicID", currentTopic);

		if (resolved)
			postFormResolved.AddField("status", 1);
		else
			postFormResolved.AddField("status", 0);


		WWW validationPageResolved = new WWW(urlResolved, postFormResolved); // + "?usuario=" + usuario + "&senha=" + senha);


		StartCoroutine(setTopicResolvedDB(validationPageResolved));


	}

	IEnumerator setTopicResolvedDB(WWW validationPage)
	{
		float timeout = 10f;
		float waitingTime = 0f;
		//loginButton.interactable = false;

		// LaunchFeedbackMessage("Carregando...", FeedbackMessageType.Info);
		//Debug.Log("waiting for validation");

		while (!validationPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {
			//Debug.Log("timeout, fuck");
			Debug.Log( "Problemas para conectar. Por favor cheque sua conexão à internet.");
			yield return null;
		}
		else {
			//Debug.Log("yeah that's the spirit");

			Debug.Log("error = " + validationPage.error);
			Debug.Log("text = " + validationPage.text);

			if (!string.IsNullOrEmpty(validationPage.error))
			{
				if (validationPage.error == "404: Not Found")
				{
					Debug.Log("Servidor indisponível no momento.\n Por favor, tente novamente mais tarde.");
				}

				else
				{
					Debug.Log("Ocorreu um erro inesperado.\n Por favor, cheque sua conexão.");
				}

				yield break;
			}



			int resultPage;

			if (!int.TryParse(validationPage.text, out resultPage))
			{
				FeedBackError("Ocorreu um erro inesperado.");
			}
			else
			{
				switch (resultPage)
				{
					case 0:
						FeedBackOk("Tópico marcado como não resolvido!");

						solveButton.SetActive (true);
						unsolveButton.SetActive (false);


						break;
					case 1:
						FeedBackOk("Tópico marcado como resolvido!");

						solveButton.SetActive (false);
						unsolveButton.SetActive (true);
					

						break;
					case 2:
						FeedBackError("Tópico inválido");
						break;
					case 3:
						FeedBackError("Requerimento inválido");
						break;
					case 4:
						FeedBackError("Tópico não encontrado");
						break;
					default:
						FeedBackError("Ocorreu um erro inesperado.");
						//FeedBackError (www.text);
						break;
				}

				callPHPForum (currentSubject);

				yield break;
			}
				

		}



	}

	IEnumerator getATopicAndItsComments(WWW validationPage)
	{
		float timeout = 10f;
		float waitingTime = 0f;
		//loginButton.interactable = false;

		// LaunchFeedbackMessage("Carregando...", FeedbackMessageType.Info);
		//Debug.Log("waiting for validation");

		while (!validationPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {
			//Debug.Log("timeout, fuck");
			Debug.Log( "Problemas para conectar. Por favor cheque sua conexão à internet.");
			yield return null;
		}
		else {
			//Debug.Log("yeah that's the spirit");

			Debug.Log("error = " + validationPage.error);
			Debug.Log("text = " + validationPage.text);

			if (!string.IsNullOrEmpty(validationPage.error))
			{
				if (validationPage.error == "404: Not Found")
				{
					Debug.Log("Servidor indisponível no momento.\n Por favor, tente novamente mais tarde.");
				}

				else
				{
					Debug.Log("Ocorreu um erro inesperado.\n Por favor, cheque sua conexão.");
				}

				yield break;
			}



			int resultPage;

			if (validationPage.text.Length > 5)
			{
				//contentController.GetComponent<scrollviewSizeController> ().organizeAnswerTextPositions ();
				parseAndFillAFullTopic(validationPage.text);
			}
			else if (!int.TryParse(validationPage.text, out resultPage))
			{

				GameObject [] deletableObjects = GameObject.FindGameObjectsWithTag ("postToBeDeleted");

				foreach (GameObject delObj in deletableObjects)
				{
					Destroy(delObj);
				}



				FeedBackOk("Essa postagem parece ter sido deletada pelo usuário :(");
			}
			else
			{
				switch (resultPage)
				{
					case 2:
						FeedBackError("Tópico inválido");
						break;

					case 4:
						FeedBackError("Tópico não encontrado");
						break;

					default:
						FeedBackError("Ocorreu um erro inesperado.");
						//FeedBackError (www.text);
						break;
				}

				yield break;
			}




		}



	}


	void parseAndFillAFullTopic(string phpResult)
	{

		Debug.Log(phpResult);

		GameObject [] deletableObjects = GameObject.FindGameObjectsWithTag ("postToBeDeleted");

		foreach (GameObject delObj in deletableObjects)
		{
			Destroy(delObj);
		}





		string toBeParsed = phpResult;

		int cont = 0;


		////////////////////////////////////////////// ORIGINAL POSTER vvvvvvvvvv///////////////////////////////////////////

		int ogUserID = int.Parse(Regex.Match(toBeParsed, @"(\d+)\§(.*)").Groups[1].Value);
		toBeParsed = Regex.Match(toBeParsed, @"(\d+)\§(.*)").Groups[2].Value;

		string title = Regex.Match (toBeParsed, @"([^§]+)\§(.*)").Groups [1].Value;
		toBeParsed = Regex.Match (toBeParsed, @"([^§]+)\§(.*)").Groups [2].Value;

		int solved = int.Parse(Regex.Match (toBeParsed, @"(\d+)\§(.*)").Groups [1].Value);
		toBeParsed = Regex.Match (toBeParsed, @"(\d+)\§(.*)").Groups [2].Value;

		if (solved==1) {
			solveButton.SetActive (false);
			unsolveButton.SetActive (true);
		}
		else {
			solveButton.SetActive (true);
			unsolveButton.SetActive (false);
		}

		string ogDate = Regex.Match (toBeParsed, @"([^§]+)\§(.*)").Groups [1].Value;
		toBeParsed = Regex.Match (toBeParsed, @"([^§]+)\§(.*)").Groups [2].Value;


		int ogLikes = int.Parse(Regex.Match (toBeParsed, @"(\d+)\§(.*)").Groups [1].Value);
		toBeParsed = Regex.Match (toBeParsed, @"(\d+)\§(.*)").Groups [2].Value;

		Debug.Log("numero de likes: " + ogLikes);

		string ogPostText = Regex.Match (toBeParsed, @"([^§]+)\§?(.*)").Groups [1].Value;
		toBeParsed = Regex.Match (toBeParsed, @"([^§]+)\§?(.*)").Groups [2].Value;

		ogPost.transform.GetChild (0).GetChild (0).GetChild (0).GetComponent<Text> ().text = ogUserID.ToString ();

		getUserName(ogUserID, ogPost.transform.GetChild (0).GetChild (0).GetChild (0).GetComponent<Text> ());


		ogPost.transform.GetChild (1).GetChild (1).GetChild (5).GetChild (0).GetComponent<Text> ().text = ogLikes.ToString();


		ogPost.transform.GetChild (0).GetChild (1).GetChild (0).GetComponent<Text> ().text = ogDate;

		Debug.Log ("texto do tópico: " + ogPostText);
		ogPost.transform.GetChild (1).GetChild (0).GetComponent<Text> ().text = ogPostText;

		singleTopicTitleText.GetComponent<Text> ().text = title;

		updatePic(ogUserID, ogUserAvatar);


		if ((loginManager.currentUserID != null) && (ogUserID == int.Parse(loginManager.currentUserID))) {
			ogPost.transform.GetChild (1).GetChild (1).GetChild (0).gameObject.SetActive (true);
			ogPost.transform.GetChild (1).GetChild (1).GetChild (0).GetComponent<Button>().onClick.AddListener(() => openEditTextFieldWithPriorText(ogPostText, currentTopic, true));
		}
		else
			ogPost.transform.GetChild (1).GetChild (1).GetChild (0).gameObject.SetActive (false);





		////////////////////////////////////////////// ORIGINAL POSTER ^^^^^^^///////////////////////////////////////////
		////////////////////////////////////////////// COMMENTERS vvvvvvvvvv///////////////////////////////////////////

		while ((toBeParsed != null) && (toBeParsed != "")) {

			Debug.Log (toBeParsed);

			int postID = int.Parse(Regex.Match(toBeParsed, @"(\d+)\§(.*)").Groups[1].Value);
			toBeParsed = Regex.Match(toBeParsed, @"(\d+)\§(.*)").Groups[2].Value;

			string postText = Regex.Match (toBeParsed, @"([^§]+)\§(.*)").Groups [1].Value;
			toBeParsed = Regex.Match (toBeParsed, @"([^§]+)\§(.*)").Groups [2].Value;

			int userID = int.Parse(Regex.Match (toBeParsed, @"(\d+)\§(.*)").Groups [1].Value);
			toBeParsed = Regex.Match (toBeParsed, @"(\d+)\§(.*)").Groups [2].Value;

			int likes = int.Parse(Regex.Match (toBeParsed, @"(\d+)\§(.*)").Groups [1].Value);
			toBeParsed = Regex.Match (toBeParsed, @"(\d+)\§(.*)").Groups [2].Value;



			string date = Regex.Match (toBeParsed, @"([^§]+)\§?(.*)").Groups [1].Value;
			toBeParsed = Regex.Match (toBeParsed, @"([^§]+)\§?(.*)").Groups [2].Value;




			////////////////////

			Transform instantiatedGO = Instantiate(clonableComment, clonableComment.transform.parent);

			instantiatedGO.tag = "postToBeDeleted";
			instantiatedGO.gameObject.SetActive (true);
			//instantiatedGO.transform.localPosition = new Vector2(instantiatedGO.transform.localPosition.x, instantiatedGO.transform.localPosition.y -100f*cont);
			instantiatedGO.transform.localPosition = new Vector2(instantiatedGO.transform.localPosition.x, instantiatedGO.transform.localPosition.y);

			instantiatedGO.transform.localScale = new Vector3(1, 1, 1);

			getUserName(userID, instantiatedGO.GetChild (0).GetChild (0).GetChild (0).GetComponent<Text> ());


			instantiatedGO.GetChild (1).GetChild (1).GetChild (3).GetChild (0).GetComponent<Text> ().text = likes.ToString();

			instantiatedGO.GetChild (1).GetChild (1).GetChild (1).GetComponent<Button>().onClick.AddListener(() => likePost(true, postID, instantiatedGO.GetChild (1).GetChild (1).GetChild (1).gameObject, instantiatedGO.GetChild (1).GetChild (1).GetChild (2).gameObject, instantiatedGO.GetChild (1).GetChild (1).GetChild (3).GetChild (0).GetComponent<Text> ()));
			instantiatedGO.GetChild (1).GetChild (1).GetChild (2).GetComponent<Button>().onClick.AddListener(() => likePost(false, postID, instantiatedGO.GetChild (1).GetChild (1).GetChild (1).gameObject, instantiatedGO.GetChild (1).GetChild (1).GetChild (2).gameObject, instantiatedGO.GetChild (1).GetChild (1).GetChild (3).GetChild (0).GetComponent<Text> ()));


			instantiatedGO.GetChild (0).GetChild (1).GetChild (0).GetComponent<Text> ().text = date;

			Debug.Log ("date: " + date);

			instantiatedGO.GetChild (1).GetChild (0).GetComponent<Text> ().text = postText;


			if ((loginManager.currentUserID != null) && (userID == int.Parse(loginManager.currentUserID))) {
				instantiatedGO.GetChild (1).GetChild (1).GetChild (0).gameObject.SetActive (true);
				instantiatedGO.GetChild (1).GetChild (1).GetChild (0).GetComponent<Button> ().onClick.AddListener (() => openEditTextFieldWithPriorText (postText, postID, false));
			}
			else
				instantiatedGO.GetChild (1).GetChild (1).GetChild (0).gameObject.SetActive (false);


			updatePic(userID, instantiatedGO.GetChild (0).GetChild (2).GetChild (0).gameObject);


			//instantiatedGO.GetChild (3).GetChild (0).GetComponent<Text> ().text = "DB";				//aqui tem que ir o número de respostas (um select do banco contando todas os posts relacionados a esse topic)


			/*if (solved == 1) {
				instantiatedGO.GetComponent<Image> ().color = new Color (0f, 0.8745f, 0.4784f, 0.3921f);
			}
			else
				instantiatedGO.GetComponent<Image> ().color = new Color (0.2902f, 0.4745f, 0.596f, 0.3921f);

			instantiatedGO.GetComponent<Button>().onClick.AddListener(() => openSingleTopic(topicID));*/


			//	instantiatedGO.GetComponent<Button> ().onClick.AddListener (() =>checkQuestion (tempInt));			//botar um listener pra abrir o tópico em questão (painel singleTopicPanel

			//	Debug.Log ("instanciou um botão para ver a questão " + tempInt.ToString ());

			cont++;

			Debug.Log ("setting default GO index to: " + (cont + 1).ToString ());
			clonableComment.SetSiblingIndex(cont + 1);

			////////////////////////////////////////////// COMMENTERS ^^^^^^^^^^^^///////////////////////////////////////////


		}

		contentController.GetComponent<scrollviewSizeController> ().organizeAnswerTextPositions ();



	}

	void getUserName(int userID, Text usernameTextField)
	{


		string url = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/getUserNameByID.php";

		WWWForm postForm = new WWWForm();

		if (userID != null) 
			postForm.AddField("userID", userID);

		WWW validationPage = new WWW(url, postForm); // + "?usuario=" + usuario + "&senha=" + senha);


		StartCoroutine(getUserInfo(validationPage, usernameCallback => {

			//if (usernameCallback!= "")
			Debug.Log("userName in function: " + usernameCallback);
			usernameTextField.text = usernameCallback;
		}));





		
	}




	IEnumerator getUserInfo(WWW validationPage, System.Action<string> callback)
	{
		float timeout = 5f;
		float waitingTime = 0f;
		//loginButton.interactable = false;

		// LaunchFeedbackMessage("Carregando...", FeedbackMessageType.Info);
		//Debug.Log("waiting for validation");

		while (!validationPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {

			yield return null;
		}
		else {
			//Debug.Log("yeah that's the spirit");

			Debug.Log("error = " + validationPage.error);
			Debug.Log("text = " + validationPage.text);

			if (!string.IsNullOrEmpty(validationPage.error))
			{
				yield break;
			}

			if (validationPage.text!="")
			{
				Debug.Log (validationPage.text);

				//currentUserID = Regex.Match(validationPage.text, @"\d+").Value;

				string userName = validationPage.text;

				Debug.Log("userName in IEnumerator: " + userName);

				callback (userName);
				yield return null;




			}

		}
	}



	void parseAndFill(string phpResult)
	{

		GameObject [] deletableObjects = GameObject.FindGameObjectsWithTag ("toBeDeleted");

		foreach (GameObject delObj in deletableObjects)
		{
			Destroy(delObj);
		}



		string toBeParsed = phpResult;

		int cont = 0;

		while ((toBeParsed != null) && (toBeParsed != "")) {

			Debug.Log (toBeParsed);
			
			int topicID = int.Parse(Regex.Match(toBeParsed, @"(\d+)\§(.*)").Groups[1].Value);
			toBeParsed = Regex.Match(toBeParsed, @"(\d+)\§(.*)").Groups[2].Value;

			string title = Regex.Match (toBeParsed, @"([^§]+)\§(.*)").Groups [1].Value;
			toBeParsed = Regex.Match (toBeParsed, @"([^§]+)\§(.*)").Groups [2].Value;

			int solved = int.Parse(Regex.Match (toBeParsed, @"(\d+)\§(.*)").Groups [1].Value);
			toBeParsed = Regex.Match (toBeParsed, @"(\d+)\§(.*)").Groups [2].Value;

			int likes = int.Parse(Regex.Match (toBeParsed, @"(\d+)\§(.*)").Groups [1].Value);
			toBeParsed = Regex.Match (toBeParsed, @"(\d+)\§(.*)").Groups [2].Value;

			int visits = int.Parse(Regex.Match (toBeParsed, @"(\d+)\§(.*)").Groups [1].Value);
			toBeParsed = Regex.Match (toBeParsed, @"(\d+)\§(.*)").Groups [2].Value;

			int answers = int.Parse(Regex.Match (toBeParsed, @"(\d+)\§?(.*)").Groups [1].Value);
			toBeParsed = Regex.Match (toBeParsed, @"(\d+)\§?(.*)").Groups [2].Value;





			////////////////////
			Transform instantiatedGO = Instantiate(clonableTopic, clonableTopic.transform.parent);

			instantiatedGO.tag = "toBeDeleted";
			instantiatedGO.gameObject.SetActive (true);
			instantiatedGO.transform.localPosition = new Vector2(instantiatedGO.transform.localPosition.x, instantiatedGO.transform.localPosition.y -100f*cont);
			instantiatedGO.transform.localScale = new Vector3(1, 1, 1);

			instantiatedGO.GetChild (0).GetChild (0).GetComponent<Text> ().text = title;

			instantiatedGO.GetChild (1).GetChild (0).GetComponent<Text> ().text = visits.ToString();	

			instantiatedGO.GetChild (2).GetChild (0).GetComponent<Text> ().text = likes.ToString();

			instantiatedGO.GetChild (3).GetChild (0).GetComponent<Text> ().text = answers.ToString();				//aqui tem que ir o número de respostas (um select do banco contando todas os posts relacionados a esse topic)


			if (solved == 1) {
				instantiatedGO.GetComponent<Image> ().color = new Color (0f, 0.8745f, 0.4784f, 0.3921f);
			}
			else
				instantiatedGO.GetComponent<Image> ().color = new Color (0.2902f, 0.4745f, 0.596f, 0.3921f);

			instantiatedGO.GetComponent<Button>().onClick.AddListener(() => openSingleTopic(topicID));



				
			//	instantiatedGO.GetComponent<Button> ().onClick.AddListener (() =>checkQuestion (tempInt));			//botar um listener pra abrir o tópico em questão (painel singleTopicPanel

			//	Debug.Log ("instanciou um botão para ver a questão " + tempInt.ToString ());

			cont++;


			/// ////////////////////


		}

		topicsSizeablePanel.GetComponent<RectTransform> ().sizeDelta = new Vector2(0, cont*100+155);

			


			
	}

	void FeedBackOk(string mensagem)
	{
		errorFeedback.CrossFadeAlpha(100f, 1f, false);
		errorFeedback.color = new Color(0.13f, 0.5f, 0.83f);
		errorFeedback.text = mensagem;
		errorFeedback.CrossFadeAlpha(0f, 3f, false);
	}

	void FeedBackError(string mensagem)
	{
		errorFeedback.CrossFadeAlpha(0f, 1f, false);
		errorFeedback.CrossFadeAlpha(100f, 2f, false);
		errorFeedback.color = new Color(0.878f, 0.043f, 0f);
		errorFeedback.text = mensagem;
		errorFeedback.CrossFadeAlpha(0f, 4f, false);
	}



	public void updatePic(int userID, GameObject userAvatar)
	{
		
		string imgURL = "https://s3.amazonaws.com/gedunorthamerica/" + userID.ToString() + "userAvatar.png";


		WWW imgPage = new WWW (imgURL); // + "?usuario=" + usuario + "&senha=" + senha);


		StartCoroutine (loadImage (imgPage, textureCallback => {

			userAvatar.GetComponent<RawImage>().texture = textureCallback;

			userAvatar.GetComponent<RawImage>().SetNativeSize();

			if (textureCallback.width > textureCallback.height) {
				Debug.Log ("imagem horizontal");
				userAvatar.GetComponent<RawImage>().rectTransform.sizeDelta = new Vector2 ((100f / userAvatar.GetComponent<RawImage>().rectTransform.sizeDelta.y) * userAvatar.GetComponent<RawImage>().rectTransform.sizeDelta.x, 100f);

			} else {
				Debug.Log ("imagem vertical");
				userAvatar.GetComponent<RawImage>().rectTransform.sizeDelta = new Vector2 (100f, (100f / userAvatar.GetComponent<RawImage>().rectTransform.sizeDelta.x) * userAvatar.GetComponent<RawImage>().rectTransform.sizeDelta.y);
			}

		}     ));


	}
		

	IEnumerator loadImage(WWW imgPage, System.Action<Texture> callback)
	{

		float timeout = 30f;
		float waitingTime = 0f;


		while (!imgPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {

			yield return null;
		} else if ((imgPage.texture.height >10) && (imgPage.texture.width >10)){


			callback (imgPage.texture);
			yield return null;



		} else {
			yield return null;
		}
	}



	public void openEditTextFieldWithPriorText(string priorText, int postID, bool topic)
	{
		editPostPanel.SetActive (true);
		editTextField.SetActive(true);
		sendButton.SetActive(true);
		invCloseButton.SetActive(true);

		editTextField.GetComponent<InputField> ().text = priorText;

		sendButton.GetComponent<Button>().onClick.RemoveAllListeners();
		sendButton.GetComponent<Button>().onClick.AddListener(() => updatePost(postID, topic));

	}

	public void updatePost(int postID, bool topic)
	{

		Debug.Log ("###########clicou para editar o post " + postID + "############################################################");

		string textToBeSent = editTextField.GetComponent<InputField> ().text;


		string urlUpdatePost = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/updatePostOrTopic.php";

		WWWForm postFormUpdatePost = new WWWForm();
		postFormUpdatePost.AddField("postID", postID);

		if (topic)
			postFormUpdatePost.AddField("topic", 1);
		else
			postFormUpdatePost.AddField("topic", 0);

		postFormUpdatePost.AddField("newPostText", textToBeSent);



		WWW validationPageUpdatePost = new WWW(urlUpdatePost, postFormUpdatePost); // + "?usuario=" + usuario + "&senha=" + senha);


		StartCoroutine(updatePostDB(validationPageUpdatePost));

	}



	IEnumerator updatePostDB(WWW validationPage)
	{
		float timeout = 10f;
		float waitingTime = 0f;
		//loginButton.interactable = false;

		// LaunchFeedbackMessage("Carregando...", FeedbackMessageType.Info);
		//Debug.Log("waiting for validation");

		while (!validationPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {
			//Debug.Log("timeout, fuck");
			Debug.Log( "Problemas para conectar. Por favor cheque sua conexão à internet.");
			yield return null;
		}
		else {
			//Debug.Log("yeah that's the spirit");

			Debug.Log("error = " + validationPage.error);
			Debug.Log("text = " + validationPage.text);

			if (!string.IsNullOrEmpty(validationPage.error))
			{
				if (validationPage.error == "404: Not Found")
				{
					Debug.Log("Servidor indisponível no momento.\n Por favor, tente novamente mais tarde.");
				}

				else
				{
					Debug.Log("Ocorreu um erro inesperado.\n Por favor, cheque sua conexão.");
				}

				yield break;
			}



			int resultPage;

			if (!int.TryParse(validationPage.text, out resultPage))
			{
				FeedBackError("Ocorreu um erro inesperado.");
			}
			else
			{

				int currentLikes;

				switch (resultPage)
				{
				case 0:
					FeedBackOk ("Postagem editada!");


					closeEditTextField ();

					singleTopicPanel.SetActive (false);

					callPHPForum (currentSubject);

					openSingleTopic (currentTopic);

					break;
				case 2:
					FeedBackError("Tópico inválido");
					break;
				case 3:
					FeedBackError("Requerimento inválido");
					break;
				case 4:
					FeedBackError("Tópico não encontrado");
					break;
				default:
					FeedBackError("Ocorreu um erro inesperado.");
					//FeedBackError (www.text);
					break;
				}

				yield break;
			}


		}



	}

	public void closeEditTextField()
	{
		editPostPanel.SetActive (false);
		editTextField.SetActive(false);
		sendButton.SetActive(false);
		invCloseButton.SetActive(false);


	}











}
