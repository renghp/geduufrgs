﻿using UnityEngine;
using UnityEngine.UI;
using System;
//using System.Data;
using System.Collections; 
//using MySql.Data.MySqlClient; 
//using MySql.Data; 
using System.IO;
using System.Text.RegularExpressions;

public class loginManager : MonoBehaviour {

	/*public static MySqlConnection dbConnection;
	static string host = "gedudb.cl0noz68ozaa.sa-east-1.rds.amazonaws.com";  
	static string id = "geduAdmin";
	static string pwd = "Gedu2019";
	static string database = "gedu"; */

	public  GameObject username;
	public  GameObject password;

	public static string savedUsername, savedPassword;

	public Text errorFeedback;

	public static string currentUser;
	public static string currentUserID;

	void Start()
	{	

		Debug.Log ("saved stuff: " +savedUsername + " " +savedPassword);

		savedUsername = PlayerPrefs.GetString ("username", "");
		savedPassword = PlayerPrefs.GetString ("password", "");

		if (savedUsername != "")
			username.GetComponent<InputField> ().text = savedUsername;

		if ((savedUsername != "") && (savedPassword != "")) {

			string url = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/login.php";

			WWWForm postForm = new WWWForm();
			postForm.AddField("user", savedUsername);
			postForm.AddField("password", savedPassword);


			WWW validationPage = new WWW(url, postForm); // + "?usuario=" + usuario + "&senha=" + senha);


			StartCoroutine(ValidateLogin(validationPage));

		}

	}



	public loginManager()
	{


	}

	void Update()
	{
		
	}

	void OnEnable()
	{
		currentUser = null;		//every time we go back to the login screen, user is logged out
		currentUserID = null;
	}


	public void loginButton()		//in order for it to be instantiated manually into a button, function needs to have a maximum of ONE parameter
	{
		string url = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/login.php";

		currentUser = username.GetComponent<InputField> ().text;

		WWWForm postForm = new WWWForm();
		postForm.AddField("user", username.GetComponent<InputField>().text);
		postForm.AddField("password", password.GetComponent<InputField>().text);

		Debug.Log(username.GetComponent<InputField>().text);
		Debug.Log(password.GetComponent<InputField>().text);

		if (PlayerPrefs.GetString("username") != username.GetComponent<InputField> ().text)		//if user logging in is not the previous one logged in, resets the schedule
			PlayerPrefs.SetInt ("storedCurrentWeek", 0);

		PlayerPrefs.SetString ("username", username.GetComponent<InputField> ().text);
		PlayerPrefs.SetString ("password", password.GetComponent<InputField> ().text);

		WWW validationPage = new WWW(url, postForm); // + "?usuario=" + usuario + "&senha=" + senha);


		StartCoroutine(ValidateLogin(validationPage));


	}


	IEnumerator ValidateLogin(WWW validationPage)
	{
		float timeout = 10f;
		float waitingTime = 0f;
		//loginButton.interactable = false;

		// LaunchFeedbackMessage("Carregando...", FeedbackMessageType.Info);
		//Debug.Log("waiting for validation");

		while (!validationPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {
			//Debug.Log("timeout, fuck");
			errorFeedback.text = "Problemas para conectar. Por favor cheque sua conexão à internet.";
			yield return null;
		}
		else {
			//Debug.Log("yeah that's the spirit");
			if (!string.IsNullOrEmpty(validationPage.error))
			{
				if (validationPage.error == "404: Not Found")
				{
					errorFeedback.text = "Servidor indisponível no momento.\n Por favor, tente novamente mais tarde.";
				}

				else
				{
					errorFeedback.text = "Ocorreu um erro inesperado.\n Por favor, cheque sua conexão.";
				}

				yield break;
			}

			//Debug.Log(validationPage.text);

			int resultPage;

			if (validationPage.text.Contains("sucesso") || validationPage.text.Contains("5"))			//remove the '|| validationPage.text.Contains("5")' part to disallow "free users"
			{
				Debug.Log (validationPage.text);

				currentUser = username.GetComponent<InputField> ().text;
				//currentUserID

				currentUserID = Regex.Match(validationPage.text, @"\d+").Value;


				Debug.Log (currentUserID);


				//loginController.lastSceneEntered = "loginScene";
				gameObject.GetComponent<loginController> ().loadScene ("menuScene");

			}
			else if (!int.TryParse(validationPage.text, out resultPage))
			{
				errorFeedback.text = "Ocorreu um erro inesperado. Por favor, cheque sua conexão.";
			}
			else
			{
				switch (resultPage)
				{
				case 1:
					//else if(www.text=="0"){ <- Original, mas não funcionou
					errorFeedback.text = "Usuário ou senha inválidos!";
					break;

				case 2:
					errorFeedback.text = "Senha incorreta!";
					break;

				case 3:
					errorFeedback.text = "Usuário não existente!";
					break;

				case 4:
					errorFeedback.text = "Servidor indisponível no momento. Tente novamente mais tarde.";
					break;

				case 5:
					errorFeedback.text = "Oops, parece que sua mensalidade não está em dia :/\n\nRedirecionando você à loja!";


					StartCoroutine(waitABit());


					break;

				default:
					errorFeedback.text = "Ocorreu um erro inesperado.";
					//FeedBackError (www.text);
					break;
				}

				yield break;
			}

			


		}
	}

	IEnumerator waitABit()
	{
		yield return new WaitForSeconds (3);

		gameObject.GetComponent<loginController> ().loadScene ("loggedOutStoreScene");
	}

	//example working function:
}