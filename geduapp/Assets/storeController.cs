﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class storeController : MonoBehaviour {


	public GameObject inspectProductPanel;
	public Text inspectedProdDesc;
	public Text inspectedProdQuant;
	public Image inspectedProdImg;

	public GameObject proceedPurchaseButton;

	public Text totalPrice;

	int currentProduct;

	int quantProd;
	public Text quantity;

	public string[] productName;
	public string[] productDescription;
	public float[] productPrice;
	public int[] productQuant;
	public Sprite[] productImage;

	public Transform clonableProduct;



	// Use this for initialization
	void Start () {

		fillStore ();

		quantProd = 0;
		inspectProductPanel.SetActive (false);
	}
		
	
	// Update is called once per frame
	void Update () {
		
	}

	public void inspectProduct(int productId)
	{
		currentProduct = productId;
		Debug.Log (productId);
		inspectedProdDesc.text = productDescription [productId];
		//quantProd = productQuant [productId];
		inspectedProdQuant.text = "Quantidade: " + quantProd.ToString();

		inspectedProdImg.sprite = productImage [productId];

		inspectProductPanel.SetActive (true);
	}

	public void selectProduct(int productId)
	{
		currentProduct = productId;



		proceedPurchaseButton.GetComponent<Button>().onClick.RemoveAllListeners();

		switch (productId)
		{
		case 0:

			totalPrice.text = "Valor a ser pago: R$ " + productPrice[productId].ToString("0.00");

			proceedPurchaseButton.GetComponent<Button> ().onClick.AddListener (() => {
				Application.OpenURL ("http://pag.ae/7UFUA6onR");			//mensal
			});
			break;
		case 1:

			totalPrice.text = "Valor a ser pago: 3x R$ " + productPrice[productId].ToString("0.00");

			proceedPurchaseButton.GetComponent<Button> ().onClick.AddListener (() => {
				Application.OpenURL ("http://pag.ae/7UFUBJNNR");			//trimestral
			});
			break;
		case 2:

			totalPrice.text = "Valor a ser pago: 6x R$ " + productPrice[productId].ToString("0.00");
			proceedPurchaseButton.GetComponent<Button> ().onClick.AddListener (() => {
				Application.OpenURL ("http://pag.ae/7UFUCCRVo");			//semestral
			});
			break;


		}



	}

	public void closeProduct()
	{
		//productQuant [currentProduct] = quantProd;
		inspectProductPanel.SetActive (false);
		fillStore ();
	}

	public void incProduct()
	{
		if (quantProd<10)
			quantProd++; 
		quantity.text = "Quantidade: " + quantProd;
	}

	public void decProduct()
	{
		if (quantProd>0)
			quantProd--; 
		quantity.text = "Quantidade: " + quantProd;
	}

	public void fillStore()
	{
		GameObject [] deletableObjects = GameObject.FindGameObjectsWithTag ("toBeDeleted");

		foreach (GameObject delObj in deletableObjects)
		{
			Destroy(delObj);
		}

		int cont = 0;
		float totalValue = 0;

		foreach (string prod in productName) {


			Transform instantiatedGO = Instantiate(clonableProduct, new Vector3(0, 0, 0), Quaternion.identity);

			instantiatedGO.gameObject.SetActive (true);
			instantiatedGO.transform.SetParent (clonableProduct.parent);

			instantiatedGO.transform.localPosition = new Vector3 (instantiatedGO.transform.localPosition.x, clonableProduct.localPosition.y-155*cont, 0f);
			instantiatedGO.transform.localScale = new Vector3(1f, 1f, 1f);



			instantiatedGO.tag = "toBeDeleted";

			instantiatedGO.GetChild (0).GetChild (0).GetComponent<Text> ().text = productName[cont];
			instantiatedGO.GetChild (1).GetChild (0).GetComponent<Text> ().text = productDescription[cont];

			if (cont ==1)
				instantiatedGO.GetChild (2).GetChild (0).GetComponent<Text> ().text = "3x\n" + productPrice[cont].ToString("0.00");

			else if (cont ==2)
				instantiatedGO.GetChild (2).GetChild (0).GetComponent<Text> ().text = "6x\n" + productPrice[cont].ToString("0.00");

			else
				instantiatedGO.GetChild (2).GetChild (0).GetComponent<Text> ().text = productPrice[cont].ToString("0.00");
			//instantiatedGO.GetChild (3).GetChild (0).GetComponent<Text> ().text = productQuant[cont].ToString();

			//totalValue += productPrice [cont] * productQuant [cont];

			int id = cont;

			instantiatedGO.GetComponent<Button> ().onClick.AddListener(() => {
				selectProduct(id);
			});


			cont++;



		}

		totalPrice.text = "Valor a ser pago: R$ " + totalValue.ToString("0.00");

	}

	public void registerAttemptPaymentDB()
	{


		string url = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/registerAttemptPayment.php";

		WWWForm postForm = new WWWForm();

		postForm.AddField("userName", PlayerPrefs.GetString("username"));


		WWW validationPage = new WWW(url, postForm); // + "?usuario=" + usuario + "&senha=" + senha);


		StartCoroutine(registerAPDB(validationPage));


	}


	IEnumerator registerAPDB(WWW validationPage)
	{
		float timeout = 10f;
		float waitingTime = 0f;
		//loginButton.interactable = false;

		// LaunchFeedbackMessage("Carregando...", FeedbackMessageType.Info);
		//Debug.Log("waiting for validation");

		while (!validationPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {
			//Debug.Log("timeout, fuck");
			Debug.Log( "Problemas para conectar. Por favor cheque sua conexão à internet.");
			yield return null;
		}
		else {
			//Debug.Log("yeah that's the spirit");

			Debug.Log("error = " + validationPage.error);
			Debug.Log("text = " + validationPage.text);

			if (!string.IsNullOrEmpty(validationPage.error))
			{
				if (validationPage.error == "404: Not Found")
				{
					Debug.Log("Servidor indisponível no momento.\n Por favor, tente novamente mais tarde.");
				}

				else
				{
					Debug.Log("Ocorreu um erro inesperado.\n Por favor, cheque sua conexão.");
				}

				yield break;
			}



			int resultPage;

			if (validationPage.text.Contains("1"))
			{
				Debug.Log ("Tentativa registrada!");

			}
			else if (!int.TryParse(validationPage.text, out resultPage))
			{
				Debug.Log("Ocorreu um erro inesperado. Por favor, cheque sua conexão.");
			}
			else
			{
				switch (resultPage)
				{
				case 2:
					//else if(www.text=="0"){ <- Original, mas não funcionou
					Debug.Log("Não foi possível registrar");
					break;

				case 0:
					Debug.Log("Usuário inválido");
					break;

				default:
					Debug.Log("Ocorreu um erro inesperado.");
					//FeedBackError (www.text);
					break;
				}

				yield break;
			}




		}



	}


}
