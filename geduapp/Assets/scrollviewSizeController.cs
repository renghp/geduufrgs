﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scrollviewSizeController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
		//organizeAnswerTextPositions ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	void OnEnable()
	{
		Debug.Log ("was enabled");
		organizeAnswerTextPositions ();
	}


	public void organizeAnswerTextPositions()
	{

		foreach(Transform child in transform)
		{
			if (child.gameObject.activeSelf) {
				child.transform.GetChild (1).GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, (child.transform.GetChild (1).GetChild (0).GetComponent<Text> ().text.Length * 4 / 5) + 100f);
				int currentIndex = child.transform.GetSiblingIndex ();

				float spacing = -(child.transform.localPosition.y - (child.transform.GetChild (1).GetChild (0).GetComponent<Text> ().text.Length * 4 / 5 + 210)) + 200;

				try {  	//positions any comment underneath the previous one
					child.transform.parent.GetChild (currentIndex + 1).transform.localPosition = new Vector3 (child.transform.parent.GetChild (currentIndex + 1).transform.localPosition.x, child.transform.localPosition.y - (child.transform.GetChild (1).GetChild (0).GetComponent<Text> ().text.Length * 4 / 5 + 300), child.transform.parent.GetChild (currentIndex + 1).transform.localPosition.z);

					transform.GetComponent<RectTransform> ().sizeDelta = new Vector2(0, spacing+50);
				} catch {

					child.transform.parent.gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, spacing);
				}
			}

		
		}


	}
}
