﻿using UnityEngine;
using System;
using System.Text;
using System.Collections;
using System.Security.Cryptography;

public class AWSWebFormBuilder
{
	// TODO: still need to store the accessKeyID of the actual root account (mturk needs this and can't use amazon IAM).
	const string rootID = "REMOVED";
	const string rootAccessKeyID = "";
	const string rootSecretAccessKey = "";

	const string appAccessKeyID = "REMOVED";
	const string appSecretAccessKey = "REMOVED";

	const double requestValidForSeconds = 60.0f;

	string CreateSignatureSHA1(string policy)
	{
		byte[] policyBytes = Encoding.Default.GetBytes(policy);
		byte[] keyBytes = Encoding.Default.GetBytes(appSecretAccessKey);

		HMACSHA1 signHash = new HMACSHA1(keyBytes);
		signHash.ComputeHash(policyBytes);

		string finalSignature = Convert.ToBase64String(signHash.Hash);
		return finalSignature;
	}

	public void BuildUploadRequest(ref WWWForm requestForm, string fileName, string fileContent)
	{
		DateTime currentTime = DateTime.UtcNow;
		DateTime requestExpiryTime = currentTime.AddSeconds(requestValidForSeconds);

		string iso8601ExpiryTime = requestExpiryTime.ToString("s") + "Z";
		string iso8601CurrentTime = currentTime.ToString("s", System.Globalization.CultureInfo.InvariantCulture);

		iso8601CurrentTime = iso8601CurrentTime.Replace(" ", "").Replace("-", "").Replace(":", "");

		string policy =     "{ \"expiration\": \"" + iso8601ExpiryTime + "\"," +
			"  \"conditions\": [" +
			"  {\"acl\": \"private\" }, " +
			"  {\"bucket\": \"ourbucket\" }," +
			"  [\"starts-with\", \"$key\", \"Project 1 Output/\"]," +
			"  [\"starts-with\", \"$Content-Type\", \"\"]," +
			"  [\"content-length-range\", 1, 102400], " +
			"  {\"x-amz-credential\": \"" + appAccessKeyID + "/" + currentTime.ToString("yyyymmdd") + "/us-east-1/s3/aws4_request\"}," +
			"  {\"x-amz-algorithm\": \"AWS4-HMAC-SHA1\"}," +
			"  {\"x-amz-date\": \"" + iso8601CurrentTime + "\" }" +
			"]}";

		policy = Convert.ToBase64String(Encoding.UTF8.GetBytes(policy));

		string signature = CreateSignatureSHA1(policy);

		requestForm.AddField("key", fileName);
		requestForm.AddField("AWSAccessKeyId", appAccessKeyID);
		requestForm.AddField("policy", policy);
		requestForm.AddField("acl", "private");
		requestForm.AddField("x-amz-credential", appAccessKeyID + "/" + currentTime.ToString("yyyymmdd") + "/us-east-1/s3/aws4_request");
		requestForm.AddField("x-amz-algorithm", "AWS4-HMAC-SHA1");
		requestForm.AddField("x-amz-date", iso8601CurrentTime);
		requestForm.AddField("Signature", signature);
		requestForm.AddField("Content-Type", "text/plain");

		requestForm.AddBinaryData("file", Encoding.UTF8.GetBytes(fileContent), fileName, "text/plain");
	}
}