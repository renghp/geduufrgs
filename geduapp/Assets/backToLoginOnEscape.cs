﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class backToLoginOnEscape : MonoBehaviour {

	// Use this for initialization

	public GameObject promptLogout;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if ((SceneManager.GetActiveScene ().name == "simulationScene") || (SceneManager.GetActiveScene ().name == "menuScene"))
				promptLogout.SetActive (true);
			else if (SceneManager.GetActiveScene ().name == "createAccScene") {
				PlayerPrefs.SetString ("password", "");
				SceneManager.LoadScene ("loginScene");
			}
			else if (SceneManager.GetActiveScene ().name == "accProfilingScene")
				SceneManager.LoadScene ("createAccScene");
			else if (SceneManager.GetActiveScene ().name == "loggedOutStoreScene") {
				PlayerPrefs.SetString ("password", "");
				SceneManager.LoadScene ("loginScene");
			}
			else
				SceneManager.LoadScene("menuScene");
		}
	}
}
