﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System;

public class getDirectedQuestionsDB : MonoBehaviour {

	public Text errorFeedback;

	int [] randomizedQuestions;		//[number]	// 0 - question 1; 1 - q2; 2 - q3; 3 - q4; 4 - q5;

	public Text questionTextField;
	public Text ATextField;
	public Text BTextField;
	public Text CTextField;
	public Text DTextField;
	public Text ETextField;


	public Text resultsPromptText;

	//public GameObject questionImg;

	public RawImage questionImg;
	public RectTransform questionScrollSize;

	public Image nextQuestionImg;
	public Sprite finalizeButtonSprite;
	public Sprite nextQuestionButtonSprite;

	public GameObject promptContinuePanel;

	public GameObject resultsSheetPanel;
	public GameObject finalStatusPanel;

	public Transform clonableResult;

	public RawImage zoomedInAnswerImg;

	public static string imgURL;
	public static string AimgURL;
	public static string BimgURL;
	public static string CimgURL;
	public static string DimgURL;
	public static string EimgURL;

	public Button previousQuestionButton;

	static System.Random rnd;

	public Text statusText;

	public Text finalStatusText;

	int question = 1;

	int questionLimit;
	int score;


	int [] currentOrder = new int[5]; 
	int currentQuestion = 0;
	int [] markedAnswers = new int[5]; 
	char [] correctAnswers = new char[5]; 

	public Button [] options;

	bool reviewingAnswers = false;

	public Scrollbar questionScrollbar;

	int currentDif = 1;

	int currentDifPhys, currentDifMath, currentDifChem;

	public static int currentSubject;
	public static int currentSubgroup;

	public static string weekDaySimu;


	// Use this for initialization
	void Start () {

		//Debug.Log ("materia do dia: " + currentSubject);

		zoomInAnswerPanel.directed = true;

		currentSubgroup = PlayerPrefs.GetInt ("subgroup");

		int currentAvg;

		switch (currentSubject) {
		case 1:
			currentAvg = profileController.physAvg;
			break;
		case 2:
			currentAvg = profileController.mathAvg;
			break;
		case 3:
			currentAvg = profileController.chemAvg;
			break;
		default:
			currentAvg = (profileController.physAvg + profileController.mathAvg + profileController.chemAvg) / 3;
			break;

		}



		if (currentAvg > 35)
			currentDif = 2;
		if (currentAvg > 80)
			currentDif = 3;
		else
			currentAvg = 1;

		currentDifPhys = currentDif;
		currentDifMath = currentDif;
		currentDifChem = currentDif;

		startInNewDifficulty ();


	}

	// Update is called once per frame
	void Update () {


		if (questionImg.IsActive ()) {

			questionImg.SetNativeSize ();

			if (Screen.orientation == ScreenOrientation.Portrait)
				questionImg.rectTransform.sizeDelta = new Vector2 (700f, (700f / questionImg.rectTransform.sizeDelta.x) * questionImg.rectTransform.sizeDelta.y);



			questionTextField.rectTransform.offsetMax = new Vector2 (questionTextField.rectTransform.offsetMax.x, -questionImg.rectTransform.sizeDelta.y - 20f);


			questionScrollSize.sizeDelta = new Vector2(questionScrollSize.sizeDelta.x, questionImg.rectTransform.sizeDelta.y + questionTextField.text.Length + 50f);


		} else {
			questionTextField.rectTransform.offsetMax = new Vector2 (questionTextField.rectTransform.offsetMax.x, -20f);

			questionScrollSize.sizeDelta =  new Vector2(questionScrollSize.sizeDelta.x, questionTextField.text.Length + 50f);
		}

	}

	public void clearQuestion(){



		questionTextField.text = "";
		ATextField.text = "";
		BTextField.text = "";
		CTextField.text = "";
		DTextField.text = "";
		ETextField.text = "";

		questionScrollbar.value = 1f;

		questionImg.texture = null;



	}

	public void nextQuestion () {

		clearQuestion ();

		previousQuestionButton.interactable = true;

		if (nextQuestionImg.sprite == finalizeButtonSprite)
			finishQuestionnaire ();

		else {

			if (reviewingAnswers) {
				nextQuestionImg.sprite = finalizeButtonSprite;
			}
			else
				nextQuestionImg.sprite = nextQuestionButtonSprite;



			if (currentQuestion < questionLimit - 1) {
				currentQuestion++;

				selectAnswer (markedAnswers [currentQuestion] - 1);

				string url = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/getQuestionByID.php";

				WWWForm postForm = new WWWForm ();
				postForm.AddField ("questionID", currentOrder [currentQuestion].ToString ());



				WWW validationPage = new WWW (url, postForm); // + "?usuario=" + usuario + "&senha=" + senha);

				statusText.text = (currentQuestion + 1).ToString () + " / " + questionLimit;



				StartCoroutine (getQuestionData (validationPage));




				if (currentQuestion == questionLimit - 1)
					nextQuestionImg.sprite = finalizeButtonSprite;

			} /*else if (currentQuestion == questionLimit - 1) {

				finishQuestionnaire ();


			}*/


		}




	}

	public void previousQuestion () {

		clearQuestion ();

		nextQuestionImg.sprite = nextQuestionButtonSprite;

		if (currentQuestion >= 1) {
			currentQuestion--;

			string url = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/getQuestionByID.php";

			WWWForm postForm = new WWWForm ();
			postForm.AddField ("questionID", currentOrder [currentQuestion].ToString ());



			WWW validationPage = new WWW (url, postForm); // + "?usuario=" + usuario + "&senha=" + senha);

			statusText.text = (currentQuestion+1).ToString() + " / "+ questionLimit;

			StartCoroutine (getQuestionData (validationPage));

			selectAnswer(markedAnswers [currentQuestion]-1);
		} 

		if (currentQuestion == 0)
			previousQuestionButton.interactable = false;



	}

	void finishQuestionnaire()
	{

		nextQuestionImg.sprite = nextQuestionButtonSprite;
		reviewingAnswers = true;

	
		resultsSheetPanel.SetActive (true);
		finalStatusPanel.SetActive (true);

		GameObject [] deletableObjects = GameObject.FindGameObjectsWithTag ("toBeDeleted");

		foreach (GameObject delObj in deletableObjects)
		{
			Destroy(delObj);
		}

		int cont = 0;

		score = 0;



		foreach (int ma in markedAnswers) {

			Transform instantiatedGO = Instantiate(clonableResult, clonableResult.transform.parent);

			//Instantiate(clonableResult, clonableResult.transform.parent,

			//instantiatedGO.transform.SetParent (clonableResult.transform.parent);
			instantiatedGO.tag = "toBeDeleted";
			instantiatedGO.gameObject.SetActive (true);

			instantiatedGO.transform.localPosition = new Vector2(instantiatedGO.transform.localPosition.x, instantiatedGO.transform.localPosition.y -100f*cont);
			instantiatedGO.transform.localScale = new Vector3(1, 1, 1);

			instantiatedGO.GetChild (0).GetChild (0).GetComponent<Text> ().text = (cont + 1).ToString();


			int tempInt = cont;
			instantiatedGO.GetComponent<Button> ().onClick.AddListener (() =>checkQuestion (tempInt));

			Debug.Log ("instanciou um botão para ver a questão " + tempInt.ToString ());



			switch (markedAnswers [cont]) {
			case 1:
				instantiatedGO.GetChild (2).GetChild (0).GetComponent<Text> ().text = "A";
				break;
			case 2:
				instantiatedGO.GetChild (2).GetChild (0).GetComponent<Text> ().text = "B";
				break;
			case 3:
				instantiatedGO.GetChild (2).GetChild (0).GetComponent<Text> ().text = "C";
				break;
			case 4:
				instantiatedGO.GetChild (2).GetChild (0).GetComponent<Text> ().text = "D";
				break;
			case 5:
				instantiatedGO.GetChild (2).GetChild (0).GetComponent<Text> ().text = "E";
				break;
			default:
				instantiatedGO.GetChild (2).GetChild (0).GetComponent<Text> ().text = "-";
				break;
			}


			//tem que vir do banco:
			instantiatedGO.GetChild (1).GetChild (0).GetComponent<Text> ().text = Char.ToUpper(correctAnswers[cont]).ToString();

			if (instantiatedGO.GetChild (1).GetChild (0).GetComponent<Text> ().text == instantiatedGO.GetChild (2).GetChild (0).GetComponent<Text> ().text) {

				Debug.Log ("questão " + cont.ToString () + " está correta!!");

				instantiatedGO.GetComponent<Image> ().color = new Color (0f, 1f, 0.13f, 0.5f);

				score++;

			}

			cont++;

			if (cont == questionLimit)
				break;

		}

		float finalScore = ((float)score/(float)questionLimit)*100f;

		string dayOfTheWeek = DateTime.Now.DayOfWeek.ToString ();

		if (finalScore < 35f) {

			PlayerPrefs.SetInt (weekDaySimu, 3);


			finalStatusText.text = "Você acertou " + finalScore.ToString ("#0.##") + "% das questões :(";
			resultsPromptText.text = "Deseja continuar tentando ou sair?";
			currentDif = 0;

		} else if (finalScore >= 35f) {

			PlayerPrefs.SetInt (weekDaySimu, 2);

			finalStatusText.text = "Você acertou " + finalScore.ToString ("#0.##") + "% das questões! :)";
			resultsPromptText.text = "Deseja continuar com questões mais difíceis ou sair?";
		}

		if (finalScore >= 80f) {

			PlayerPrefs.SetInt (weekDaySimu, 1);

			finalStatusText.text = "Parabéns! Você acertou " + finalScore.ToString ("#0.##") + "% das questões! :)";

		}


	}


	IEnumerator getQuestionData(WWW validationPage)
	{


		float timeout = 60f;
		float waitingTime = 0f;
		//loginButton.interactable = false;

		// LaunchFeedbackMessage("Carregando...", FeedbackMessageType.Info);


		while (!validationPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {

			FeedBackError("Imagem não encontrada, grande demais ou servidor offline :(");
			yield return null;
		}
		else {


			Debug.Log("error = " + validationPage.error);
			Debug.Log("text = " + validationPage.text);

			if (!string.IsNullOrEmpty(validationPage.error))
			{
				if (validationPage.error == "404: Not Found")
				{
					FeedBackError("Servidor indisponível no momento.\n Por favor, tente novamente mais tarde.");
				}

				else
				{
					FeedBackError("Ocorreu um erro inesperado.\n Por favor, cheque sua conexão.");
				}

				yield break;
			}



			int resultPage;



			if (validationPage.text.Length < 5) {

				int.TryParse (validationPage.text, out resultPage);

				switch (resultPage) {
				case 3:
					//else if(www.text=="0"){ <- Original, mas não funcionou
					FeedBackError("Servidor indisponível!");
					break;

				case 2:
					FeedBackError("Questão inválida: " + currentQuestion + " id = " + currentOrder[currentQuestion]);
					break;

				default:
					FeedBackError("Não encontramos os dados dessa questão :(");
					break;
				}
			} else {				//parse db text

				errorFeedback.text = "";

				//FeedBackOk("Questão válida: " + currentQuestion + " id = " + currentOrder[currentQuestion]);

				questionTextField.text = Regex.Match(validationPage.text, @"((.|\n)*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)").Groups[1].Value;

				ATextField.text = Regex.Match(validationPage.text, @"(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)").Groups[2].Value;

				if (ATextField.text == "") {
					ATextField.text = "Clique na lupa para visualizar!";
				}

				BTextField.text = Regex.Match(validationPage.text, @"(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)").Groups[3].Value;

				if (BTextField.text == "") {
					BTextField.text = "Clique na lupa para visualizar!";
				}

				CTextField.text = Regex.Match(validationPage.text, @"(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)").Groups[4].Value;

				if (CTextField.text == "") {
					CTextField.text = "Clique na lupa para visualizar!";
				}

				DTextField.text = Regex.Match(validationPage.text, @"(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)").Groups[5].Value;

				if (DTextField.text == "") {
					DTextField.text = "Clique na lupa para visualizar!";
				}

				ETextField.text = Regex.Match(validationPage.text, @"(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)").Groups[6].Value;

				if (ETextField.text == "") {
					ETextField.text = "Clique na lupa para visualizar!";
				}

				imgURL = Regex.Match(validationPage.text, @"(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)").Groups[7].Value;

				AimgURL = Regex.Match(validationPage.text, @"(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)").Groups[8].Value;
				BimgURL = Regex.Match(validationPage.text, @"(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)").Groups[9].Value;
				CimgURL = Regex.Match(validationPage.text, @"(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)").Groups[10].Value;
				DimgURL = Regex.Match(validationPage.text, @"(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)").Groups[11].Value;
				EimgURL = Regex.Match(validationPage.text, @"(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)").Groups[12].Value;

				string ca;
				ca = Regex.Match(validationPage.text, @"(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)\§(.*)").Groups[13].Value;

				correctAnswers[currentQuestion] = ca[0];

				Debug.Log("answer of question " + currentQuestion.ToString() + ": " + ca);

				if (imgURL != "") {


					WWW imgPage = new WWW (imgURL); // + "?usuario=" + usuario + "&senha=" + senha);


					StartCoroutine (loadImage (imgPage));


				} else {
					questionImg.gameObject.SetActive (false);
					questionTextField.rectTransform.offsetMax = new Vector2 (questionTextField.rectTransform.offsetMax.x, -20);
				}

				/*if (AimgURL != "")
					Debug.Log (AimgURL);
				if (BimgURL != "")
					Debug.Log (BimgURL);
				if (CimgURL != "")
					Debug.Log (CimgURL);
				if (DimgURL != "")
					Debug.Log (DimgURL);
				if (EimgURL != "")
					Debug.Log (EimgURL);*/

			}

		}


	}

	IEnumerator getQuestionIDs(WWW validationPage, int dif, int sub)
	{
		float timeout = 10f;
		float waitingTime = 0f;



		while (!validationPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {

			FeedBackError("Imagem não encontrada, grande demais ou servidor offline :(");
			yield return null;
		}
		else {


			Debug.Log("error = " + validationPage.error);
			Debug.Log("text = " + validationPage.text);

			if (!string.IsNullOrEmpty(validationPage.error))
			{
				if (validationPage.error == "404: Not Found")
				{
					FeedBackError("Servidor indisponível no momento.\n Por favor, tente novamente mais tarde.");
				}

				else
				{
					FeedBackError("Ocorreu um erro inesperado.\n Por favor, cheque sua conexão.");
				}

				yield break;
			}



			int resultPage;



			if (!validationPage.text.Contains("§")) {

				int.TryParse (validationPage.text, out resultPage);

				switch (resultPage) {
				case 3:
					FeedBackError("Servidor indisponível!");
					break;

				case 2:
					FeedBackError("Questão inválida: " + currentQuestion);
					break;

				default:
					FeedBackError("Não encontramos questões dessa matéria :(");
					break;
				}
			} else {				//parse db text

				Debug.Log("achou questões");



				string first;
				string result = validationPage.text;

				Debug.Log ("parsing");

				List<int> allQuestionsTemporary = new List<int>();

				int temp = 0;

				while (result != "")  {



					if(Int32.TryParse (Regex.Match (result, @"([0-9]+[0-9]+)\§(.*)").Groups [1].Value, out temp))
						allQuestionsTemporary.Add (temp);
					else if (Int32.TryParse (result, out temp))		//in order to get the last id
						allQuestionsTemporary.Add (temp);	


					result = Regex.Match (result, @"([0-9]+[0-9]+)\§(.*)").Groups [2].Value;

					Debug.Log (result);
				}


				//randomizing questions from the list:



				int r1 = rnd.Next (allQuestionsTemporary.Count);

				//Debug.Log ("randomized item: " + allQuestionsTemporary[r1]);

				randomizedQuestions [0] = allQuestionsTemporary [r1];

				int r2 = rnd.Next (allQuestionsTemporary.Count);

				while (r2 == r1) {
					r2 = rnd.Next (allQuestionsTemporary.Count);
				}

				randomizedQuestions [1] = allQuestionsTemporary [r2];

				int r3 = rnd.Next (allQuestionsTemporary.Count);

				while ((r3 == r1) || (r3 == r2)) {
					r3 = rnd.Next (allQuestionsTemporary.Count);
				}

				randomizedQuestions [2] = allQuestionsTemporary [r3];

				int r4 = rnd.Next (allQuestionsTemporary.Count);

				while ((r4 == r1) || (r4 == r2) || (r4 == r3)) {
					r4 = rnd.Next (allQuestionsTemporary.Count);
				}

				randomizedQuestions [3] = allQuestionsTemporary [r4];

				int r5 = rnd.Next (allQuestionsTemporary.Count);

				while ((r5 == r1) || (r5 == r2) || (r5 == r3) || (r5 == r4)) {
					r5 = rnd.Next (allQuestionsTemporary.Count);
				}

				randomizedQuestions [4] = allQuestionsTemporary [r5];


				currentOrder = randomizedQuestions;







				//calls the first question
				string url = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/getQuestionByID.php";

				WWWForm postForm = new WWWForm();
				postForm.AddField("questionID", currentOrder [currentQuestion].ToString());


				WWW validationPage2 = new WWW(url, postForm); // + "?usuario=" + usuario + "&senha=" + senha);

				questionLimit = 5;

				statusText.text = (currentQuestion+1).ToString() + " / "+ questionLimit;

				/*Debug.Log ("ordem das questões: ");
				Debug.Log (currentOrder [0].ToString ());
				Debug.Log (currentOrder [1].ToString ());
				Debug.Log (currentOrder [2].ToString ());
				Debug.Log (currentOrder [3].ToString ());
				Debug.Log (currentOrder [4].ToString ());
				Debug.Log (currentOrder [5].ToString ());*/

				Debug.Log ("chamando questão de id: " + currentOrder [currentQuestion].ToString ());

				StartCoroutine(getQuestionData(validationPage2)); 

				selectAnswer(markedAnswers [currentQuestion]-1);




			}

		}


	}

	public void selectAnswer(int chosenOptionId)
	{
		markedAnswers [currentQuestion] = chosenOptionId+1;		//leaves 0 as null (unanswered) and [1 .. 5] as [a .. e]

		foreach (Button opt in options)
		{
			opt.interactable = true;
		}

		if (chosenOptionId != -1)
			options [chosenOptionId].interactable = false;

	}

	IEnumerator loadImage(WWW imgPage)
	{

		float timeout = 20f;
		float waitingTime = 0f;


		while (!imgPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {

			FeedBackError("Problemas para conectar. Por favor cheque sua conexão à internet.");
			yield return null;
		} else {

			questionImg.gameObject.SetActive (true);

			questionImg.texture = imgPage.texture;

		}
	}

	void checkQuestion(int num)
	{

		Debug.Log("clicou para ver a questão " + num.ToString());

		finalStatusPanel.SetActive (false);
		resultsSheetPanel.SetActive (false);

		currentQuestion = num-1;

		nextQuestion ();

	}

	public void sendResult()
	{



		string url = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/simulationResult.php";

		WWWForm postForm = new WWWForm();

		if (loginManager.currentUserID != null) {
			postForm.AddField ("userID", loginManager.currentUserID);
		}


		switch (currentSubject) {
		case 1:
			postForm.AddField("nQuestionsPhys", (int)questionLimit);
			postForm.AddField("nCorrectPhys", score);
			break;
		case 2:
			postForm.AddField("nQuestionsMath", (int)questionLimit);
			postForm.AddField("nCorrectMath", score);
			break;
		case 3:
			postForm.AddField("nQuestionsChem", (int)questionLimit);
			postForm.AddField("nCorrectChem", score);
			break;
		}







		WWW validationPage2 = new WWW(url, postForm); // + "?usuario=" + usuario + "&senha=" + senha);

		StartCoroutine(sendResultsDB(validationPage2)); 

	}

	IEnumerator sendResultsDB(WWW validationPage)
	{
		float timeout = 10f;
		float waitingTime = 0f;



		while (!validationPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {

			FeedBackError("Servidor não disponível ou offline :(");
			yield return null;
		}
		else {


			Debug.Log("error = " + validationPage.error);
			Debug.Log("text = " + validationPage.text);

			if (!string.IsNullOrEmpty(validationPage.error))
			{
				if (validationPage.error == "404: Not Found")
				{
					FeedBackError("Servidor indisponível no momento.\n Por favor, tente novamente mais tarde.");
				}

				else
				{
					FeedBackError("Ocorreu um erro inesperado.\n Por favor, cheque sua conexão.");
				}

				yield break;
			}



			if (validationPage.text.Contains ("sucesso")) {
				FeedBackOk ("Resultados salvos!");
				promptContinuePanel.SetActive (true);
				if (currentDif < 3) {
					currentDif++;
					currentDifPhys++;
					currentDifMath++;
					currentDifChem++;
				}
			}

			else if (validationPage.text.Contains("username"))
				FeedBackError("Usuário não encontrado!\nVocê está logado?");

			else
				FeedBackError("Erro desconhecido: " + validationPage.text);

		}


	}


	public void startInNewDifficulty()
	{

		if (currentDif == 0)
			currentDif = 1;

		currentQuestion = 0;
		reviewingAnswers = false;

		for (int i = 0; i < 5; i++)
			markedAnswers [i] = 0;

		resultsSheetPanel.SetActive (false);
		finalStatusPanel.SetActive (false);
		previousQuestionButton.interactable = false;

		promptContinuePanel.SetActive (false);


		rnd = new System.Random();

		randomizedQuestions = new int[5];

		//get question ids:

		//easy physics:
		string urlAllQuestions = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/getQuestions.php";

		WWWForm postFormQuestions = new WWWForm();
		postFormQuestions.AddField("subjectID", currentSubject);
		postFormQuestions.AddField("difficulty", currentDif);
		postFormQuestions.AddField("subgroupID", currentSubgroup);

		//Debug.Log ("$$$$$$$$$$$$$$$$ subgrupo atual é " + currentSubgroup + "$$$$$$$$$$$$$$$$");


		WWW validationQuestions = new WWW(urlAllQuestions, postFormQuestions); // + "?usuario=" + usuario + "&senha=" + senha);


		StartCoroutine(getQuestionIDs(validationQuestions, currentDif, 1));

	}

	void FeedBackOk(string mensagem)
	{
		errorFeedback.CrossFadeAlpha(100f, 1f, false);
		errorFeedback.color = new Color(0.13f, 0.5f, 0.83f);
		errorFeedback.text = mensagem;
		errorFeedback.CrossFadeAlpha(0f, 3f, false);
	}

	void FeedBackError(string mensagem)
	{
		errorFeedback.CrossFadeAlpha(0f, 1f, false);
		errorFeedback.CrossFadeAlpha(100f, 2f, false);
		errorFeedback.color = new Color(0.878f, 0.043f, 0f);
		errorFeedback.text = mensagem;
		errorFeedback.CrossFadeAlpha(0f, 4f, false);
	}


}
