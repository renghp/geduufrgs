﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Globalization;
using UnityEngine.UI;

public class weekController : MonoBehaviour {
	

	public Text weekNumberText;

	public Text PhysSubgroupText, MathSubgroupText, ChemSubgroupText, PhysSubgroupText2, MathSubgroupText2, ChemSubgroupText2;

	public Sprite physSprite, mathSprite, chemSprite;

	public GameObject[] days, restDays, simuDays;

	int totalSubgroups = 7;		//needs to vary according to the number of subgroups on the database

	public static int currentSubgroup, currentSubgroup2;

	// Use this for initialization
	void Start () {

	


	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnEnable()
	{

		int actualCurrentWeek = GetIso8601WeekOfYear (DateTime.Now);
		//weekNumberText.text = "Semana #" + actualCurrentWeek;

		int lastWeekStored = PlayerPrefs.GetInt ("storedCurrentWeek", 0);

		//int currentSubgroup = actualCurrentWeek % totalSubgroups - 4;		//returns a subgroup from 0 to 6 based on week number



		int testsTaken = 0;

		if (PlayerPrefs.GetInt ("mondaySimu") != 0)
			testsTaken++;
		if (PlayerPrefs.GetInt ("tuesdaySimu") != 0)
			testsTaken++;
		if (PlayerPrefs.GetInt ("wednesdaySimu") != 0)
			testsTaken++;
		if (PlayerPrefs.GetInt ("thursdaySimu") != 0)
			testsTaken++;
		if (PlayerPrefs.GetInt ("fridaySimu") != 0)
			testsTaken++;
		if (PlayerPrefs.GetInt ("saturdaySimu") != 0)
			testsTaken++;
		if (PlayerPrefs.GetInt ("sundaySimu") != 0)
			testsTaken++;


		if (lastWeekStored == 0) {
			
			PlayerPrefs.SetInt ("storedCurrentWeek", actualCurrentWeek);		//updates current week

			setActivitiesOfTheWeek ();			//whenever week changes, schedule changes.

			afterWeekIsSet (); 
		}

		if (lastWeekStored != actualCurrentWeek) {

			if (testsTaken >= 2)			//checks if at least 2 simulated tests were taken by the student
			{
				Debug.Log("********************************ATUALIZANDO SEMANA!!!********************************");

				PlayerPrefs.SetInt ("storedCurrentWeek", actualCurrentWeek);		//updates current week
				profileController.currentUserWeek++;

				setActivitiesOfTheWeek ();			//whenever week changes, schedule changes.


				//atualizar a semana atual no banco do usuario!!
				string urlWeek = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/updateUserWeek.php";

				WWWForm postFormWeek = new WWWForm();
				postFormWeek.AddField("userID", loginManager.currentUserID);
				postFormWeek.AddField("currentWeek", profileController.currentUserWeek);

				WWW validationPageWeek = new WWW(urlWeek, postFormWeek); // + "?usuario=" + usuario + "&senha=" + senha);

				StartCoroutine(updateWeekDB(validationPageWeek)); 


			}
			else 
				afterWeekIsSet ();


		} else {
			
			fillWeekWithStoredData ();

			afterWeekIsSet ();
		}



	}

	void afterWeekIsSet()
	{
		currentSubgroup = (profileController.currentUserWeek *2)-1 ;	//currentUserWeek starts at 1, which indicates the first subgroup for each subject
		currentSubgroup2 = currentSubgroup+1;

		PlayerPrefs.SetInt ("subgroup", currentSubgroup);		

		string url = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/getSubgroupByID.php";

		WWWForm postFormPhys = new WWWForm();
		postFormPhys.AddField("subgroupID", currentSubgroup);
		postFormPhys.AddField("subjectID", 1);

		WWW validationPagePhys = new WWW(url, postFormPhys); // + "?usuario=" + usuario + "&senha=" + senha);

		StartCoroutine(getSubgroup(validationPagePhys, 1)); 



		WWWForm postFormPhys2 = new WWWForm();
		postFormPhys2.AddField("subgroupID", currentSubgroup2);
		postFormPhys2.AddField("subjectID", 1);

		WWW validationPagePhys2 = new WWW(url, postFormPhys2); // + "?usuario=" + usuario + "&senha=" + senha);

		StartCoroutine(getSubgroup(validationPagePhys2, 4)); 



		WWWForm postFormMath = new WWWForm();
		postFormMath.AddField("subgroupID", currentSubgroup);
		postFormMath.AddField("subjectID", 2);

		WWW validationPageMath = new WWW(url, postFormMath); // + "?usuario=" + usuario + "&senha=" + senha);

		StartCoroutine(getSubgroup(validationPageMath, 2)); 



		WWWForm postFormMath2 = new WWWForm();
		postFormMath2.AddField("subgroupID", currentSubgroup2);
		postFormMath2.AddField("subjectID", 2);

		WWW validationPageMath2 = new WWW(url, postFormMath2); // + "?usuario=" + usuario + "&senha=" + senha);

		StartCoroutine(getSubgroup(validationPageMath2, 5)); 




		WWWForm postFormChem = new WWWForm();
		postFormChem.AddField("subgroupID", currentSubgroup);
		postFormChem.AddField("subjectID", 3);

		WWW validationPageChem = new WWW(url, postFormChem); // + "?usuario=" + usuario + "&senha=" + senha);

		StartCoroutine(getSubgroup(validationPageChem, 3)); 


		WWWForm postFormChem2 = new WWWForm();
		postFormChem2.AddField("subgroupID", currentSubgroup2);
		postFormChem2.AddField("subjectID", 3);

		WWW validationPageChem2 = new WWW(url, postFormChem2); // + "?usuario=" + usuario + "&senha=" + senha);

		StartCoroutine(getSubgroup(validationPageChem2, 6)); 



	}

	IEnumerator updateWeekDB(WWW validationPage)
	{
		float timeout = 10f;
		float waitingTime = 0f;



		while (!validationPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {

			afterWeekIsSet ();
			yield return null;
		}
		else {


			Debug.Log("error = " + validationPage.error);
			Debug.Log("text = " + validationPage.text);

			if (!string.IsNullOrEmpty (validationPage.error)) {
				if (validationPage.error == "404: Not Found") {
					
					Debug.Log ("Servidor indisponível no momento.\n Por favor, tente novamente mais tarde.");
					afterWeekIsSet ();

				} else {
					
					Debug.Log ("Ocorreu um erro inesperado.\n Por favor, cheque sua conexão.");
					afterWeekIsSet ();
				}

				yield break;
			} else if (validationPage.text.Contains("sucesso")) {

				afterWeekIsSet ();

			} else if (validationPage.text.Contains("resetou")) {
				profileController.currentUserWeek = 1;
				afterWeekIsSet ();
			}

		}

		

	}

	IEnumerator getSubgroup(WWW validationPage, int subject)
	{
		float timeout = 10f;
		float waitingTime = 0f;



		while (!validationPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {

			//FeedBackError("Servidor não disponível ou offline :(");
			yield return null;
		}
		else {


			Debug.Log("error = " + validationPage.error);
			Debug.Log("text = " + validationPage.text);

			if (!string.IsNullOrEmpty (validationPage.error)) {
				if (validationPage.error == "404: Not Found") {
					Debug.Log ("Servidor indisponível no momento.\n Por favor, tente novamente mais tarde.");
				} else {
					Debug.Log ("Ocorreu um erro inesperado.\n Por favor, cheque sua conexão.");
				}

				yield break;
			} else {

				if (subject == 1)
					PhysSubgroupText.text = "SEG: Fís. - " + validationPage.text;

				else if (subject ==2)
					MathSubgroupText.text = "TER: Mat. - " + validationPage.text;

				else if (subject ==3)
					ChemSubgroupText.text = "QUA: Quí. - " + validationPage.text;

				if (subject == 4)
					PhysSubgroupText2.text = "QUI: Fís. - " + validationPage.text;

				else if (subject ==5)
					MathSubgroupText2.text = "SEX: Mat. - " + validationPage.text;

				else if (subject ==6)
					ChemSubgroupText2.text = "SÁB: Quí. - " + validationPage.text;

			}	

		}


	}

	public int GetIso8601WeekOfYear(DateTime time)
	{
		// Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
		// be the same week# as whatever Thursday, Friday or Saturday are,
		// and we always get those right
		DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);


		DateTime sundayTime;
		DateTime saturdayTime;

		switch (day) {
		case DayOfWeek.Sunday:
			sundayTime = time;
			saturdayTime = time.AddDays (6);
			break;
		case DayOfWeek.Monday:
			sundayTime = time.AddDays (-1);
			saturdayTime = time.AddDays (5);
			break;
		case DayOfWeek.Tuesday:
			sundayTime = time.AddDays (-2);
			saturdayTime = time.AddDays (4);
			break;
		case DayOfWeek.Wednesday:
			sundayTime = time.AddDays (-3);
			saturdayTime = time.AddDays (3);
			break;
		case DayOfWeek.Thursday:
			sundayTime = time.AddDays (-4);
			saturdayTime = time.AddDays (2);
			break;
		case DayOfWeek.Friday:
			sundayTime = time.AddDays (-5);
			saturdayTime = time.AddDays (1);
			break;
		case DayOfWeek.Saturday:
			sundayTime = time.AddDays (-6);
			saturdayTime = time;
			break;
		}

		weekNumberText.text = "Semana de " + sundayTime.Day + "/" + sundayTime.Month + " a " + saturdayTime.Day + "/" + saturdayTime.Month;
		//Debug.Log ("domingo é dia " + sundayTime.Day + "/" + sundayTime.Month + " sábado é dia " + saturdayTime.Day + "/" + saturdayTime.Month);


		if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
		{
			time = time.AddDays(3);
		}

	

		// Return the week of our adjusted day
		return CultureInfo
			.InvariantCulture
			.Calendar
			.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
	}





	void setActivitiesOfTheWeek ()
	{
		Debug.Log ("vamos organizar as atividades da semana");

		int smallestAvg = 0;


		/*if (profileController.physAvg < profileController.mathAvg && profileController.physAvg < profileController.chemAvg) {
			smallestAvg = 1;
		} else if (profileController.mathAvg < profileController.physAvg && profileController.mathAvg < profileController.chemAvg) {
			smallestAvg = 2;
		} else if (profileController.chemAvg < profileController.physAvg && profileController.chemAvg < profileController.mathAvg) {
			smallestAvg = 3;
		} else if (profileController.chemAvg < profileController.physAvg && profileController.chemAvg == profileController.mathAvg) {
			smallestAvg = 23;
		} else if (profileController.physAvg < profileController.chemAvg && profileController.physAvg == profileController.mathAvg) {
			smallestAvg = 12;
		} else if (profileController.physAvg < profileController.mathAvg && profileController.physAvg == profileController.chemAvg) {
			smallestAvg = 13;
		} else if (profileController.physAvg == profileController.mathAvg && profileController.physAvg == profileController.chemAvg) {
			smallestAvg = 123;
		}

		switch (smallestAvg) {

		case 1:
			PlayerPrefs.SetInt ("mondaySubject", 1); 	//most suggested: phys. 2nd most: math
			PlayerPrefs.SetInt ("tuesdaySubject", 2);	
			PlayerPrefs.SetInt ("wednesdaySubject", 1);
			PlayerPrefs.SetInt ("thursdaySubject", 3);
			PlayerPrefs.SetInt ("fridaySubject", 1);
			PlayerPrefs.SetInt ("saturdaySubject", 2);
			PlayerPrefs.SetInt ("sundaySubject", 1);
			break;
		
		case 2:
			PlayerPrefs.SetInt ("mondaySubject", 2); 	//most suggested: math. 2nd most: chem
			PlayerPrefs.SetInt ("tuesdaySubject", 3);	
			PlayerPrefs.SetInt ("wednesdaySubject", 2);
			PlayerPrefs.SetInt ("thursdaySubject", 1);
			PlayerPrefs.SetInt ("fridaySubject", 2);
			PlayerPrefs.SetInt ("saturdaySubject", 3);
			PlayerPrefs.SetInt ("sundaySubject", 2);
			break;
		case 3:
			PlayerPrefs.SetInt ("mondaySubject", 3); 	//most suggested: chem. 2nd most: phys
			PlayerPrefs.SetInt ("tuesdaySubject", 1);	
			PlayerPrefs.SetInt ("wednesdaySubject", 3);
			PlayerPrefs.SetInt ("thursdaySubject", 2);
			PlayerPrefs.SetInt ("fridaySubject", 3);
			PlayerPrefs.SetInt ("saturdaySubject", 1);
			PlayerPrefs.SetInt ("sundaySubject", 3);
			break;
		case 23:
			PlayerPrefs.SetInt ("mondaySubject", 2); 	//most suggested: math & chem
			PlayerPrefs.SetInt ("tuesdaySubject", 3);	
			PlayerPrefs.SetInt ("wednesdaySubject", 1);
			PlayerPrefs.SetInt ("thursdaySubject", 2);
			PlayerPrefs.SetInt ("fridaySubject", 3);
			PlayerPrefs.SetInt ("saturdaySubject", 2);
			PlayerPrefs.SetInt ("sundaySubject", 3);
			break;
		case 13:
			PlayerPrefs.SetInt ("mondaySubject", 1); 	//most suggested: phys & chem
			PlayerPrefs.SetInt ("tuesdaySubject", 3);	
			PlayerPrefs.SetInt ("wednesdaySubject", 2);
			PlayerPrefs.SetInt ("thursdaySubject", 1);
			PlayerPrefs.SetInt ("fridaySubject", 3);
			PlayerPrefs.SetInt ("saturdaySubject", 1);
			PlayerPrefs.SetInt ("sundaySubject", 3);
			break;
		case 12:
			PlayerPrefs.SetInt ("mondaySubject", 1); 	//most suggested: phys & math
			PlayerPrefs.SetInt ("tuesdaySubject", 2);	
			PlayerPrefs.SetInt ("wednesdaySubject", 3);
			PlayerPrefs.SetInt ("thursdaySubject", 1);
			PlayerPrefs.SetInt ("fridaySubject", 2);
			PlayerPrefs.SetInt ("saturdaySubject", 1);
			PlayerPrefs.SetInt ("sundaySubject", 2);
			break;
		case 123:
			PlayerPrefs.SetInt ("mondaySubject", 1); 	//most suggested: tie
			PlayerPrefs.SetInt ("tuesdaySubject", 2);	
			PlayerPrefs.SetInt ("wednesdaySubject", 3);
			PlayerPrefs.SetInt ("thursdaySubject", 1);
			PlayerPrefs.SetInt ("fridaySubject", 2);
			PlayerPrefs.SetInt ("saturdaySubject", 3);
			PlayerPrefs.SetInt ("sundaySubject", 3);
			break;
		}*/
			
		PlayerPrefs.SetInt ("mondaySubject", 1); 	//most suggested: tie
		PlayerPrefs.SetInt ("tuesdaySubject", 2);	
		PlayerPrefs.SetInt ("wednesdaySubject", 3);
		PlayerPrefs.SetInt ("thursdaySubject", 1);
		PlayerPrefs.SetInt ("fridaySubject", 2);
		PlayerPrefs.SetInt ("saturdaySubject", 3);
		PlayerPrefs.SetInt ("sundaySubject", 0);

		PlayerPrefs.SetInt ("mondayStatus", 0); 	//gray = 0, green = 1, math = 2, chem = 3
		PlayerPrefs.SetInt ("tuesdayStatus", 0);	//default is setting all as grey
		PlayerPrefs.SetInt ("wednesdayStatus", 0);
		PlayerPrefs.SetInt ("thursdayStatus", 0);
		PlayerPrefs.SetInt ("fridayStatus", 0);
		PlayerPrefs.SetInt ("saturdayStatus", 0);
		PlayerPrefs.SetInt ("sundayStatus", 0);

		PlayerPrefs.SetInt ("mondayRest", 0); 	//gray = 0, green = 1, math = 2, chem = 3
		PlayerPrefs.SetInt ("tuesdayRest", 0);	//default is setting all as grey
		PlayerPrefs.SetInt ("wednesdayRest", 0);
		PlayerPrefs.SetInt ("thursdayRest", 0);
		PlayerPrefs.SetInt ("fridayRest", 0);
		PlayerPrefs.SetInt ("saturdayRest", 0);
		PlayerPrefs.SetInt ("sundayRest", 0);

		PlayerPrefs.SetInt ("mondaySimu", 0); 	//gray = 0, green = 1, math = 2, chem = 3
		PlayerPrefs.SetInt ("tuesdaySimu", 0);	//default is setting all as grey
		PlayerPrefs.SetInt ("wednesdaySimu", 0);
		PlayerPrefs.SetInt ("thursdaySimu", 0);
		PlayerPrefs.SetInt ("fridaySimu", 0);
		PlayerPrefs.SetInt ("saturdaySimu", 0);
		PlayerPrefs.SetInt ("sundaySimu", 0);



		fillWeekWithStoredData ();

	}

	void fillWeekWithStoredData ()
	{
		//setActivitiesOfTheWeek ();


		Debug.Log ("vamos só preencher as atividades da semana");

		foreach (GameObject d in days) {

			string subName = d.tag + "Subject";
			string statName = d.tag + "Status";

			int subject = PlayerPrefs.GetInt (subName, 0);
			int status = PlayerPrefs.GetInt (statName, 0);

			Debug.Log (subject);

			switch (subject) {
			case 1:
				d.transform.GetChild (0).GetComponent<RawImage> ().texture = physSprite.texture;
				break;
			case 2:
				d.transform.GetChild (0).GetComponent<RawImage> ().texture = mathSprite.texture;
				break;
			case 3:
				d.transform.GetChild (0).GetComponent<RawImage> ().texture = chemSprite.texture;
				break;
			default:
				d.transform.GetChild (0).GetComponent<RawImage> ().texture = null;
				break;

			}

			switch (status) {
			case 1:
				d.GetComponent<scheduleActivityController> ().turnGreenOnClick ();		//sets it as green
				break;
			case 2:
				d.GetComponent<scheduleActivityController> ().turnGreenOnClick ();
				d.GetComponent<scheduleActivityController> ().turnGreenOnClick ();		//sets it as yellow
				break;
			case 3:
				d.GetComponent<scheduleActivityController> ().turnGreenOnClick ();
				d.GetComponent<scheduleActivityController> ().turnGreenOnClick ();
				d.GetComponent<scheduleActivityController> ().turnGreenOnClick ();		//sets it as red		
				break;
			default:
				break;																	//leaves it as gray

			}

		}

		foreach (GameObject r in restDays) {

			string statName = r.tag + "Rest";

			int status = PlayerPrefs.GetInt (statName, 0);


			switch (status) {
			case 1:
				r.GetComponent<scheduleActivityController> ().turnGreenOnClickRest ();		//sets it as green
				break;
			case 2:
				r.GetComponent<scheduleActivityController> ().turnGreenOnClickRest ();
				r.GetComponent<scheduleActivityController> ().turnGreenOnClickRest ();		//sets it as yellow
				break;
			case 3:
				r.GetComponent<scheduleActivityController> ().turnGreenOnClickRest ();
				r.GetComponent<scheduleActivityController> ().turnGreenOnClickRest ();
				r.GetComponent<scheduleActivityController> ().turnGreenOnClickRest ();		//sets it as red		
				break;
			default:
				break;																	//leaves it as gray

			}

		}

		foreach (GameObject s in simuDays) {

			string statName = s.tag + "Simu";

			int status = PlayerPrefs.GetInt (statName, 0);


			switch (status) {
			case 1:
				s.GetComponent<scheduleActivityController> ().turnGreenOnClickSimu ();		//sets it as green
				break;
			case 2:
				s.GetComponent<scheduleActivityController> ().turnGreenOnClickSimu ();
				s.GetComponent<scheduleActivityController> ().turnGreenOnClickSimu ();		//sets it as yellow
				break;
			case 3:
				s.GetComponent<scheduleActivityController> ().turnGreenOnClickSimu ();
				s.GetComponent<scheduleActivityController> ().turnGreenOnClickSimu ();
				s.GetComponent<scheduleActivityController> ().turnGreenOnClickSimu ();		//sets it as red		
				break;
			default:
				break;																	//leaves it as gray

			}

		}
		
	}
}
