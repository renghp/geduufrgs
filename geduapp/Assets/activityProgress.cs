﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class activityProgress : MonoBehaviour {

	float progress;


	// Use this for initialization
	void Start () {
		progress = 0;
		updateProgress ();
	}
	
	// Update is called once per frame
	void Update () {
		
		//updateProgress ();
	}

	public void updateProgress()
	{
		progress = 0;

		switch (gameObject.name) {
		case "mondayActivityButton":

			foreach (GameObject acti in GameObject.FindGameObjectsWithTag ("monday")) {
			
				if (acti.transform.GetComponent<Image> ().color.r == 0f)
					progress++;
			
			}

			progress = progress / GameObject.FindGameObjectsWithTag ("monday").Length;
			break;
		case "tuesdayActivityButton":

			foreach (GameObject acti in GameObject.FindGameObjectsWithTag ("tuesday")) {

				if (acti.transform.GetComponent<Image> ().color.r == 0f)
					progress++;

			}
			progress = progress / GameObject.FindGameObjectsWithTag ("tuesday").Length;
			break;
		case "wednesdayActivityButton":

			foreach (GameObject acti in GameObject.FindGameObjectsWithTag ("wednesday")) {

				if (acti.transform.GetComponent<Image> ().color.r == 0f)
					progress++;

			}

			progress = progress / GameObject.FindGameObjectsWithTag ("wednesday").Length;
			break;
		case "thursdayActivityButton":

			foreach (GameObject acti in GameObject.FindGameObjectsWithTag ("thursday")) {

				if (acti.transform.GetComponent<Image> ().color.r == 0f)
					progress++;

			}

			progress = progress / GameObject.FindGameObjectsWithTag ("thursday").Length;
			break;
		case "fridayActivityButton":

			foreach (GameObject acti in GameObject.FindGameObjectsWithTag ("friday")) {

				if (acti.transform.GetComponent<Image> ().color.r == 0f)
					progress++;

			}

			progress = progress / GameObject.FindGameObjectsWithTag ("friday").Length;
			break;

		case "saturdayActivityButton":
			
			foreach (GameObject acti in GameObject.FindGameObjectsWithTag ("saturday")) {

				if (acti.transform.GetComponent<Image> ().color.r == 0f)
					progress++;

			}

			progress = progress / GameObject.FindGameObjectsWithTag ("saturday").Length;
			break;
		case "sundayActivityButton":

			foreach (GameObject acti in GameObject.FindGameObjectsWithTag ("sunday")) {

				if (acti.transform.GetComponent<Image> ().color.r == 0f)
					progress++;

			}

			progress = progress / GameObject.FindGameObjectsWithTag ("sunday").Length;
			break;
		}

		transform.GetChild (0).GetComponent<Text> ().text = (progress * 100f).ToString("##0") + "%";
	}


}
