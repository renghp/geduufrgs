﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class clearOldTopic : MonoBehaviour {

	public Text [] clearableTexts;

	public Sprite defaultUserAvatar;

	public RawImage userAvatar;

	public GameObject likeTopicButton, unlikeTopicButton;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnEnable()
	{
		likeTopicButton.SetActive (true);
		unlikeTopicButton.SetActive (false);

		GameObject [] deletableObjects = GameObject.FindGameObjectsWithTag ("postToBeDeleted");

		foreach (GameObject delObj in deletableObjects)
		{
			Destroy(delObj);
		}

		foreach (Text ct in clearableTexts)
			ct.text = "";

		userAvatar.texture = defaultUserAvatar.texture;

		userAvatar.SetNativeSize();

		if (defaultUserAvatar.texture.width > defaultUserAvatar.texture.height) {
			Debug.Log ("imagem horizontal");
			userAvatar.rectTransform.sizeDelta = new Vector2 ((100f / userAvatar.rectTransform.sizeDelta.y) * userAvatar.rectTransform.sizeDelta.x, 100f);

		} else {
			Debug.Log ("imagem vertical");
			userAvatar.rectTransform.sizeDelta = new Vector2 (100f, (100f / userAvatar.rectTransform.sizeDelta.x) * userAvatar.rectTransform.sizeDelta.y);
		}

	}
}
