﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class majorDropdownController : MonoBehaviour {

	public Text areaDropdownLabel;
	public GameObject majorDropdown;
	Dropdown m_DropdownMajor;
	Dropdown m_DropdownArea;

	List<string> majorsArt = new List<string> (new string[] {"Curso Desejado", "Artes Visuais", "Dança", "Design Visual", "Design de Produto", "História da Arte", "Música", "Teatro" });
	List<string> majorsBio = new List<string> (new string[] {"Curso Desejado", "Agronomia", "Desenvolvimento Rural (EAD)", "Biotecnologia", "Ciências Biológicas", "Ciências Biológicas - Ceclimar", "Ciências da Natureza (EAD)" , "Desenvolvimento Regional (Litoral Norte)", "Educação do Campo (Litoral Norte)", "Educação do Campo (Porto Alegre)", "Engenharia Ambiental", "Engenharia Cartográfica", "Engenharia de Alimentos", "Geografia", "Geografia (EAD)", "Geografia (Litoral Norte)", "Medicina Veterinária", "Química", "Zootecnia" });
	List<string> majorsCom = new List<string> (new string[] {"Curso Desejado", "Arquivologia", "Biblioteconomia", "Comunicação Social", "Desenvolvimento Regional (Litoral Norte)", "Geografia (Litoral Norte)", "Jornalismo", "Museologia", "Publicidade e Propaganda", "Relações Públicas" });
	List<string> majorsEco = new List<string> (new string[] {"Curso Desejado", "Administração", "Administração Pública e Social", "Desenvolvimento Rural (EAD)", "Ciências Atuariais", "Ciências Contábeis", "Ciências Econômicas", "Desenvolvimento Regional (Litoral Norte)", "Engenharia de Produção", "Engenharia de Serviços (Litoral Norte)", "Geografia (Litoral Norte)"  });
	List<string> majorsEng = new List<string> (new string[] {"Curso Desejado", "Arquitetura e Urbanismo", "Design Visual", "Design de Produto", "Engenharia Ambiental", "Engenharia Cartográfica", "Engenharia Civil", "Engenharia Elétrica", "Engenharia Física", "Engenharia Hídrica", "Engenharia Mecânica", "Engenharia Metalúrgica", "Engenharia Química", "Engenharia de Alimentos", "Engenharia de Computação", "Engenharia de Controle e Automação", "Engenharia de Energia", "Engenharia de Gestão de Energia (Litoral Norte)", "Engenharia de Materiais", "Engenharia de Minas", "Engenharia de Produção", "Engenharia de Serviços (Litoral Norte)" });
	List<string> majorsExa = new List<string> (new string[] {"Curso Desejado", "Ciência da Computação", "Desenvolvimento Regional (Litoral Norte)", "Engenharia de Gestão de Energia (Litoral Norte)", "Engenharia de Serviços (Litoral Norte)", "Estatística", "Física", "Geografia (Litoral Norte)", "Ciência e Tecnologia (Litoral Norte)", "Matemática"  });
	List<string> majorsHum = new List<string> (new string[] {"Curso Desejado", "Ciências Jurídicas e Sociais", "Ciências Sociais", "Ciências Sociais (EAD)", "Desenvolvimento Regional (Litoral Norte)", "Filosofia", "Geografia (Litoral Norte)", "História", "Letras", "Pedagogia", "Pedagogia (EAD)", "Políticas Públicas", "Psicologia", "Relações Internacionais", "Serviço Social"  });
	List<string> majorsSau = new List<string> (new string[] {"Curso Desejado", "Biomedicina", "Ciências Biológicas", "Educação Física", "Enfermagem", "Farmácia", "Fisioterapia", "Fonoaudiologia", "Medicina", "Medicina Veterinária", "Nutrição", "Odontologia", "Psicologia", "Saúde Coletiva", "Serviço Social"  });

	// Use this for initialization
	void Start () {
		
		majorDropdown.gameObject.SetActive (false);
		m_DropdownMajor = majorDropdown.gameObject.GetComponent<Dropdown>();
		m_DropdownMajor.onValueChanged.AddListener(delegate {
			updateMajorAndAreaInDB();
		});

		m_DropdownArea = GetComponent<Dropdown>();
		m_DropdownArea.onValueChanged.AddListener(delegate {
			manualUpdate();
		});
	}
	
	// Update is called once per frame
	void Update () {

		
	}

	public void manualUpdate()
	{

		if (areaDropdownLabel.text == "Área Desejada")
			majorDropdown.gameObject.SetActive (false);
		else {
			majorDropdown.gameObject.SetActive (true);

			//Clear the old options of the Dropdown menu
			m_DropdownMajor.ClearOptions();
			//Add the options created in the List above

			switch (areaDropdownLabel.text) {
			case "Artes":
				m_DropdownMajor.AddOptions (majorsArt);
				break;
			case "Biológicas, Naturais e Agrárias":
				m_DropdownMajor.AddOptions (majorsBio);
				break;
			case "Comunicação e Informação":
				m_DropdownMajor.AddOptions (majorsCom);
				break;
			case "Economia, Gestão e Negócios":
				m_DropdownMajor.AddOptions (majorsEco);
				break;
			case "Engenharia e Arquitetura":
				m_DropdownMajor.AddOptions (majorsEng);
				break;
			case "Exatas e Tecnológicas":
				m_DropdownMajor.AddOptions (majorsExa);
				break;
			case "Humanas e Sociais":
				m_DropdownMajor.AddOptions (majorsHum);
				break;
			case "Saúde":
				m_DropdownMajor.AddOptions (majorsSau);
				break;
				//default:
				//	m_Dropdown.AddOptions (majorsSau);
				//	break;
			}


		}

		m_DropdownMajor.value = 0;
	}

	public void updateMajorAndAreaInDB()
	{
		Debug.Log (m_DropdownMajor.options [m_DropdownMajor.value].text + m_DropdownArea.options [m_DropdownArea.value].text);		//send these values to the DB

		string url = "http://ec2-18-228-189-86.sa-east-1.compute.amazonaws.com/updateAreaMajor.php";

		WWWForm postForm = new WWWForm();
		postForm.AddField("userID", loginManager.currentUserID);
		postForm.AddField("areaName",  m_DropdownArea.options [m_DropdownArea.value].text);
		postForm.AddField("majorName", m_DropdownMajor.options [m_DropdownMajor.value].text);

		Debug.Log( loginManager.currentUserID);

		WWW validationPage = new WWW(url, postForm); // + "?usuario=" + usuario + "&senha=" + senha);


		StartCoroutine(updateAreaAndMajor(validationPage));


	}


	IEnumerator updateAreaAndMajor(WWW validationPage)
	{
		float timeout = 10f;
		float waitingTime = 0f;
		//loginButton.interactable = false;

		// LaunchFeedbackMessage("Carregando...", FeedbackMessageType.Info);
		//Debug.Log("waiting for validation");

		while (!validationPage.isDone && waitingTime < timeout)
		{
			yield return null;
			waitingTime += Time.deltaTime;

		}

		if (waitingTime >= timeout) {
			//Debug.Log("timeout, fuck");
			Debug.Log( "Problemas para conectar. Por favor cheque sua conexão à internet.");
			yield return null;
		}
		else {
			//Debug.Log("yeah that's the spirit");

			Debug.Log("error = " + validationPage.error);
			Debug.Log("text = " + validationPage.text);

			if (!string.IsNullOrEmpty(validationPage.error))
			{
				if (validationPage.error == "404: Not Found")
				{
					Debug.Log("Servidor indisponível no momento.\n Por favor, tente novamente mais tarde.");
				}

				else
				{
					Debug.Log("Ocorreu um erro inesperado.\n Por favor, cheque sua conexão.");
				}

				yield break;
			}



			int resultPage;

			if (validationPage.text!="")
			{


				Debug.Log("Updateado");

			}
			else if (!int.TryParse(validationPage.text, out resultPage))
			{
				Debug.Log("Ocorreu um erro inesperado. Por favor, cheque sua conexão.");
			}
			else
			{
				switch (resultPage)
				{
				case 3:
					//else if(www.text=="0"){ <- Original, mas não funcionou
					Debug.Log("Servidor indisponível!");
					break;

				case 2:
					Debug.Log("Usuário inválido");
					break;

				default:
					Debug.Log("Ocorreu um erro inesperado.");
					//FeedBackError (www.text);
					break;
				}

				yield break;
			}




		}



	}

}
