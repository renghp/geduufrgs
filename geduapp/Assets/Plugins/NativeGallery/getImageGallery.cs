﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class getImageGallery : MonoBehaviour {

	public RawImage temporaryImg;
	public RawImage avatarImg;

	public GameObject loadUserObject;

	public static string userAvatarFilePath;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void saveImage( )
	{
		//avatarImg.texture = temporaryImg.texture;

		//loadUserObject.GetComponent<loadUserAvatar>







		//if (temporaryImg.texture.width > temporaryImg.texture.height) {
			//Debug.Log ("imagem horizontal");
			//avatarImg.rectTransform.sizeDelta = new Vector2 ((250f / avatarImg.rectTransform.sizeDelta.y) * avatarImg.rectTransform.sizeDelta.x, 250f);

		//}
		/*else{
			Debug.Log ("imagem vertical");
			avatarImg.rectTransform.sizeDelta = new Vector2 (250f, (250f / avatarImg.rectTransform.sizeDelta.x) * avatarImg.rectTransform.sizeDelta.y);
		}*/
	}

	public void PickImage( int maxSize )
	{

		NativeGallery.Permission permission = NativeGallery.GetImageFromGallery( ( path ) =>
			{
				Debug.Log( "Image path: " + path );
				if( path != null )
				{
					// Create Texture from selected image
					Texture2D texture = NativeGallery.LoadImageAtPath( path, maxSize );
					if( texture == null )
					{
						Debug.Log( "Couldn't load texture from " + path );
						return;
					}
						

					userAvatarFilePath = path;

					temporaryImg.texture = texture;


					temporaryImg.SetNativeSize ();

					temporaryImg.rectTransform.sizeDelta = new Vector2 ((400f / gameObject.GetComponent<RawImage> ().rectTransform.sizeDelta.y) * gameObject.GetComponent<RawImage> ().rectTransform.sizeDelta.x, 400f);




				}
			}, "Selecione uma imagem para o seu avatar", "image/png", maxSize );

		Debug.Log( "Permission result: " + permission );

	}


}
