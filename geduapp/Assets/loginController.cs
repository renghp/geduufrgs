﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class loginController : MonoBehaviour {

	//public static string lastSceneEntered;

	// Use this for initialization
	void Start () {
		
	
	}
	
	// Update is called once per frame
	void Update () {
		

	}

	void Awake()
	{
		DontDestroyOnLoad(this.gameObject);
	}

	public void loadScene(string sceneName)
	{
		//lastSceneEntered = SceneManager.GetActiveScene().name;
		if (sceneName == "loginScene") {
			//PlayerPrefs.SetString ("username", "");
			PlayerPrefs.SetString ("password", "");
			//PlayerPrefs.SetInt ("storedCurrentWeek", 0);
		}

		SceneManager.LoadScene(sceneName);
	}

	public void quitApp()
	{
		Application.Quit();
	}
}
