INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (1, 3, 'e', 1, "Sendo dado que 1J = 0,239 cal, o valor que melhor expressa, em calorias, o calor produzido em 5 minutos de funcionamento de um ferro elétrico, ligado a uma fonte de 120 V e atravessado por uma corrente de 5,0A, é: ", "", 
"7,0 10^4 ", "0,70 10^4", "0,070 10^4", "0,43 10^4", "4,3 10^4",
"", "", "", "", "");

DELETE from question where id >0;

ALTER TABLE question AUTO_INCREMENT = 1;

select * from question;

-- FISICA FACIL -------------------------------

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (1, 3, 'd', 1, "Alguns peixes, como o poraquê, a enguia-elétrica da Amazônia, podem produzir uma corrente elétrica quando se encontram em perigo. 
Um poraquê de 1 metro de comprimento, em perigo, produz uma corrente em torno de 2 ampères e uma voltagem de 600 volts. O quadro apresenta a potência aproximada de
equipamentos elétricos. O equipamento elétrico que tem potência similar àquela produzida por esse peixe em perigo é o(a):", 
"https://s3.amazonaws.com/gedunorthamerica/q1Fis.png", 
"exaustor", "computador", "aspirador de pó", "churrasqueira elétrica", "secadora de roupas",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (1, 3, 'b', 1, "A epilação a laser (popularmente conhecida como depilação a laser) consiste na aplicação 
de uma fonte de luz para aquecer e causar uma lesão localizada e controlada nos folículos 
capilares. Para evitar que outros tecidos sejam danificados, selecionam-se comprimentos de 
onda que são absorvidos pela melanina presente nos pelos, mas que não afetam a oxi-hemoglobina 
do sangue e a água dos tecidos da região em que o tratamento será aplicado. A figura mostra como 
é a absorção de diferentes comprimentos de onda pela melanina, oxi-hemoglobina e água. Qual é o 
comprimento de onda, em nm, ideal para a epilação a laser?", 
"https://s3.amazonaws.com/gedunorthamerica/q2fis.png", 
"400", "700", "1100", "900", "500",
"", "", "", "", "");


INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (1, 3, 'e', 1, "A figura mostra como é a emissão de radiação eletromagnética para cinco tipos de lâmpada: haleto metálico, tungstênio, mercúrio, xênon e LED (diodo emissor de luz). As áreas marcadas em cinza são proporcionais à intensidade da energia liberada pela lâmpada. As linhas pontilhadas mostram a sensibilidade do olho humano aos diferentes comprimentos de onda. UV e IV são as regiões do ultravioleta e do infravermelho, respectivamente. Um arquiteto deseja iluminar uma sala usando uma lâmpada que produza boa iluminação, mas que não aqueça o ambiente. Qual tipo de lâmpada melhor atende ao desejo do arquiteto?", 
"https://s3.amazonaws.com/gedunorthamerica/q3fis.png", 
"Haleto Metálico", "Tungstênio", "Mercúrio", "Xênon", "LED",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (1, 1, 'b', 1, "Em uma colisão frontal entre dois automóveis, a força que o cinto de segurança exerce sobre o tórax e abdômen do motorista pode causar lesões graves nos órgãos internos. Pensando na segurança do seu produto, um fabricante de automóveis realizou testes em cinco modelos diferentes de cinto. Os testes simularam uma colisão de 0,30 segundo de duração, e os bonecos que representavam os ocupantes foram equipados com acelerômetros. Esse equipamento registra o módulo da desaceleração do boneco em função do tempo. Os parâmetros como massa dos bonecos, dimensões dos cintos e velocidade imediatamente antes e após o impacto foram os mesmos para todos os testes. O resultado final obtido está no gráfico de aceleração por tempo. Qual modelo de cinto oferece menor risco de lesão interna ao motorista?", 
"https://s3.amazonaws.com/gedunorthamerica/q4fis.png", 
"1", "2", "3", "4", "5",
"", "", "", "", "");


INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (1, 3, 'c', 1, "Os congestionamentos de trânsito constituem um problema que aflige, todos os dias, milhares de motoristas brasileiros. O gráfico ilustra a situação, representando, ao longo de um intervalo definido de tempo, a variação da velocidade de um veículo durante um congestionamento. Quantos minutos o veículo permaneceu imóvel ao longo do intervalo de tempo total analisado?", 
"https://s3.amazonaws.com/gedunorthamerica/q5fis.png", 
"4", "3", "2", "1", "0",
"", "", "", "", "");


INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (1, 3, 'c', 1, "Ao sintonizar uma estação de rádio AM, o ouvinte está selecionando apenas uma dentre as inúmeras ondas que chegam à antena receptora do aparelho. Essa seleção acontece em razão da ressonância do circuito receptor com a onda que se propaga. O fenômeno físico abordado no texto é dependente de qual característica da onda?", 
"", 
"Amplitude", "Polarização", "Frequência", "Intensidade", "Velocidade",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (1, 3, 'd', 3, "Para a instalação de um aparelho de ar-condicionado, é sugerido que ele seja colocado na parte superior da parede do cômodo, pois a maioria dos fluidos (líquidos e gases), quando aquecidos, sofrem expansão, tendo sua densidade diminuída e sofrendo um deslocamento ascendente. Por sua vez, quando são resfriados, tornam-se mais densos e sofrem um deslocamento descendente. A sugestão apresentada no texto minimiza o consumo de energia, porque:", 
"", 
"diminui a umidade do ar dentro do cômodo.", "aumenta a taxa de condução térmica para fora do cômodo.", "torna mais fácil o escoamento da água para fora do cômodo.", "facilita a circulação das correntes de ar frio e quente dentro do cômodo.", "e)	diminui a taxa de emissão de calor por parte do aparelho para dentro do cômodo.",
"", "", "", "", "");


INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (1, 3, 'a', 1, "Uma lâmpada LED (diodo emissor de luz), que funciona com 12V e corrente contínua de 0,45A, produz a mesma quantidade de luz que uma lâmpada incandescente de 60W de potência. Qual é o valor da redução da potência consumida ao se substituir a lâmpada incandescente pela de LED?", 
"", 
"54,6W", "27,0W", "26,6W", "5,4W", "5,0W",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (1, 3, 'd', 1, "Uma pessoa abre sua geladeira, verifica o que há dentro e depois fecha a porta dessa geladeira. Em seguida, ela tenta abrir a geladeira novamente, mas só consegue fazer isso depois de exercer uma força mais intensa do que a habitual. A dificuldade extra para reabrir a geladeira ocorre porque o(a)", 
"", 
"volume de ar dentro da geladeira diminuiu.", "motor da geladeira está funcionando com potência máxima.", "força exercida pelo imã fixado na porta da geladeira aumenta.", "pressão no interior da geladeira está abaixo da pressão externa.", "temperatura no interior da geladeira é inferior ao valor existente antes de ela ser aberta.",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (1, 3, 'd', 1, "Ao ouvir uma flauta e um piano emitindo a mesma nota musical, consegue-se diferenciar esses instrumentos um do outro. Essa diferenciação se deve principalmente ao(à)", 
"", 
"intensidade sonora do som de cada instrumento musical.", "potência sonora do som emitido pelos diferentes instrumentos musicais.", "diferente velocidade de propagação do som emitido por cada instrumento musical.", "timbre do som, que faz com que os formatos das ondas de cada instrumento sejam diferentes.", "altura do som, que possui diferentes frequências para diferentes instrumentos musicais.",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (1, 3, 'c', 1, "Uma pessoa, lendo o manual de uma ducha que acabou de adquirir para a sua casa, observe o gráfico, que relaciona a vazão na ducha com a pressão, medida em metros de coluna de água (mca). Nessa casa residem quatro pessoas. Cada uma delas toma um banho por dia, com duração média de 8 minutos, permanecendo o registro aberto com vazão máxima durante esse tempo. A ducha é instalada em um ponto seis metros abaixo do nível da lâmina de água, que se mantém constante dentro do reservatório. Ao final de 30 dias, esses banhos consumirão um volume de água, em litros, igual a:", 
"https://s3.amazonaws.com/gedunorthamerica/q12fis.png", 
"69120", "17280", "11520", "8640", "2880",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (1, 3, 'e', 1, "Em viagens de avião, é solicitado aos passageiros o desligamento de todos os aparelhos cujo funcionamento envolva a emissão ou a recepção de ondas eletromagnéticas. O procedimento é utilizado para eliminar fontes de radiação que possam interferir nas comunicações via rádio dos pilotos com a torre de controle. A propriedade das ondas emitidas que justifica o procedimento adotado é o fato de:", 
"", 
"terem fases opostas.", "serem ambas audíveis.", "terem intensidades inversas.", "serem da mesma amplitude.", "terem frequências próximas.",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (1, 1, 'c', 1, "Uma empresa de transportes precisa efetuar a entrega de uma encomenda o mais breve possível. Para tanto, a equipe de logística analisa o trajeto desde a empresa até o local da entrega. Ela verifica que o trajeto apresenta dois trechos de distâncias diferentes e velocidades máximas permitidas diferentes. No primeiro trecho, a velocidade máxima permitida é de 80 km/h e a distância a ser percorrida é de 80 km. No segundo trecho, cujo comprimento vale 60 km, a velocidade máxima permitida é 120 km/h.
Supondo que as condições de trânsito sejam favoráveis para que o veículo da empresa ande continuamente na velocidade máxima permitida, qual será o tempo necessário, em horas, para a realização da entrega?", 
"", 
"0,7", "1,4", "1,5", "2,0", "3,0",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (1, 3, 'd', 1, "A capacidade mínima, em BTU/h, de um aparelho de ar-condicionado, para ambientes sem exposição ao sol, pode ser determinada da seguinte forma:
• 600 BTU/h por m2 , considerando-se até duas pessoas no ambiente;
• para cada pessoa adicional nesse ambiente, acrescentar 600 BTU/h;
• acrescentar mais 600 BTU/h para cada equipamento eletroeletrônico em funcionamento no ambiente.
Será instalado um aparelho de ar-condicionado em uma sala, sem exposição ao sol, de dimensões 4 m x 5 m, em que permaneçam quatro pessoas e possua um aparelho de televisão em funcionamento. A capacidade mínima, em BTU/h, desse aparelho de arcondicionado deve ser", 
"", 
"12000", "12600", "13200", "13800", "15000",
"", "", "", "", "");


INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (1, 3, 'c', 1, "Uma das modalidades presentes nas olimpíadas é o salto com vara. As etapas de um dos saltos de um atleta estão representadas na figura. Desprezando-se as forças dissipativas (resistência do ar e atrito), para que o salto atinja a maior altura possível, ou seja, o máximo de energia seja conservada, é necessário que", 
"https://s3.amazonaws.com/gedunorthamerica/q16fis.png", 
"a energia cinética, representada na etapa I, seja totalmente convertida em energia potencial elástica representada na etapa IV.", 
"a energia cinética, representada na etapa II, seja totalmente convertida em energia potencial gravitacional, representada na etapa IV.", 
"a energia cinética, representada na etapa I, seja totalmente convertida em energia potencial gravitacional, representada na etapa III.", 
"a energia potencial gravitacional, representada na etapa II, seja totalmente convertida em energia potencial elástica, representada na etapa IV.", 
"a energia potencial gravitacional, representada na etapa I, seja totalmente convertida em energia potencial elástica, representada na etapa III.",
"", "", "", "", "");


-- MATEMATICA FACIL -------------------------------


INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (2, 4, 'e', 1, "(2009) Em Florença, Itália, na Igreja de Santa Croce, é possível encontrar um portão em que aparecem os anéis de Borromeo. Alguns historiadores acreditavam que os círculos representavam as três artes: escultura, pintura e arquitetura, pois elas eram tão próximas quanto inseparáveis. Qual dos esboços a seguir melhor representa os anéis de Borromeo?", 
"https://s3.amazonaws.com/gedunorthamerica/q17mat.png", 
"", "", "", "", "",
"https://s3.amazonaws.com/gedunorthamerica/q17matA.png", 
"https://s3.amazonaws.com/gedunorthamerica/q17matB.png", 
"https://s3.amazonaws.com/gedunorthamerica/q17matC.png", 
"https://s3.amazonaws.com/gedunorthamerica/q17matD.png", 
"https://s3.amazonaws.com/gedunorthamerica/q17matE.png");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (2, 4, 'c', 1, "(2011) Em uma certa cidade, os moradores de um bairro carente de espaços de lazer reivindicam à prefeitura municipal a construção de uma praça. A prefeitura concorda com a solicitação e afirma que irá construí-la em formato retangular devido às características técnicas do terreno. Restrições de natureza orçamentária impõem que sejam gastos, no máximo, 180 m de tela para cercar a praça. A prefeitura apresenta aos moradores desse bairro as medidas dos terrenos disponíveis para a construção da praça. Para optar pelo terreno de maior área, que atenda às restrições impostas pela prefeitura, os moradores deverão escolher o terreno.", 
"https://s3.amazonaws.com/gedunorthamerica/q18mat.png", 
"1", "2", "3", "4", "5",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (2, 4, 'c', 1, "(2011) Para uma atividade realizada no laboratório de Matemática, um aluno precisa construir uma maquete da quadra de esportes da escola que tem 28 m de comprimento por 12 m de largura. A maquete deverá ser construída na escala de 1 : 250. Que medidas de comprimento e largura, em cm, o aluno utilizará na construção da maquete?", 
"", 
"4,8 e 11,2", "7,0 e 3,0", "11,2 e 4,8", "28,0 e 12,0", "30,0 e 70,0",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (2, 4, 'c', 1, "(2016) Os alunos de uma escola utilizaram cadeiras iguais às da figura para uma aula ao ar livre. A professora, ao final da aula, solicitou que os alunos fechassem as cadeiras para guardá-las. Depois de guardadas, os alunos fizeram um esboço da vista lateral da cadeira fechada. Qual é o esboço obtido pelos alunos?", 
"https://s3.amazonaws.com/gedunorthamerica/q20mat.png", 
"", "", "", "", "",
"https://s3.amazonaws.com/gedunorthamerica/q20matA.png", 
"https://s3.amazonaws.com/gedunorthamerica/q20matB.png", 
"https://s3.amazonaws.com/gedunorthamerica/q20matC.png", 
"https://s3.amazonaws.com/gedunorthamerica/q20matD.png", 
"https://s3.amazonaws.com/gedunorthamerica/q20matE.png");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (2, 4, 'c', 1, "(2016) Um túnel deve ser lacrado com uma tampa de concreto. A seção transversal do túnel e a tampa de concreto têm contornos de um arco de parábola e mesmas dimensões. Para determinar o custo da obra, um engenheiro deve calcular a área sob o arco parabólico em questão. Usando o eixo horizontal no nível do chão e o eixo de simetria da parábola como eixo vertical, obteve a seguinte equação para a parábola: y = 9 – x2 , sendo x e y medidos em metros. Sabe-se que a área sob uma parábola como esta é igual a 2/3 da área do retângulo cujas dimensões são, respectivamente, iguais à base e à altura da entrada do túnel. Qual é a área da parte frontal da tampa de concreto, em metros quadrados?", 
"", 
"18", "20", "36", "45", "54",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (2, 4, 'e', 1, "(2015) Uma família fez uma festa de aniversário e enfeitou o local da festa com bandeirinhas de papel. Essas bandeirinhas foram feitas da seguinte maneira: inicialmente, recortaram as folhas de papel em forma de quadrado, como mostra a Figura 1. Em seguida, dobraram as folhas quadradas ao meio sobrepondo os lados BC e AD, de modo que C e D coincidam, e o mesmo ocorra com A e B, conforme ilustrado na Figura 2. Marcaram os pontos médios O e N, dos lados FG e AF, respectivamente, e o ponto M do lado AD, de modo que AM seja igual a um quarto de AD. A seguir, fizeram cortes sobre as linhas pontilhadas ao longo da folha dobrada. Após os cortes, a folha é aberta e a bandeirinha está pronta.
A figura que representa a forma da bandeirinha pronta é", 
"https://s3.amazonaws.com/gedunorthamerica/q21mat.png", 
"", "", "", "", "",
"https://s3.amazonaws.com/gedunorthamerica/q21matA.png", 
"https://s3.amazonaws.com/gedunorthamerica/q21matB.png", 
"https://s3.amazonaws.com/gedunorthamerica/q21matC.png", 
"https://s3.amazonaws.com/gedunorthamerica/q21matD.png", 
"https://s3.amazonaws.com/gedunorthamerica/q21matE.png");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (2, 4, 'b', 1, "(2018) Um produtor de milho utiliza uma área de 160 hectares para as suas atividades agrícolas. Essa área é dividida em duas partes: uma de 40 hectares, com maior produtividade, e outra, de 120 hectares, com menor produtividade. 
A produtividade é dada pela razão entre a produção, em tonelada, e a área cultivada. Sabe-se que a área de 40 hectares tem produtividade igual a 2,5 vezes à da outra. Esse fazendeiro pretende aumentar sua produção total em 15%, aumentando o tamanho da sua propriedade. Para tanto, pretende comprar uma parte de uma fazenda vizinha, que possui a mesma produtividade da parte de 120 hectares de suas terras. 
Qual é a área mínima, em hectare, que o produtor precisará comprar?
", 
"", 
"36", "33", "27", "24", "21",
"", "", "", "", "");



INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (2, 4, 'b', 1, "Minecraft é um jogo virtual que pode auxiliar no desenvolvimento de conhecimentos relacionados a espaço e forma. É possível criar casas, edifícios, monumentos e até naves espaciais, tudo em escala real, através do empilhamento de cubinhos. Um jogador deseja construir um cubo com dimensões 4 x 4 x 4. Ele já empilhou alguns dos cubinhos necessários, conforme a figura.
Os cubinhos que ainda faltam empilhar para finalizar a construção do cubo, juntos, formam uma peça única, capaz de completar a tarefa.  O formato da peça capaz de completar o cubo 4 x 4 x 4 é
", 
"https://s3.amazonaws.com/gedunorthamerica/q23mat.png", 
"", "", "", "", "",
"https://s3.amazonaws.com/gedunorthamerica/q23matA.png", 
"https://s3.amazonaws.com/gedunorthamerica/q23matB.png", 
"https://s3.amazonaws.com/gedunorthamerica/q23matC.png", 
"https://s3.amazonaws.com/gedunorthamerica/q23matD.png", 
"https://s3.amazonaws.com/gedunorthamerica/q23matE.png");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (2, 4, 'b', 1, "(2018) A raiva é uma doença viral e infecciosa, transmitida por mamíferos. A campanha nacional de vacinação antirrábica tem o objetivo de controlar a circulação do vírus da raiva canina e felina, prevenindo a raiva humana. O gráfico mostra a cobertura (porcentagem de vacinados) da campanha, em cães, nos anos de 2013,2015 e 2017, no município de Belo Horizonte, em Minas Gerais. Os valores das coberturas dos anos de 2014 e 2016 não estão informados no gráfico e deseja-se estimá-los. Para tal, levou-se em consideração que a variação na cobertura de vacinação da campanha antirrábica, nos períodos de 2013 a 2015 e de 2015 a 2017, deu-se de forma linear. Qual teria sido a cobertura dessa campanha no ano de 2014?", 
"https://s3.amazonaws.com/gedunorthamerica/q24mat.png", 
"62,3%", "63,0%", "63,5%", "64,0%", "65,5%",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (2, 4, 'c', 1, "(2017) Os congestionamentos de trânsito constituem um problema que aflige, todos os dias, milhares de motoristas brasileiros. O gráfico ilustra a situação, representando, ao longo de um intervalo definido de tempo, a variação da velocidade de um veículo durante um congestionamento. Quantos minutos o veículo permaneceu imóvel ao longo do intervalo de tempo total analisado?", 
"https://s3.amazonaws.com/gedunorthamerica/q25mat.png", 
"4", "3", "2", "1", "0",
"", "", "", "", "");

-- QUIMICA FACIL -------------------------------


INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (3, 4, 'b', 1, "Um fato corriqueiro ao se cozinhar arroz é o derramamento de parte da água de cozimento sobre a chama azul do fogo, mudando-a para uma chama amarela. Essa mudança de cor pode suscitar interpretações diversas, relacionadas às substâncias presentes na água de cozimento. Além do sal de cozinha (NaCl), nela se encontram carboidratos, proteínas e sais minerais. Cientificamente, sabe-se que essa mudança de cor da chama ocorre pela", 
"", 
"reação do gás de cozinha com o sal, volatilizando gás cloro.", 
"emissão de fótons pelo sódio, excitado por causa da chama", 
"produção de derivado amarelo, pela reação com o carboidrato.", 
"reação do gás de cozinha com a água, formando gás hidrogênio.", 
"excitação das moléculas de proteínas, com formação de luz amarela.",
"", "", "", "", "");


INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (3, 4, 'a', 1, "Muitas indústrias e fábricas lançam para o ar, através de suas chaminés, poluentes prejudiciais às plantas e aos animais. Um desses poluentes reage quando em contato com o gás oxigênio e a água da atmosfera, conforme as equações químicas:
Equação 1: 2 SO2 + O2 -> 2 SO3
Equação 2:  SO3 + H2O -> H2SO4
De acordo com as equações, a alteração ambiental decorrente da presença desse poluente intensifica o(a)", 
"", 
"formação de chuva ácida.",
"surgimento de ilha de calor.", 
"redução da camada de ozônio.",
"ocorrência de inversão térmica.", 
"emissão de gases de efeito estufa.",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (3, 4, 'b', 1, "Em algumas regiões brasileiras, é comum se encontrar um animal com odor característico, o zorrilho. Esse odor serve para a proteção desse animal, afastando seus predadores. Um dos feromônios responsáveis por esse odor é uma substância que apresenta isomeria trans e um grupo tiol ligado à sua cadeia. A estrutura desse feromônio, que ajuda na proteção do zorrilho, é:", 
"", 
"", "", "", "", "",
"https://s3.amazonaws.com/gedunorthamerica/q29quiA.png", 
"https://s3.amazonaws.com/gedunorthamerica/q29quiB.png", 
"https://s3.amazonaws.com/gedunorthamerica/q29quiC.png", 
"https://s3.amazonaws.com/gedunorthamerica/q29quiD.png", 
"https://s3.amazonaws.com/gedunorthamerica/q29quiE.png");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (3, 4, 'c', 1, "O hidrocarboneto representado pela estrutura química a seguir pode ser isolado a partir das folhas ou das flores de determinadas plantas. Além disso, sua função é relacionada, entre outros fatores, a seu perfil de insaturações. Considerando esse perfil específico, quantas ligações pi a molécula contém?", 
"https://s3.amazonaws.com/gedunorthamerica/q30qui.png", 
"1", "2", "4", "6", "7",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (3, 4, 'd', 1, "O esquema representa, de maneira simplificada, o processo de produção de etanol utilizando milho como matéria-prima. A etapa de hidrólise na produção de etanol a partir do milho é fundamental para que ", 
"https://s3.amazonaws.com/gedunorthamerica/q31qui.png", 
"a glicose seja convertida em sacarose.",
 "as enzimas dessa planta sejam ativadas.",
 "a maceração favoreçaa solubilização em água.",
 "o amido seja transformado em substratos utilizáveis pela levedura.",
 "os grãos com diferentes composições químicas sejam padronizados.",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (3, 4, 'c', 1, "Considerando a obtenção e o consumo desse biocombustível, há transformação química quando:", 
"https://s3.amazonaws.com/gedunorthamerica/q32qui.png", 
"o etanol é armazenado em tanques de aço inoxidável.", 
"a palha de cana-de-açúcar é exposta ao sol para secagem.", 
"a palha da cana e o etanol são usados como fonte de energia.", 
"os poluentes SOx, NOx e MP são mantidos intactos e dispersos na atmosfera.", 
"os materiais particulados (MP) são espalhados no ar e sofrem deposição seca.",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (3, 4, 'e', 1, "Ao se deparar com um indivíduo que sofreu queimadura com formação de flictena, o procedimento de primeiros socorros que deve ser realizado antes de encaminhar o paciente ao hospital é", 
"https://s3.amazonaws.com/gedunorthamerica/q33qui.png", 
"colocar gelo sobre a flictena para amenizar o ardor.", 
"utilizar manteiga para evitar o rompimento da flictena.", 
"passar creme dental para diminuir a ardência da flictena.", 
"perfurar a flictena para que a água acumulada seja liberada.", 
"cobrir a flictena com gazes molhadas para evitar a desidratação.",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (3, 4, 'a', 1, "Uma forma de organização de um sistema biológico é a presença de sinais diversos utilizados pelos indivíduos para se comunicarem. No caso das abelhas da espécie Apis mellifera, os sinais utilizados podem ser feromônios. Para saírem e voltarem de suas colmeias, usam um feromônio que indica a trilha percorrida por elas (Composto A). Quando pressentem o perigo, expelem um feromônio de alarme (Composto B), que serve de sinal para um combate coletivo. O que diferencia cada um desses sinais utilizados pelas abelhas são as estruturas e funções orgânicas dos feromônios. As funções orgânicas que caracterizam os feromônios de trilha e de alarme são, respectivamente,", 
"https://s3.amazonaws.com/gedunorthamerica/q34qui.png", 
"álcool e éster.", 
"aldeído e cetona.", 
"éter e hidrocarboneto.", 
"enol e ácido carboxílico.", 
"ácido carboxílico e amida.",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (3, 4, 'e', 1, "O principal processo industrial utilizado na produção de fenol é a oxidação do cumeno (isopropilbenzeno). A equação mostra que esse processo envolve a formação do hidroperóxido de cumila, que em seguida é decomposto em fenol e acetona, ambos usados na indústria química como precursores de moléculas mais complexas. Após o processo de síntese, esses dois insumos devem ser separados para comercialização individual. Considerando as características físico-químicas dos dois insumos formados, o método utilizado para a separação da mistura, em escala industrial, é a ", 
"https://s3.amazonaws.com/gedunorthamerica/q35qui.png", 
"filtração", "ventilação.", "decantação.", "vaporação.", "destilação fracionada.",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (3, 4, 'a', 1, "As moléculas de nanoputians lembram figuras humanas e foram criadas para estimular o interesse de jovens na compreensão da linguagem expressa em fórmulas estruturais, muito usadas em química orgânica. Um exemplo é o NanoKid, representado na figura. Em que parte do corpo do NanoKid existe carbono quaternário?", 
"https://s3.amazonaws.com/gedunorthamerica/q36qui.png", 
"Mãos", "Cabeça", "Tórax", "Abdômen", "Pés",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (3, 4, 'a', 1, "Entre as substâncias usadas para o tratamento de água está o sulfato de alumínio que, em meio alcalino, forma partículas em suspensão na água, às quais as impurezas presentes no meio se aderem.
O método de separação comumente usado para retirar o sulfato de alumínio com as impurezas aderidas é a", 
"", 
"flotação.", "levigação.", "ventilação", "peneiração", "centrifugação",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (3, 4, 'a', 1, "A própolis é um produto natural conhecido por suas propriedades anti-inflamatórias e cicatrizantes. Esse material contém mais de 200 compostos identificados até o momento. Dentre eles, alguns são de estrutura simples, como é o caso do C6H5CO2CH2CH3, cuja estrutura está mostrada a seguir. O ácido carboxílico e o álcool capazes de produzir o éster em apreço por meio da reação de esterificação são,respectivamente,", 
"https://s3.amazonaws.com/gedunorthamerica/q37qui.png", 
"ácido benzoico e etanol.", 
"ácido propanoico e hexanol.", 
"ácido fenilacético e metanol.", 
"ácido propiônico e cicloexanol.", 
"ácido acético e álcool benzílico.",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (3, 4, 'c', 1, "Uma dona de casa acidentalmente deixou cair na geladeira a água proveniente do degelo de um peixe, o que deixou um cheiro forte e desagradável dentro do eletrodoméstico. Sabe-se que o odor característico de peixe se deve às aminas e que esses compostos se comportam como bases. Na tabela são listadas as concentrações hidrogeniônicas de alguns materiais encontrados na cozinha, que a dona de casa pensa em utilizar na limpeza da geladeira. Dentre os materiais listados, quais são apropriados para amenizar esse odor?", 
"https://s3.amazonaws.com/gedunorthamerica/q38qui.png", 
"Álcool ou sabão.", 
"Suco de limão ou álcool.", 
"Suco de limão ou vinagre.", 
"Suco de limão, leite ou sabão", 
"Sabão ou carbonato de sódio/barrilha.",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (3, 4, 'b', 1, "Os tubos de PVC, material organoclorado sintético, são normalmente utilizados como encanamento na construção civil. Ao final da sua vida útil, uma das formas de descarte desses tubos pode ser a incineração. Nesse processo libera-se HCl (g), cloreto de hidrogênio, dentre outras substâncias. Assim, é necessário um tratamento para evitar o problema da emissão desse poluente.
Entre as alternativas possíveis para o tratamento, é apropriado canalizar e borbulhar os gases provenientes da incineração em", 
"", 
"água dura.", 
"água de cal.", 
"água salobra.", 
"água destilada.", 
"água desmineralizada.",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (3, 4, 'c', 1, "O armazenamento de certas vitaminas no organismo apresenta grande dependência de sua solubilidade. Por exemplo, vitaminas hidrossolúveis devem ser incluídas na dieta diária, enquanto vitaminas lipossolúveis são armazenadas em quantidades suficientes para evitar doenças causadas pela sua carência. A seguir são apresentadas as estruturas químicas de cinco vitaminas necessárias ao organismo. Dentre as vitaminas apresentadas na figura, aquela que necessita de maior suplementação diária é:", 
"https://s3.amazonaws.com/gedunorthamerica/q40qui.png", 
"I", 
"II", 
"III", 
"IV", 
"V",
"", "", "", "", "");

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (3, 4, 'e', 1, "Osmose é um processo espontâneo que ocorre em todos os organismos vivos e é essencial à manutenção da vida. Uma solução 0,15 mol/L de NaCl (cloreto de sódio) possui a mesma pressão osmótica das soluções presentes nas células humanas. A imersão de uma célula humana em uma solução 0,20 mol/L de NaCl tem, como consequência, a", 
"", 
"adsorção de íons Na+ sobre a superfície da célula.", 
"difusão rápida de íons Na+ para o interior da célula.", 
"diminuição da concentração das soluções presentes na célula.", 
"transferência de íons Na+ da célula para a solução.", 
"transferência de moléculas de água do interior da célula para a solução.",
"", "", "", "", "");

-- template:

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (3, 4, '', 1, " ", 
"https://s3.amazonaws.com/gedunorthamerica/.png", 
"", 
"", 
"", 
"", 
"",
"", "", "", "", "");


select * from question;





