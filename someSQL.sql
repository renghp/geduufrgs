-- DROP TABLE area;
-- DELETE FROM student where id > 0;
-- SELECT * FROM student;
-- describe student;
-- ALTER TABLE major MODIFY name VARCHAR(50) NOT NULL;
-- ALTER TABLE area AUTO_INCREMENT = 1;
CREATE TABLE area (id int PRIMARY KEY NOT NULL AUTO_INCREMENT, name VARCHAR(40) NOT NULL);

CREATE TABLE major (id int PRIMARY KEY NOT NULL AUTO_INCREMENT, name VARCHAR(50) NOT NULL);

ALTER TABLE major ADD CONSTRAINT majorUniqueName UNIQUE (name);

CREATE TABLE student (id int PRIMARY KEY NOT NULL AUTO_INCREMENT, user varchar(20) UNIQUE NOT NULL,  name VARCHAR(30) NOT NULL, lastname VARCHAR(30) NOT NULL, email VARCHAR(30) NOT NULL UNIQUE, password VARCHAR(60) NOT NULL, 
areaID int, majorID int, works int(3), studies int(5), highSchool int(3), payment bool, currentWeek int DEFAULT 1, FOREIGN KEY (areaID) REFERENCES area(id), FOREIGN KEY (majorID) REFERENCES major(id));

-- change this to zero if we end up nor letting free users in
ALTER TABLE student ALTER payment SET DEFAULT 1;

-- ALTER TABLE student ADD COLUMN currentWeek int DEFAULT 1;

SELECT MAX(id) AS totalSubgroups FROM subgroup;

CREATE TABLE subject (id int PRIMARY KEY NOT NULL AUTO_INCREMENT, name VARCHAR(15) NOT NULL);

CREATE TABLE subgroup (id int NOT NULL, name VARCHAR(60) NOT NULL, subjectID int, FOREIGN KEY (subjectID) REFERENCES subject(id), PRIMARY KEY (id, subjectID));

CREATE TABLE question (id int PRIMARY KEY NOT NULL AUTO_INCREMENT, subjectID int, subgroupID int, correctanswer char NOT NULL, difficulty int(4), 
questiontext text NOT NULL, img varchar(300), 
Atext text NOT NULL, Btext text NOT NULL, Ctext text NOT NULL, Dtext text NOT NULL, Etext text NOT NULL, 
Aimg varchar(300), Bimg varchar(300), Cimg varchar(300), Dimg varchar(300), Eimg varchar(300),
FOREIGN KEY (subjectID) REFERENCES subject(id), FOREIGN KEY (subgroupID) REFERENCES subgroup(id));

CREATE TABLE student_simulation (id int NOT NULL AUTO_INCREMENT, studentID int NOT NULL, dateTaken timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
nQuestionsMath int, nCorrectMath int, nQuestionsPhys int, nCorrectPhys int, nQuestionsChem int, nCorrectChem int,  
FOREIGN KEY (studentID) REFERENCES student(id), PRIMARY KEY (id, studentID));

CREATE TABLE forumTopic (id int NOT NULL AUTO_INCREMENT, studentID int NOT NULL, subjectID int NOT NULL, datePosted timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, dateLastAnswer timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
title varchar(60) NOT NULL, topicText text NOT NULL, solved bool, likes int, visits int,  
FOREIGN KEY (studentID) REFERENCES student(id), FOREIGN KEY (subjectID) REFERENCES subject(id), PRIMARY KEY (id));

drop table forumTopic;

CREATE TABLE post (id int NOT NULL AUTO_INCREMENT, studentID int NOT NULL, forumTopicID int NOT NULL, datePosted timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
postText text NOT NULL, likes int,  
FOREIGN KEY (studentID) REFERENCES student(id), FOREIGN KEY (forumTopicID) REFERENCES forumTopic(id), PRIMARY KEY (id));

CREATE TABLE subscription (id int NOT NULL AUTO_INCREMENT, studentID int NOT NULL, datePayed timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, days int,  
FOREIGN KEY (studentID) REFERENCES student(id), PRIMARY KEY (id));


INSERT INTO subscription (studentID) values(33);

describe subscription;

SELECT id FROM subscription WHERE studentID = 33 ORDER BY datePayed DESC;

SELECT id FROM subscription WHERE studentID = 33 AND datePayed = (SELECT MAX(datePayed) FROM subscription);

SELECT * FROM student;

-- ALL AREAS:
INSERT INTO area values(NULL, "Artes"), (NULL, "Biológicas, Naturais e Agrárias"), (NULL, "Comunicação e Informação"), (NULL, "Economia, Gestão e Negócios"), (NULL, "Engenharia e Arquitetura"), (NULL, "Exatas e Tecnológicas"), (NULL, "Humanas e Sociais"), (NULL, "Saúde");

-- ALL MAJORS INTO THEIR RESPECTIVE AREAS:
INSERT INTO major values(NULL, "Artes Visuais"), (NULL, "Dança"), (NULL, "Design Visual"), (NULL, "Design de Produto"), (NULL, "História da Arte"), (NULL, "Música"), (NULL, "Teatro");
INSERT INTO major_area values(1, 1), (2, 1), (3, 1), (4, 1), (5, 1), (6,1), (7, 1);

INSERT INTO major values(NULL, "Agronomia"), (NULL, "Desenvolvimento Rural (EAD)"), (NULL, "Biotecnologia"), (NULL, "Ciências Biológicas"), (NULL, "Ciências Biológicas - Ceclimar"),
 (NULL, "Ciências da Natureza (EAD)"), (NULL, "Desenvolvimento Regional (Litoral Norte)"), (NULL, "Educação do Campo (Litoral Norte)"),
(NULL, "Engenharia Ambiental"), (NULL, "Engenharia Cartográfica"), (NULL, "Engenharia de Alimentos"), (NULL, "Geografia"), (NULL, "Geografia (EAD)"), 
(NULL, "Geografia (Litoral Norte)"), (NULL, "Medicina Veterinária"), (NULL, "Química"), (NULL, "Zootecnia");
INSERT INTO major_area values(8, 2), (9, 2), (10, 2), (11, 2), (12, 2), (13, 2), (23, 2), (24, 2), (14, 2), (15, 2), (16, 2), (17, 2), (18, 2), (19, 2), (20, 2), (21, 2), (22, 2);

INSERT INTO major values(NULL, "Arquivologia"), (NULL, "Biblioteconomia"), (NULL, "Comunicação Social"), (NULL, "Jornalismo"), (NULL, "Museologia"), (NULL, "Publicidade e Propaganda"),
 (NULL, "Relações Públicas");
INSERT INTO major_area values(14, 3), (21, 3);
INSERT INTO major_area values(25, 3), (26, 3), (27, 3), (28, 3), (29,3), (30, 3), (31, 3);

INSERT INTO major values(NULL, "Administração"), (NULL, "Administração Pública e Social"), (NULL, "Ciências Atuariais"), (NULL, "Ciências Contábeis"), 
(NULL, "Ciências Econômicas"), (NULL, "Engenharia de Produção"), (NULL, "Engenharia de Serviços (Litoral Norte)");
INSERT INTO major_area values(9, 4), (14, 4), (21,4);
INSERT INTO major_area values(32, 4), (33, 4), (34,4), (35,4), (36,4), (37,4), (38,4);
  
INSERT INTO major values(NULL, "Arquitetura e Urbanismo"), 
  (NULL,"Engenharia Civil"), (NULL,"Engenharia Elétrica"), (NULL,"Engenharia Física"), (NULL,"Engenharia Hídrica"), (NULL,"Engenharia Mecânica"), (NULL,"Engenharia Metalúrgica"),
  (NULL,"Engenharia Química"), (NULL,"Engenharia de Computação"), (NULL,"Engenharia de Controle e Automação"), (NULL,"Engenharia de Energia"), 
  (NULL,"Engenharia de Gestão de Energia (Litoral Norte)"), (NULL,"Engenharia de Materiais"), (NULL,"Engenharia de Minas");
INSERT INTO major_area values(3, 5), (4, 5);
INSERT INTO major_area values(16, 5), (17, 5), (18,5), (37,5),(38,5);
INSERT INTO major_area values(39, 5), (40, 5), (41,5), (42,5), (43,5), (44,5), (45,5), (46,5), (47,5), (48,5), (49,5), (50,5), (51,5), (52,5);
  
INSERT INTO major values(NULL, "Ciência da Computação"), (NULL, "Estatística"), (NULL, "Física"), (NULL, "Ciência e Tecnologia (Litoral Norte)"), (NULL, "Matemática");
INSERT INTO major_area values(14, 6), (50, 6), (38, 6), (21, 6);
INSERT INTO major_area values(53, 6), (54, 6), (55, 6), (56, 6),(57, 6);
   
INSERT INTO major values(NULL, "Ciências Jurídicas e Sociais"), (NULL, "Ciências Sociais"), (NULL, "Ciências Sociais (EAD)"), (NULL, "Filosofia"),
 (NULL, "História"), (NULL, "Letras"), (NULL, "Pedagogia"), (NULL, "Pedagogia (EAD)"), (NULL, "Políticas Públicas"), (NULL, "Psicologia"), 
(NULL, "Relações Internacionais"), (NULL, "Serviço Social");
INSERT INTO major_area values(14, 7), (21, 7);
INSERT INTO major_area values(58, 7), (59, 7), (60, 7), (61, 7), (62, 7), (63, 7), (64, 7), (65, 7), (66, 7), (67, 7), (68, 7), (69, 7);

INSERT INTO major values(NULL, "Biomedicina"), (NULL, "Educação Física"), (NULL, "Enfermagem"), (NULL, "Farmácia"), (NULL, "Fisioterapia"), (NULL, "Fonoaudiologia"), 
(NULL, "Medicina"), (NULL, "Nutrição"), (NULL, "Odontologia"), (NULL, "Saúde Coletiva");
INSERT INTO major_area values(11, 8), (69, 8);  
INSERT INTO major_area values(22, 8); 
INSERT INTO major_area values(67, 8); 
INSERT INTO major_area values(70, 8), (71, 8), (72, 8), (73, 8), (74, 8), (75, 8), (76, 8), (77, 8), (78, 8), (79, 8);  
  

INSERT INTO area values(99, "Selecione");
INSERT INTO major values(99, "Selecione");

-- ALL SUBJECTS:

INSERT INTO subject values (NULL, "Física"), (NULL, "Matemática"), (NULL, "Química"), (NULL, "Outra");

select * from subject;

select * from question where subgroupID = 6;

-- ALL SUBGROUPS:

select * from student;

UPDATE student SET payment = 1 WHERE id >0;



INSERT INTO subgroup values (0, "Iniciais", 1), (1, "Cinemática", 1), (2, "Ondas", 1), (3, "Termodinâmica", 1), (4, "Eletrodinâmica", 1), (5, "Hidrostática", 1), (6, "Física Moderna", 1), (7, "Energia", 1), (8, "Outras", 1);
INSERT INTO subgroup values (0, "Iniciais", 2), (1, "Lógica", 2), (2, "Funções", 2), (3, "Funções Casos Especiais", 2), (4, "Geometria", 2), (5, "Geometria Analítica", 2), (6, "Probabilidade e Análise Combinatória", 2), (7, "Potência e Radiciação", 2), (8, "Outras", 2);
INSERT INTO subgroup values (0, "Iniciais", 2), (1, "Orgânica", 3), (2, "Estequiometria", 3), (3, "Eletroquímica", 3), (4, "Bioquímica", 3), (5, "Inorgânica", 3), (6, "Cinética", 3), (7, "Ambiental", 3), (8, "Outras", 3);

DELETE from subgroup where id >=0;

select name from subgroup where id = 1 AND subjectID = 1;

UPDATE subgroup SET name = "Bioquímica" WHERE id = 4 AND subjectID = 3;



-- TESTS USER

-- in order to set automatic timestamp:
INSERT INTO student_simulation (studentID, nQuestionsMath, nCorrectMath, nQuestionsPhys, nCorrectPhys, nQuestionsChem, nCorrectChem) values(28, 1, 1, 1, 1, 1, 1);

INSERT INTO forumTopic (studentID, subjectID, title, topicText, solved, likes, visits) values(33, 3, "dúvida de química 7", "alguém aqui por acaso sabe quando é 3 + 3?alguém aqui por acaso sabe quando é 3 + 3????alguém aqui por acaso sabe quando é 3 + 3????alguém aqui por acaso sabe quando é 3 + 3????alguém aqui por acaso sabe quando é 3 + 3????alguém aqui por acaso sabe quando é 3 + 3????alguém aqui por acaso sabe quando é 3 + 3???alguém aqui por acaso sabe quando é 3 + 3???alguém aqui por acaso sabe quando é 3 + 3????alguém aqui por acaso sabe quando é 3 + 3????alguém aqui por acaso sabe quando é 3 + 3????alguém aqui por acaso sabe quando é 3 + 3????alguém aqui por acaso sabe quando é 3 + 3???alguém aqui por acaso sabe quando é 3 + 3????alguém aqui por acaso sabe quando é 3 + 3????alguém aqui por acaso sabe quando é 3 + 3????alguém aqui por acaso sabe quando é 3 + 3????alguém aqui por acaso sabe quando é 3 + 3????alguém aqui por acaso sabe quando é 3 + 3????", 0, 2, 5);

select * from question;



select * from forumTopic WHERE subjectID = 1 ORDER BY dateLastAnswer DESC;

select * from student;
-- drop table subgroup;

DESCRIBE student;

SELECT title, solved, studentID, datePosted, topicText  FROM forumTopic WHERE id = 1;

-- RELATION between major and area:
CREATE TABLE major_area(majorID int NOT NULL, areaID int NOT NULL, FOREIGN KEY (areaID) REFERENCES area(id), FOREIGN KEY (majorID) REFERENCES major(id), PRIMARY KEY (majorID, areaID));

select * from student;

UPDATE subscription SET datePayed = CURRENT_TIMESTAMP, days = 31 WHERE id = 2;

UPDATE student
SET currentWeek = 1
WHERE id > 0;

select * from student_simulation;

INSERT INTO student
VALUES (null, "fulanin", "Ciclano", "Da Silva", "fulano@gmail.com", "12345", 5, 2, 1, 2, 0);

INSERT INTO student
VALUES (null, "ciclanin", "Ciclano", "Da Silva", "ciclano@gmail.com", "aCV8giTeaTYcA", 5, 2, 1, 2, 0, true);

select * from student;

SELECT password FROM student WHERE email = "fulano@gmail.com";

SELECT password FROM student WHERE id = 1;

SELECT * FROM student_simulation;


SELECT * FROM subscription;

DELETE from question where id >0;

DELETE from forumTopic where id >0;

select * from student;

DESCRIBE post;

INSERT INTO post (studentID, forumTopicID, postText, likes) VALUES (28, 1, "sei não", 0);

INSERT INTO post (studentID, forumTopicID, postText, likes) VALUES (29, 1, "cé loko", 0);

INSERT INTO post (studentID, forumTopicID, postText, likes) VALUES (30, 1, "A palavra scriptorium (IPA: [scriptorium] Ltspkr.png ouvir) (no plural scriptoria) designa o espaço em que os livros manuscritos eram produzidos na Europa durante a Idade Média. Ainda é incerta a origem dessa acepção da palavra e se sabe que diferentes usos foram dados para o termo. Ao longo da história, a palavra scriptorium chegou a designar as ferramentas de escrita, o local de produção, o conjunto de uma obra, uma coleção de livros de algum monastério, o que mostrava sua proximidade com as bibliotecas, ou ainda mesas de trabalho associadas à atividade escriturária. Até meados do século XI, os scriptoria, enquanto espaços de produção livreira, eram encontrados dentro das catedrais e monastérios. Acredita-se que apenas as instituições que contavam com boas condições materiais poderiam ter um número suficiente de monges ou freiras empenhados na escrita, de tal forma que trabalhos necessários para a sobrevivência da instituição monástica, como a agricultura e a criação de animais, não fossem prejudicados por falta de pessoas. Porém, a partir do renascimento do século XII, com o desenvolvimento do ambiente urbano e o surgimento das universidades, a importância das instituições eclesiásticas na produção de conhecimento é descentralizada. No mesmo período, catedrais, abadias e conventos passam a compartilhar o protagonismo na produção manuscrita com ambientes mais laicos ou corteses. Isso ocorre na Península Ibérica, quando Afonso I de Portugal e Afonso X de Leão e Castela contavam com equipes de amanuenses que trabalhavam no sentido de coordenar pela palavra escrita os projetos políticos e culturais de seus respectivos reinos.", 0);

select * from question;

select * from subscription;


SELECT name, areaID, majorID FROM student WHERE id = 28;

INSERT INTO student (user, name, lastname, email, password, areaID, majorID, works, studies, highschool) VALUES ("rrrr", "Renan", "Guarese", "rg@gm.com", "rrrr", 8, 10, 0, 0, 0);

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (1, 3, 'c', 2, "Quanto é 2 + 2?", "https://rockcontent.com/wp-content/uploads/2017/01/formatos-de-imagem-2.jpg", 
"3", "4", "5", "6", "N/A",
"https://rockcontent.com/wp-content/uploads/2017/01/formatos-de-imagem-2.jpg", null, null, null, null);

INSERT INTO question (subjectID, subgroupID, correctanswer, difficulty, questiontext, img, 
Atext, Btext, Ctext, Dtext, Etext,
Aimg, Bimg, Cimg, Dimg, Eimg) 
VALUES (3, 3, 'c', 3, "questão 5 de química difícil", "https://s3-sa-east-1.amazonaws.com/gedufiles/ex5.png", 
"", "", "", "", "",
"https://s3-sa-east-1.amazonaws.com/gedufiles/a.PNG", "https://s3-sa-east-1.amazonaws.com/gedufiles/exampleImg.png", "https://s3-sa-east-1.amazonaws.com/gedufiles/ex2.png", "https://s3-sa-east-1.amazonaws.com/gedufiles/ex3.png", "https://s3-sa-east-1.amazonaws.com/gedufiles/ex4.png");

INSERT INTO student_simulation (studentID, nQuestionsMath, nCorrectMath, nQuestionsPhys, nCorrectPhys, nQuestionsChem, nCorrectChem) 
VALUES (33, 0, 0, 0, 0, 0, 0);


select * from student;

describe forumTopic;


SELECT SUM(nQuestionsMath), SUM(nCorrectMath), SUM(nQuestionsPhys), SUM(nCorrectPhys), SUM(nQuestionsChem), SUM(nCorrectChem) FROM student_simulation WHERE studentID = 33;

SELECT SUM(nQuestionsMath), SUM(nCorrectMath), SUM(nQuestionsPhys), SUM(nCorrectPhys), SUM(nQuestionsChem), SUM(nCorrectChem) FROM student_simulation WHERE studentID = 33;

UPDATE student SET areaID = 5, majorID= 5 WHERE id = 29;

SELECT nQuestionsMath, nCorrectMath, nQuestionsPhys, nCorrectPhys, nQuestionsChem, nCorrectChem FROM student_simulation WHERE id = 45;


SELECT COUNT(id) AS totalComments FROM post WHERE forumTopicID = 1;

SELECT id, title, solved, likes, visits  FROM forumTopic WHERE subjectID = 1 ORDER BY dateLastAnswer DESC;


SELECT id FROM student_simulation
WHERE studentID = 33
AND dateTaken >= curdate() - INTERVAL DAYOFWEEK(curdate())+6 DAY;


