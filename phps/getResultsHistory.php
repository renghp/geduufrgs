<?php
	
	$hostname = 'gedu-2-db-instance.cl0noz68ozaa.sa-east-1.rds.amazonaws.com';
	$dbusername = 'geduAdmin';
	$dbpassword = 'Gedu2019';
	$dbname =  'gedu';
    $dsn = 'mysql:host='. $hostname. ';dbname='.$dbname;
    $simulationsTable = "student_simulation";


	$conn = new PDO ("mysql:host=$hostname;dbname=$dbname", $dbusername, $dbpassword);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	
	
	if(!$conn){
		die("ERROR:001 Connection failed.");
    }
    
    if( isset($_POST["userID"]) ){
		$userID = $_POST["userID"];			
	}
	else{
		//$userID = 33;			
		die("3");	
    }
	
    


    $sql = "SELECT SUM(nQuestionsMath) AS `totalMath`, SUM(nCorrectMath) AS `correctMath`, SUM(nQuestionsPhys) AS `totalPhys`, SUM(nCorrectPhys) AS `correctPhys`, SUM(nQuestionsChem) AS `totalChem`, SUM(nCorrectChem) AS `correctChem` FROM `$simulationsTable` 
	WHERE studentID = :userID AND dateTaken >= curdate() - INTERVAL DAYOFWEEK(curdate())+6 DAY";


    $stmt_0 = $conn->prepare($sql);
    $stmt_0->bindParam(":userID",$userID);

    $stmt_0->execute();
	
	  

    if( $stmt_0->rowCount()==1 ){

        $result= $stmt_0->fetch(PDO::FETCH_ASSOC,0);

        $totalMath = $result["totalMath"];
		$totalPhys = $result["totalPhys"];
		$totalChem = $result["totalChem"];
		
		$correctMath = $result["correctMath"];
		$correctPhys = $result["correctPhys"];
		$correctChem = $result["correctChem"];
		
		$ratioMath = round(($correctMath/$totalMath)*100);
		$ratioPhys = round(($correctPhys/$totalPhys)*100);
		$ratioChem = round(($correctChem/$totalChem)*100);
		
	}
	else
	{
		$ratioMath = 0;
		$ratioPhys = 0;
		$ratioChem = 0;
	}
	
			
	
	$sql2 = "SELECT MAX(id) AS `lastID` FROM `$simulationsTable` WHERE studentID = :userID";


    $stmt_1 = $conn->prepare($sql2);
    $stmt_1->bindParam(":userID",$userID);

    $stmt_1->execute();
	
	 

    if( $stmt_1->rowCount()==1 ){


		
        $result2= $stmt_1->fetch(PDO::FETCH_ASSOC,0);

        $lastID = $result2["lastID"];
		

		
		$sql3 = "SELECT nQuestionsMath, nCorrectMath, nQuestionsPhys, nCorrectPhys, nQuestionsChem, nCorrectChem FROM `$simulationsTable` WHERE id = :lastID";

		$stmt_2 = $conn->prepare($sql3);
		$stmt_2->bindParam(":lastID",$lastID);

		$stmt_2->execute();
		
		  

		if( $stmt_2->rowCount()==1 ){



			
			$result3= $stmt_2->fetch(PDO::FETCH_ASSOC,0);

			$lastMath = $result3["nQuestionsMath"];
			$lastPhys = $result3["nQuestionsPhys"];
			$lastChem = $result3["nQuestionsChem"];
			
			$lastScoreMath = $result3["nCorrectMath"];
			$lastScorePhys = $result3["nCorrectPhys"];
			$lastScoreChem = $result3["nCorrectChem"];
			
			$lastRatioMath = round(($lastScoreMath/$lastMath)*100);
			$lastRatioPhys = round(($lastScorePhys/$lastPhys)*100);
			$lastRatioChem = round(($lastScoreChem/$lastChem)*100);
			
			
			
		}
		else
		{
			$lastRatioMath = 0;
			$lastRatioPhys = 0;
			$lastRatioChem = 0;
		}
		
	}
	else
	{
			$lastRatioMath = 0;
			$lastRatioPhys = 0;
			$lastRatioChem = 0;
	}
	
		//-------------------all phys results
		
	$sql4 = "SELECT `nQuestionsPhys`, `nCorrectPhys` FROM `$simulationsTable` WHERE studentID = :userID";
	
			$stmt_3 = $conn->prepare($sql4);
			$stmt_3->bindParam(":userID",$userID);

			$stmt_3->execute();
		


    if( $stmt_3->rowCount()>=1 ){


        
		$str1 = '';
    
		$objList = $stmt_3->fetch();			//pra não começar com hífen		
		$str1 = round(100*$objList[1]/$objList[0]);
    
		while ($objList = $stmt_3->fetch())			//salva cada id na string separado por um $$$
		{
			$str1 = $str1 . '§' .  round(100*$objList[1]/$objList[0]);
		}
		
		//die($str1);
	
	}
	else
		$str1='';
	
	
	//-------------------all math results
	
	$sql5 = "SELECT `nQuestionsMath`, `nCorrectMath` FROM `$simulationsTable` WHERE studentID = :userID";

		$stmt_4 = $conn->prepare($sql5);
		$stmt_4->bindParam(":userID",$userID);

		$stmt_4->execute();
	


    if( $stmt_4->rowCount()>=1 ){


        
		$str2 = '';
    
		$objList2 = $stmt_4->fetch();			//pra não começar com hífen		
		$str2 = round(100*$objList2[1]/$objList2[0]);
    
		while ($objList2 = $stmt_4->fetch())			//salva cada id na string separado por um $$$
		{
			$str2 = $str2 . '§' .  round(100*$objList2[1]/$objList2[0]);
		}
		

	
	}
	else
		$str2='';
	
		//-------------------all chem results
		
		$sql6 = "SELECT `nQuestionsChem`, `nCorrectChem` FROM `$simulationsTable` WHERE studentID = :userID";

		$stmt_5 = $conn->prepare($sql6);
		$stmt_5->bindParam(":userID",$userID);

		$stmt_5->execute();
	


    if( $stmt_5->rowCount()>=1 ){


        
		$str3 = '';
    
		$objList3 = $stmt_5->fetch();			//pra não começar com hífen		
		$str3 = round(100*$objList3[1]/$objList3[0]);
    
		while ($objList3 = $stmt_5->fetch())			//salva cada id na string separado por um $$$
		{
			$str3 = $str3 . '§' .  round(100*$objList3[1]/$objList3[0]);
		}
		

	
	}
	else
		$str3='';
	
	//-----------------finish

	
	die("recent:".$ratioPhys."§".$ratioMath."§".$ratioChem."§last:".$lastRatioPhys."§".$lastRatioMath."§".$lastRatioChem."§allPhys:".$str1."§allMath:".$str2."§allChem:".$str3);


    ?>