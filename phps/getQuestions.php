<?php
	
	$hostname = 'gedu-2-db-instance.cl0noz68ozaa.sa-east-1.rds.amazonaws.com';
	$dbusername = 'geduAdmin';
	$dbpassword = 'Gedu2019';
	$dbname =  'gedu';
    $dsn = 'mysql:host='. $hostname. ';dbname='.$dbname;
    $question_table = "question";
    $area_table = "area";
    $major_table = "major";

	$conn = new PDO ("mysql:host=$hostname;dbname=$dbname", $dbusername, $dbpassword);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	if(!$conn){
		die("ERROR:001 Connection failed.");
    }
    
    if( isset($_POST["subjectID"]) ){
		$subjectID = $_POST["subjectID"];			
	}
	else{
		$subjectID = 99;					//any subject
    }
	if( isset($_POST["subgroupID"]) ){
		$subgroupID = $_POST["subgroupID"];			
	}
	else{
		$subgroupID = 99;					//any subgroup
    }
	if( isset($_POST["difficulty"]) ){
		$difficulty = $_POST["difficulty"];			
	}
	else{
		$difficulty = 0;		//0 - any difficulty, 1 - easy, 2 - med, 3 - hard
    }
    
   
	if ($subjectID == 99)
	{
		if ($difficulty == 0)
		{
			$sql = "SELECT `id` FROM `$question_table`";
			
			$stmt_0 = $conn->prepare($sql);
			$stmt_0->execute();
		}
		else
		{
			$sql = "SELECT `id` FROM `$question_table` WHERE difficulty = :difficulty";
			$stmt_0 = $conn->prepare($sql);
			$stmt_0->bindParam(":difficulty",$difficulty);

			$stmt_0->execute();
		}
	}
	else if ($subgroupID == 99)
	{
		if ($difficulty == 0)
		{
			$sql = "SELECT `id` FROM `$question_table` WHERE subjectID = :subjectID";
		
			$stmt_0 = $conn->prepare($sql);
			$stmt_0->bindParam(":subjectID",$subjectID);

			$stmt_0->execute();
		}
		else
		{
			$sql = "SELECT `id` FROM `$question_table` WHERE subjectID = :subjectID AND difficulty = :difficulty";
			$stmt_0 = $conn->prepare($sql);
			$stmt_0->bindParam(":subjectID",$subjectID);
			$stmt_0->bindParam(":difficulty",$difficulty);

			$stmt_0->execute();
		}
	
	}
	else
	{
		if ($difficulty == 0)
		{
			$sql = "SELECT `id` FROM `$question_table` WHERE subjectID = :subjectID AND subgroupID = :subgroupID";
			
			$stmt_0 = $conn->prepare($sql);
			$stmt_0->bindParam(":subjectID",$subjectID);
			$stmt_0->bindParam(":subgroupID",$subgroupID);

			$stmt_0->execute();
			
		}

		else 
		{
			$sql = "SELECT `id` FROM `$question_table` WHERE subjectID = :subjectID AND subgroupID = :subgroupID AND difficulty = :difficulty";
			$stmt_0 = $conn->prepare($sql);
			$stmt_0->bindParam(":subjectID",$subjectID);
			$stmt_0->bindParam(":subgroupID",$subgroupID);
			$stmt_0->bindParam(":difficulty",$difficulty);

			$stmt_0->execute();
		}
		
	}
	

    if( $stmt_0->rowCount()>=1 ){

        
		$str1 = '';
    
		$objList = $stmt_0->fetch();			//pra não começar com hífen		
		$str1 = $objList[0];
    
		while ($objList = $stmt_0->fetch())			//salva cada id na string separado por um $$$
		{
			$str1 = $str1 . '§' .  $objList[0];
		}
		
		die($str1);
	
	}
	



    ?>