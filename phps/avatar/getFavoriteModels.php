<?php
	
	$hostname = 'localhost';
	$dbusername = 'phpmyadmin';
	$dbpassword = '9dQKz';
	$dbname =  'avatar';
    $dsn = 'mysql:host='. $hostname. ';dbname='.$dbname;
	$userTableName = "users";
	$inventoryTableName = "Inventory_User";
	
	if(!isset($_POST['userName']))
	{
		echo "Missing POST parameter.";
		exit();
    }
    else{
        $user 	   = $_POST['userName']; //
    }

	
	try{
		$conn = new PDO ("mysql:host=$hostname;dbname=$dbname", $dbusername, $dbpassword);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	catch(PDOException $e){
	
		echo "Failed to connect to MySQL: " .$e->getMessage();
		exit();
	}

    

	
			
    $sql_0= "SELECT `ExperimentID` , `Note`, UNIX_TIMESTAMP(TimeStamp) as `TimeStamp` FROM " . $inventoryTableName . " WHERE Username = :user";
    
    $stmt = $conn->prepare($sql_0);

    $stmt->bindParam(":user",$user );

    try{
        $stmt->execute();
    }
    catch(PDOException $e){
        die("Fail to find inventory: ".$e->message());
        
    }

	
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $result = utf8ize($result); //Caso não seja passado para UTF8, o json acaba ficando vazio
    
    
    $dataW["contents"] = $result; //prepare the variable for the json
    $j = json_encode($dataW);
    die($j);
	
    
    function utf8ize($d) {
        if (is_array($d)) {
            foreach ($d as $k => $v) {
                $d[$k] = utf8ize($v);
            }
        } else if (is_string ($d)) {
            return utf8_encode($d);
        }
        return $d;
    }
?>